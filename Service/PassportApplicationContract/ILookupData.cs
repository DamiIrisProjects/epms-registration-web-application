﻿using PassportApplicationCommon.Model;
using PassportApplicationCommon.Models;
using System.Collections.Generic;
using System.ServiceModel;

namespace PassportApplicationContract
{
    [ServiceContract]
    public interface ILookupData
    {
        [OperationContract]
        List<ProcessingCountry> GetCountries();

        [OperationContract]
        List<PassportBranch> GetBranchesForCountry(string countryCode);
                
        [OperationContract]
        PassportBranch GetNigeriaMainOffice();

        [OperationContract]
        List<State> GetNigeriaStates();

        [OperationContract]
        List<Passport> GetPreviousPassports(string previousPassportNumber);

        [OperationContract]
        List<KeyValuePair<int, string>> GetMaritialStatusList();

        [OperationContract]
        List<ProcessingColour> GetEyeColours();

        [OperationContract]
        List<ProcessingColour> GetHairColours();

        [OperationContract]
        List<Gender> GetGenderList();

        [OperationContract]
        List<DocumentType> GetDocumentTypes();

        [OperationContract]
        DocumentType GetDocumentType(int documentTypeId);

        [OperationContract]
        List<BreederDoc> GetApplicationTypeDocuments(int apptype);

        [OperationContract]
        List<KeyValuePair<int, string>> GetApplicationReasons();


        [OperationContract]
        List<KeyValuePair<int, string>> GetRejectReasons();


        [OperationContract]
        List<Passport> GetPassportHistory(string previousPassportNumber);

        [OperationContract]
        byte[] GetPassportImage(string passportNo, bool isFaceImage);
    }
}
