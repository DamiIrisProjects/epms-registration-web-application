﻿using PassportApplicationCommon.Models;
using System.Collections.Generic;
using System.ServiceModel;

namespace PassportApplicationContract
{
    [ServiceContract]
    public interface IEnrollment
    {
        //[OperationContract]
        //Payment GetPayment(int applicationID, int referenceNo);
                
        //[OperationContract]
        //PaymentResponse CalculatePayment(int documentType,int reason, DateTime dateOfBirth, DateTime paymentDate,bool passportExpired);

        [OperationContract]
        Passport GetPassport(string previousPassportNumber);

        [OperationContract]
        PassportApplication SaveEnrollment(PassportApplication application);

        [OperationContract]
        PassportApplication GetEnrollment(int applicationId, int referenceNo);

        [OperationContract]
        PassportApplication GetEnrollmentByFormumber(string formNumber);

        [OperationContract]
        List<PassportApplication> GetEnrollments(int type);

        [OperationContract]
        int SaveBreederDocs(string docsvariable, string formno);

        [OperationContract]
        int RejectEnrollment(string formno, string reasonid, string reason);

        [OperationContract]
        int AcceptEnrollment(string formno);

        // Screening
        [OperationContract]
        PassportApplication GetNextScreening();
    }
}
