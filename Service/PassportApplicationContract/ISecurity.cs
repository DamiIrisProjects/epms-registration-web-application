﻿using PassportApplicationCommon.Model;

namespace PassportApplicationContract
{
    public interface ISecurity
    {
        void SaveException(ExaroSystemException exception);
    }
}
