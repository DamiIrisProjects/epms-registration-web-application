﻿using System.ServiceModel;
using PassportApplicationCommon.Models;

namespace PassportApplicationContract
{
    [ServiceContract]
    public interface IPayment
    {
        //[OperationContract]
        //Payment GetPayment(int applicationID, int referenceNo);

        //[OperationContract]
        //PaymentResponse CalculatePayment(int documentType,int reason, DateTime dateOfBirth, DateTime paymentDate,bool passportExpired);
        
        [OperationContract]
        void ValidatePayment(ref PassportApplication application);

        [OperationContract]
        void CheckPaymentDetails(ref PassportApplication application);        
    }
}
