﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Caching;
using PassportApplicationCommon.Models;

namespace PassportApplicationService.Cache
{
    public static class CacheData
    {
        private static object _gsmLocker = new object();      
        private static MemoryCache _cache = MemoryCache.Default;

        private static DateTime GetCacheExpiryDate()
        {
            DateTime date;
            string time = IsNullObjectOrIsEmptyString(ConfigurationManager.AppSettings["CacheRemovalTime"], "06:00").ToString();

            if (!DateTime.TryParse(DateTime.Today.AddDays(1).ToString("yyyy/MM/dd " + time), out date))
            {
                date = Convert.ToDateTime(DateTime.Now.AddDays(1).ToString("yyyy/MM/dd 06:00"));
            }

            //If the time set is later on the same day, do it at that time, not only the next day
            if (DateTime.Now.AddHours(-date.Hour).AddMinutes(-date.Minute).Day < DateTime.Now.Day)
            {
                date = date.AddDays(-1);
            }
            return date;
        }

        private static CacheItemPolicy GetStandardCacheItemPolicy(CacheEntryRemovedCallback removedcallback)
        {
            CacheItemPolicy policy = new CacheItemPolicy();

            policy.Priority = CacheItemPriority.Default;
            policy.AbsoluteExpiration = GetCacheExpiryDate();
            policy.RemovedCallback = removedcallback;

            return policy;
        }

        private static CacheItemPolicy Get10MinuteCacheItemPolicy()
        {
            CacheItemPolicy policy = new CacheItemPolicy();

            policy.Priority = CacheItemPriority.Default;
            policy.AbsoluteExpiration = DateTime.Now.AddMinutes(10);
            
            return policy;
        }
               
        public static void Add10MinuteExpiryImage(string identifier,byte[] image)
        {
            _cache.Add(identifier,image,Get10MinuteCacheItemPolicy());
        }

        public static void Add10MinuteExpiryPassportGroup(string identifier, List<Passport> passport)
        {
            _cache.Add(identifier, passport, Get10MinuteCacheItemPolicy());
        }

        public static byte[] GetImage(string identifier)
        {
            return (byte[])_cache[identifier];
        }

        public static List<Passport> GetPassportGroup(string identifier)
        { 
            return (List<Passport>)_cache[identifier];
        }

        public static List<string> GetValidGsmCacheList()
        {
            if (_cache["CacheData.ValidGsmCacheList"] == null ||
                ((List<string>)_cache["CacheData.ValidGsmCacheList"]).Count == 0 ||
                _cache["CacheData.ValidGsmCacheList"].GetType() != typeof(List<string>))
            {
                lock (_gsmLocker)
                {
                    if (_cache["CacheData.ValidGsmCacheList"] == null ||
                        ((List<string>)_cache["CacheData.ValidGsmCacheList"]).Count == 0 ||
                        _cache["CacheData.ValidGsmCacheList"].GetType() != typeof(List<string>))
                    {
                        //MessageData messageData = new MessageData();
                        //cache.Add("CacheData.ValidGsmCacheList", messageData.GetValidGsmPrefixes(), GetStandardCacheItemPolicy(ValidGsmCacheListRemovedCallback));
                    }
                }
            }

            return (List<string>)_cache["CacheData.ValidGsmCacheList"];
        }

        private static void ValidGsmCacheListRemovedCallback(CacheEntryRemovedArguments arguments)
        {
            GetValidGsmCacheList();
        }

        public static object IsNullObjectOrIsEmptyString(object input, object defaultOutput)
        {
            if (input == null || input == DBNull.Value || input.ToString() == string.Empty)
            {
                return defaultOutput;
            }
            return input;
        }
    }
}