﻿using Exaro.Data;
using PassportApplicationCommon.Model;
using PassportApplicationCommon.Models;
using PassportApplicationContract;
using PassportApplicationData;
using PassportApplicationService.Cache;
using System;
using System.Collections.Generic;

namespace PassportApplicationService
{
    public class LookupDataService : ILookupData
    {
        public List<ProcessingCountry> GetCountries()
        {
            try
            {
                var ldata = new LookupData();

                return ldata.GetCountries();
            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }
            return null;
        }

        public List<PassportBranch> GetBranchesForCountry(string countryCode)
        {
            try
            {
                LookupData ldata = new LookupData();

                return ldata.GetBranchesForCountry(countryCode);
            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }
            return null;
        }
                
        public PassportBranch GetNigeriaMainOffice()
        {
            try
            {
                LookupData ldata = new LookupData();

                return ldata.GetNigeriaMainOffice();
            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }
            return null;
        }
        
        public List<State> GetNigeriaStates()
        {
            try
            {
                LookupData ldata = new LookupData();

                return ldata.GetNigeriaStates();
            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }
            return null;
        }

        public List<Passport> GetPreviousPassports(string previousPassportNumber)
        {
            try
            {
                LookupData ldata = new LookupData();

                return ldata.GetPreviousPassports(previousPassportNumber);
            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }
            return null;
        }
        
        public List<KeyValuePair<int, string>> GetMaritialStatusList()
        {
            try
            {
                LookupData ldata = new LookupData();

                return ldata.GetMaritialStatusList();
            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }
            return null;
        }

        public List<Gender> GetGenderList()
        {
            try
            {
                LookupData ldata = new LookupData();

                return ldata.GetGenderList();
            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }
            return null;
        }

        public List<ProcessingColour> GetHairColours()
        {
            try
            {
                LookupData ldata = new LookupData();

                return ldata.GetHairColours();
            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }
            return null;
        }

        public List<ProcessingColour> GetEyeColours()
        {
            try
            {
                LookupData ldata = new LookupData();

                return ldata.GetEyeColours();
            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }
            return null;
        }

        public List<DocumentType> GetDocumentTypes()
        {
            try
            {
                LookupData ldata = new LookupData();

                return ldata.GetDocumentTypes();
            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }
            return null;
        }

        public DocumentType GetDocumentType(int documentTypeId)
        {
            try
            {
                EnrollmentData eData = new EnrollmentData();

                return eData.GetDocumentType(documentTypeId);
            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }
            return null;
        }

        public List<BreederDoc> GetApplicationTypeDocuments(int apptype)
        {
            try
            {
                LookupData eData = new LookupData();

                return eData.GetApplicationTypeDocuments(apptype);
            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
                return null;
            }
        }

        public List<KeyValuePair<int, string>> GetApplicationReasons()
        {
            try
            {
                LookupData eData = new LookupData();

                return eData.GetApplicationReasons();
            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
                return null;
            }
        }

        public List<KeyValuePair<int, string>> GetRejectReasons()
        {
            try
            {
                LookupData ed = new LookupData();
                return ed.GetRejectReasons();

            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }

            return null;
        }

        public List<Passport> GetPassportHistory(string previousPassportNumber)
        {
            try
            {
                LookupData ed = new LookupData();
                List<Passport> passportList = ed.GetPassportHistory(previousPassportNumber);

                // Save images in cache
                string identifier = Guid.NewGuid().ToString();
                CacheData.Add10MinuteExpiryPassportGroup(identifier, passportList);

                if (passportList.Count > 0)
                {
                    foreach (Passport passport in passportList)
                    {
                        //Add Images to cache
                        if (passport.TempFace != null)
                            CacheData.Add10MinuteExpiryImage(passport.DocNo + "F", passport.TempFace);

                        if (passport.TempSign != null)
                            CacheData.Add10MinuteExpiryImage(passport.DocNo + "S", passport.TempSign);
                        
                        passport.IdentificationGuid = identifier;

                        //Clear Images
                        passport.TempFace = null;
                        passport.TempSign = null;

                        
                        //passport.StageDescription = vData.GetStageDescription(passport.StageCode);   // Avoiding DB for demo                         
                        passport.StageDescription = new LookupData().GetStageDescription(passport.StageCode);   
                    }

                    return passportList;
                }    

            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }

            return null;
        }

        public byte[] GetPassportImage(string passportNo, bool isFaceImage)
        {
            try
            {
                var image = CacheData.GetImage(passportNo + (isFaceImage ? "F" : "S"));

                if (image != null && image.Length < 100)
                    image = null;

                return image;
            }
            catch (Exception ex)
            {
                new ErrorHandler().SaveError(ex, Environment.MachineName);
            }
            return null;
        }        

    }
}
