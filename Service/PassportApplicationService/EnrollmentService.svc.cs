﻿using Exaro.Data;
using PassportApplicationCommon.Enum;
using PassportApplicationCommon.Models;
using PassportApplicationContract;
using PassportApplicationData;
using System;
using System.Collections.Generic;

namespace PassportApplicationService
{
    public class EnrollmentService : IEnrollment
    {       
        public Passport GetPassport(string previousPassportNumber)
        {
            try
            {

            }
            catch (Exception ex)
            {
                new ErrorHandler().SaveError(ex, Environment.MachineName);
            }
            return null;
        }

        public PassportApplication SaveEnrollment(PassportApplication application)
        {
            try
            {
                var ed = new EnrollmentData();

                return ed.SaveEnrollment(ref application);
            }
            catch (Exception ex)
            {
                new ErrorHandler().SaveError(ex, Environment.MachineName);
            }

            return null;
        }

        public PassportApplication GetEnrollment(int applicationId, int referenceNo)
        {
            try
            {
                var ed = new EnrollmentData();

                return ed.GetEnrollment(applicationId, referenceNo);

            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }
            return null;
        }

        public PassportApplication GetEnrollmentByFormumber(string formNumber)
        {
            try
            {
                var ed = new EnrollmentData();

                return ed.GetEnrollmentByFormNumber(formNumber);

            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }
            return null;
        }

        public List<PassportApplication> GetEnrollments(int type)
        {
            try
            {
                var ed = new EnrollmentData();
                return ed.GetEnrollments(type);

            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }

            return null;
        }
       
        public int SaveBreederDocs(string docsvariable, string formno)
        {
            try
            {
                var ed = new EnrollmentData();

                return ed.SaveBreederDocs(docsvariable, formno);

            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }

            return 0;
        }

        public int RejectEnrollment(string formno, string reasonid, string reason)
        {
            try
            {
                var ed = new EnrollmentData();

                return ed.RejectEnrollment(formno, reasonid, reason);

            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }

            return 0;
        }

        public int AcceptEnrollment(string formno)
        {
            try
            {
                var ed = new EnrollmentData();

                return ed.AcceptEnrollment(formno);

            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }

            return 0;
        }

        // Screening
        public PassportApplication GetNextScreening()
        {
            try
            {
                var ed = new EnrollmentData();

                return ed.GetNextScreening();

            }
            catch (Exception ex)
            {
                new ErrorHandler().SaveError(ex, Environment.MachineName);
            }
            return null;
        }

        #region Operations

        private Payment GetPayment(int applicationId, int referenceNo)
        {
            try
            {
                var pd = new PaymentData();

                return pd.GetPayment(applicationId, referenceNo);
            }
            catch (Exception ex)
            {
                new ErrorHandler().SaveError(ex, Environment.MachineName);
            }
            return null;
        }

        private static void SetPaymentFields(PaymentInformation application, PaymentInformation payment, PayCurrency currency, DateTime paymentDate, int paymentAmount, string paymentName)
        {
            application.PayeDue = payment.CalculatedBasePrice;
            application.PaycDue = payment.CalculatedAdditionalPrice;

            application.PaymentStatus = (int)PayStatusEnrollment.Null;
            application.PayeStatus = (int)PayStatusEnrollment.Null;

            if (!payment.PaymentExist)
            {
                application.PaymentStatus = (int)PayStatusEnrollment.Outstanding;
                application.PayeStatus = (int)PayStatusEnrollment.Outstanding;
            }
            else
            {
                if (payment.PaymentMatch)
                {
                    application.PaymentStatus = (int)PayStatusEnrollment.Verified;
                    application.PayeStatus = (int)PayStatusEnrollment.Verified;
                }
                else
                {
                    application.PaymentStatus = (int)PayStatusEnrollment.Different;
                    application.PayeStatus = (int)PayStatusEnrollment.Different;
                }
            }

            application.PayeSysConfirmdate = DateTime.Now;

            //The field should be replaced when dami's user management system is available
            application.PayeSysConfirmWho = "System User";

            application.PayeDatepaid = paymentDate;
            application.PayeAmount = paymentAmount;

            application.PayeNamesurname = paymentName;

            //Implement pay status decisions
            application.PaycStatus = (int)PayStatusEnrollment.Null;

            application.PaycSysConfirmdatetime = DateTime.Now;

            //The field should be replaced when dami's user management system is available
            application.PaycSysConfirmwho = "System User";

        }

        private DocumentType GetDocumentType(int documentTypeId)
        {
            if (documentTypeId == 0)
                documentTypeId = 1;

            var lp = new LookupDataService();

            return lp.GetDocumentType(documentTypeId);
        }

        private PaymentResponse CalculatePayment(int documentType, int reason, DateTime dateOfBirth, DateTime paymentDate, bool passportExpired)
        {
            try
            {
                var response = new PaymentResponse();

                var ed = new EnrollmentData();

                var docType = ed.GetDocumentType(documentType);
                var appReason = (ApplicationReason)reason;

                var pd = new PaymentData();

                //P = Ordinary Passport/Standard
                if (docType.DocTypeCode == "P")
                {
                    if (docType.Pages == 32)
                    {
                        int age = (DateTime.Now - dateOfBirth).Days / 365;

                        if (age < 18 || age > 60)
                        {
                            //₦8,750
                            response.BaseAmountNaira = pd.GetPaymentAmount(1, paymentDate);
                            //$65
                            response.BaseAmountUsd = pd.GetPaymentAmount(2, paymentDate);
                        }
                        else if (!passportExpired && appReason == ApplicationReason.ReissueChangeOfInformationDueToMarriage)
                        {
                            //₦8,750
                            response.BaseAmountNaira = pd.GetPaymentAmount(1, paymentDate);
                            //$65
                            //Assume reissue due to marriage fee the same for USD amount as we don't have proper rules
                            response.BaseAmountUsd = pd.GetPaymentAmount(2, paymentDate);
                        }
                        else
                        {
                            //18-60 years
                            //₦15,000
                            response.BaseAmountNaira = pd.GetPaymentAmount(3, paymentDate);
                            //$94
                            response.BaseAmountUsd = pd.GetPaymentAmount(4, paymentDate);
                        }
                    }
                    else if (docType.Pages == 64)
                    {
                        //₦20,000
                        response.BaseAmountNaira = pd.GetPaymentAmount(5, paymentDate);
                        //$137
                        response.BaseAmountUsd = pd.GetPaymentAmount(6, paymentDate);
                    }
                }
                //Official - Only done in nigeria, so only Naira amounts applicable
                if (docType.DocTypeCode == "PF")
                {
                    if (docType.Pages == 32)
                    {
                        //₦8,500
                        response.BaseAmountNaira = pd.GetPaymentAmount(7, paymentDate);
                    }
                    else
                    {
                        //64 Pages
                        //₦15,500
                        response.BaseAmountNaira = pd.GetPaymentAmount(8, paymentDate);
                    }
                }

                //Diplomatic
                if (docType.DocTypeCode == "PD")
                {
                    throw new ApplicationException("Diplomatic passports does not pay and should not be calculated.");
                }

                //Special cases - CONFIRMED BY MINA THAT IS IS NOT DONE OUTSIDE NIGERIA
                if (appReason == ApplicationReason.ReissueLost || appReason == ApplicationReason.ReissueStolen)
                {
                    //₦20,000	
                    response.ExtraAmountNaira = pd.GetPaymentAmount(9, paymentDate);
                }

                if (appReason == ApplicationReason.ReissueChangeOfInformationDueToOtherReasons)
                {
                    //₦30,000	
                    response.ExtraAmountNaira = pd.GetPaymentAmount(11, paymentDate);
                }

                return response;
            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }
            return null;
        }

        #endregion
    }
}
