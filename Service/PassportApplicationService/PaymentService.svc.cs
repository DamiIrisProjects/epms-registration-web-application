﻿using Exaro.Data;
using PassportApplicationCommon.Enum;
using PassportApplicationCommon.Helpers;
using PassportApplicationCommon.Models;
using PassportApplicationContract;
using PassportApplicationData;
using System;

namespace PassportApplicationService
{
    public class PaymentService : IPayment
    {
        public void ValidatePayment(ref PassportApplication application)
        {
            application.ErrorMessage = string.Empty;

            try
            {
                // Validate EnrollmentLookup
                if (!GetDocumentType(application.PassportInformation.DocumentType).DocTypeCode.Contains("PD"))
                {
                    Payment payment = GetPayment(application.Payment.ApplicationId, application.Payment.ReferenceNumber);

                    //Check if payment does not exist
                    if (payment == null || (payment.PaymentStatus == PaymentStatus.Used && string.IsNullOrEmpty(payment.FormNo)))
                    {
                        //Add Error message to Specific step                    
                        application.ErrorMessage = "The payment could not be verified. Please make sure the payment is allocated " +
                                                   "against the correct branch and unused.";
                        application.Payment.PaymentExist = false;
                        return;
                    }
                    if (application.Payment != null)
                        application.Payment.PaymentExist = true;

                    // Ensure we have names
                    if (string.IsNullOrEmpty(application.Payment.Firstname) || string.IsNullOrEmpty(application.Payment.Surname))
                    {
                        application.ErrorMessage = "Please provide a valid firstname and surname under personal details.";
                        return;
                    }

                    // The algorithm compares strings. The result is 1 if it is a 100% match.
                    double result = Sorensen.DiceCoefficient(payment.Firstname, application.Payment.Firstname);
                    if (result < 0.8)
                    {
                        application.Payment.FirstnameMatch = false;
                        application.ErrorMessage = "The firstname provided does not match the firstname on the payment.";
                        return;
                    }
                    // If it is not a 100% match, pass information back to allow operator make a decision
                    if (result == 1)
                    {
                        application.Payment.FirstnameMatch = true;
                    }
                    else
                    {
                        application.Payment.FirstnameCoefficient = result;
                        application.Payment.PaymentFirstname = payment.Firstname;
                    }

                    result = Sorensen.DiceCoefficient(payment.LastName, application.Payment.Surname);
                    if (result < 0.8)
                    {
                        application.Payment.SurnameMatch = false;
                        application.ErrorMessage = "The surname provided does not match the surname on the payment.";
                        return;
                    }
                    if (result == 1)
                    {
                        application.Payment.SurnameMatch = true;
                    }
                    else
                    {
                        application.Payment.SurnameCoefficient = result;
                        application.Payment.PaymentSurname = payment.LastName;
                    }

                    // Check DOB matches
                    if (!string.Equals(application.Payment.Gender, payment.Gender, StringComparison.CurrentCultureIgnoreCase))
                    {
                        application.Payment.GenderMatch = false;
                        application.ErrorMessage = "The gender provided does not match the gender on the payment.";
                        return;
                    }
                    application.Payment.GenderMatch = true;

                    // Check Gender matches
                    if (application.Payment.DateOfBirth != payment.Dob)
                    {
                        application.Payment.DobMatch = false;
                        application.ErrorMessage = "The date of birth provided does not match the date of birth on the payment.";
                        return;
                    }
                    application.Payment.DobMatch = true;

                    // Calculate amount to be paid
                    var response = CalculatePayment(application.PassportInformation.DocumentType, 
                        application.PassportInformation.ApplicationType, application.Payment.DateOfBirth, payment.PaymentDate, false);

                    if (payment.AmountDollar > 0)
                    {
                        application.Payment.PaymentTotalPrice = payment.AmountDollar;
                        application.Payment.CalculatedAdditionalPrice = response.ExtraAmountUsd;
                        application.Payment.CalculatedBasePrice = response.BaseAmountUsd;

                        SetPaymentFields(application.Payment, application.Payment, payment.PaymentDate, payment.AmountDollar, 
                            payment.Firstname + " " + payment.LastName);

                        if (payment.AmountDollar != response.TotalAmountUsd)
                        {
                            application.ErrorMessage = "The payment amount does not match the amount due.";
                            application.Payment.PaymentMatch = false;
                            return;
                        }
                        application.Payment.PaymentMatch = true;
                    }
                    else if (payment.AmountNaira > 0)
                    {
                        application.Payment.PaymentTotalPrice = payment.AmountNaira;
                        application.Payment.CalculatedAdditionalPrice = response.ExtraAmountNaira;
                        application.Payment.CalculatedBasePrice = response.BaseAmountNaira;

                        SetPaymentFields(application.Payment, application.Payment, payment.PaymentDate, payment.AmountNaira, payment.Firstname + " " + payment.LastName);

                        if (payment.AmountNaira != response.TotalAmountNaira)
                        {
                            application.ErrorMessage = "The payment amount does not match the amount due.";
                            application.Payment.PaymentMatch = false;
                            application.Payment.AmountFound = payment.AmountNaira;
                            application.Payment.AmountExpected = response.TotalAmountNaira;
                            return;
                        }
                        application.Payment.PaymentMatch = true;
                    }

                    // If everything is fine, then mark the payment as used and assign it a form number
                    new PaymentData().SavePaymentReference(application.Payment.ApplicationId, application.Payment.ReferenceNumber, 
                        application.FormNo, true);
                }
                else
                {
                    application.Payment.PaymentStatus = (int)PayStatusEnrollment.NotRequired;
                }
            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }
        }

        public void CheckPaymentDetails(ref PassportApplication application)
        {
            try
            {
                // Validate EnrollmentLookup
                if (!GetDocumentType(application.PassportInformation.DocumentType).DocTypeCode.Contains("PD"))
                {
                    var payment = GetPaymentDetails(application.Payment.ApplicationId, application.Payment.ReferenceNumber);

                    if (!string.IsNullOrEmpty(payment?.Firstname))
                    {
                        application.Payment.PaymentExist = true;

                        // The algorithm compares strings. The result is 1 if it is a 100% match.
                        double result = Sorensen.DiceCoefficient(payment.Firstname, application.Payment.Firstname);
                        if (result < 0.8)
                        {
                            application.Payment.FirstnameMatch = false;
                            application.ErrorMessage = "The firstname provided does not match the firstname on the payment.";
                            return;
                        }
                        // If it is not a 100% match, pass information back to allow operator make a decision
                        if (result == 1)
                        {
                            application.Payment.FirstnameMatch = true;
                        }
                        else
                        {
                            application.Payment.FirstnameCoefficient = result;
                            application.Payment.PaymentFirstname = payment.Firstname;
                        }

                        result = Sorensen.DiceCoefficient(payment.LastName, application.Payment.Surname);
                        if (result < 0.8)
                        {
                            application.Payment.SurnameMatch = false;
                            application.ErrorMessage = "The surname provided does not match the surname on the payment.";
                            return;
                        }
                        if (result == 1)
                        {
                            application.Payment.SurnameMatch = true;
                        }
                        else
                        {
                            application.Payment.SurnameCoefficient = result;
                            application.Payment.PaymentSurname = payment.LastName;
                        }

                        // Check DOB matches
                        if (!string.Equals(application.Payment.Gender, payment.Gender, StringComparison.CurrentCultureIgnoreCase))
                        {
                            application.Payment.GenderMatch = false;
                            application.ErrorMessage = "The gender provided does not match the gender on the payment.";
                            return;
                        }
                        application.Payment.GenderMatch = true;

                        // Check Gender matches
                        if (application.Payment.DateOfBirth != payment.Dob)
                        {
                            application.Payment.DobMatch = false;
                            application.ErrorMessage = "The date of birth provided does not match the date of birth on the payment.";
                            return;
                        }
                        application.Payment.DobMatch = true;

                        //Calculate amount to be paid
                        var response = CalculatePayment(application.PassportInformation.DocumentType, application.PassportInformation.ApplicationType, 
                            application.Payment.DateOfBirth, payment.PaymentDate, false);

                        if (payment.AmountDollar > 0)
                        {
                            application.Payment.PaymentTotalPrice = payment.AmountDollar;
                            application.Payment.CalculatedAdditionalPrice = response.ExtraAmountUsd;
                            application.Payment.CalculatedBasePrice = response.BaseAmountUsd;

                            SetPaymentFields(application.Payment, application.Payment, payment.PaymentDate, payment.AmountDollar, payment.Firstname + " " + payment.LastName);

                            if (payment.AmountDollar != response.TotalAmountUsd)
                            {
                                application.ErrorMessage = "The payment amount does not match the amount due.";
                                application.Payment.PaymentMatch = false;
                                return;
                            }
                            application.Payment.PaymentMatch = true;
                        }
                        else if (payment.AmountNaira > 0)
                        {
                            application.Payment.PaymentTotalPrice = payment.AmountNaira;
                            application.Payment.CalculatedAdditionalPrice = response.ExtraAmountNaira;
                            application.Payment.CalculatedBasePrice = response.BaseAmountNaira;

                            SetPaymentFields(application.Payment, application.Payment, payment.PaymentDate, payment.AmountNaira, payment.Firstname + " " + payment.LastName);

                            if (payment.AmountNaira != response.TotalAmountNaira)
                            {
                                application.ErrorMessage = "The payment amount does not match the amount due.";
                                application.Payment.PaymentMatch = false;
                                application.Payment.AmountFound = payment.AmountNaira;
                                application.Payment.AmountExpected = response.TotalAmountNaira;
                                return;
                            }
                            application.Payment.PaymentMatch = true;
                        }
                    }
                }
                else
                {
                    application.Payment.PaymentStatus = (int)PayStatusEnrollment.NotRequired;
                }
            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }
        } 


        #region Operations

        private Payment GetPayment(int applicationId, int referenceNo)
        {
            try
            {
                PaymentData pd = new PaymentData();

                return pd.GetPayment(applicationId, referenceNo);
            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }
            return null;
        }

        private Payment GetPaymentDetails(int applicationId, int referenceNo)
        {
            try
            {
                var pd = new PaymentData();

                return pd.GetPaymentDetails(applicationId, referenceNo);
            }
            catch (Exception ex)
            {
                new ErrorHandler().SaveError(ex, Environment.MachineName);
            }
            return null;
        }

        private static void SetPaymentFields(PaymentInformation application, PaymentInformation payment, DateTime paymentDate, int paymentAmount, string paymentName)
        {
            application.PayeDue = payment.CalculatedBasePrice;
            application.PaycDue = payment.CalculatedAdditionalPrice;

            application.PaymentStatus = (int)PayStatusEnrollment.Null;
            application.PayeStatus = (int)PayStatusEnrollment.Null;

            if (!payment.PaymentExist)
            {
                application.PaymentStatus = (int)PayStatusEnrollment.Outstanding;
                application.PayeStatus = (int)PayStatusEnrollment.Outstanding;
            }
            else
            {
                if (payment.PaymentMatch)
                {
                    application.PaymentStatus = (int)PayStatusEnrollment.Verified;
                    application.PayeStatus = (int)PayStatusEnrollment.Verified;
                }
                else
                {
                    application.PaymentStatus = (int)PayStatusEnrollment.Different;
                    application.PayeStatus = (int)PayStatusEnrollment.Different;
                }
            }

            application.PayeSysConfirmdate = DateTime.Now;

            //The field should be replaced when dami's user management system is available
            application.PayeSysConfirmWho = "System User";

            application.PayeDatepaid = paymentDate;
            application.PayeAmount = paymentAmount;

            application.PayeNamesurname = paymentName;

            //Implement pay status decisions
            application.PaycStatus = (int)PayStatusEnrollment.Null;

            application.PaycSysConfirmdatetime = DateTime.Now;

            //The field should be replaced when dami's user management system is available
            application.PaycSysConfirmwho = "System User";

        }

        private DocumentType GetDocumentType(int documentTypeId)
        {
            if (documentTypeId == 0)
                documentTypeId = 1;

            LookupDataService lp = new LookupDataService();

            return lp.GetDocumentType(documentTypeId);
        }

        private PaymentResponse CalculatePayment(int documentType, int reason, DateTime dateOfBirth, DateTime paymentDate, bool passportExpired)
        {
            try
            {
                PaymentResponse response = new PaymentResponse();

                EnrollmentData ed = new EnrollmentData();

                DocumentType docType = ed.GetDocumentType(documentType);
                ApplicationReason appReason = (ApplicationReason)reason;

                PaymentData pd = new PaymentData();

                //P = Ordinary Passport/Standard
                if (docType.DocTypeCode == "P")
                {
                    if (docType.Pages == 32)
                    {
                        int age = (DateTime.Now - dateOfBirth).Days / 365;

                        if (age < 18 || age > 60)
                        {
                            //₦8,750
                            response.BaseAmountNaira = pd.GetPaymentAmount(1, paymentDate);
                            //$65
                            response.BaseAmountUsd = pd.GetPaymentAmount(2, paymentDate);
                        }
                        else if (!passportExpired && appReason == ApplicationReason.ReissueChangeOfInformationDueToMarriage)
                        {
                            //₦8,750
                            response.BaseAmountNaira = pd.GetPaymentAmount(1, paymentDate);
                            //$65
                            //Assume reissue due to marriage fee the same for USD amount as we don't have proper rules
                            response.BaseAmountUsd = pd.GetPaymentAmount(2, paymentDate);
                        }
                        else
                        {
                            //18-60 years
                            //₦15,000
                            response.BaseAmountNaira = pd.GetPaymentAmount(3, paymentDate);
                            //$94
                            response.BaseAmountUsd = pd.GetPaymentAmount(4, paymentDate);
                        }
                    }
                    else if (docType.Pages == 64)
                    {
                        //₦20,000
                        response.BaseAmountNaira = pd.GetPaymentAmount(5, paymentDate);
                        //$137
                        response.BaseAmountUsd = pd.GetPaymentAmount(6, paymentDate);
                    }
                }
                //Official - Only done in nigeria, so only Naira amounts applicable
                if (docType.DocTypeCode == "PF")
                {
                    if (docType.Pages == 32)
                    {
                        //₦8,500
                        response.BaseAmountNaira = pd.GetPaymentAmount(7, paymentDate);
                    }
                    else
                    {
                        //64 Pages
                        //₦15,500
                        response.BaseAmountNaira = pd.GetPaymentAmount(8, paymentDate);
                    }
                }

                //Diplomatic
                if (docType.DocTypeCode == "PD")
                {
                    throw new ApplicationException("Diplomatic passports does not pay and should not be calculated.");
                }

                //Special cases - CONFIRMED BY MINA THAT IS IS NOT DONE OUTSIDE NIGERIA
                if (appReason == ApplicationReason.ReissueLost || appReason == ApplicationReason.ReissueStolen)
                {
                    //₦20,000	
                    response.ExtraAmountNaira = pd.GetPaymentAmount(9, paymentDate);
                }

                if (appReason == ApplicationReason.ReissueChangeOfInformationDueToOtherReasons)
                {
                    //₦30,000	
                    response.ExtraAmountNaira = pd.GetPaymentAmount(11, paymentDate);
                }

                return response;
            }
            catch (Exception ex)
            {
                (new ErrorHandler()).SaveError(ex, Environment.MachineName);
            }
            return null;
        } 

        #endregion
    }
}
