﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data.OracleClient;
using System.Configuration;
using System.Runtime.Serialization;
using PassportApplicationCommon.Model;
//using log4net;

namespace Exaro.Data
{
    

    public sealed class LogSystemError
    {
        //private Queue<ExaroSystemException> exceptionQueue = new Queue<ExaroSystemException>();
        static LogSystemError instance = null;
        static readonly object padlock = new object();

        LogSystemError()
        {
        }
        
        public void SaveError(ExaroSystemException exceptionInfo)
        {
            TrimExceptionLength(exceptionInfo);

            ErrorHandler se = new ErrorHandler();
            se.SaveErrorToDisk(exceptionInfo);
            //instance.exceptionQueue.Enqueue(exceptionDetails);                        
        }

        private void TrimExceptionLength(ExaroSystemException exceptionInfo)
        {
            exceptionInfo.Browser = (exceptionInfo.Browser.Length > 100 ? exceptionInfo.Browser.Substring(0, 100) : exceptionInfo.Browser);
            exceptionInfo.Message = (exceptionInfo.Message.Length > 1000 ? exceptionInfo.Message.Substring(0, 1000) : exceptionInfo.Message);
            exceptionInfo.Stacktrace = (exceptionInfo.Stacktrace.Length > 4000 ? exceptionInfo.Stacktrace.Substring(0, 4000) : exceptionInfo.Stacktrace);
            exceptionInfo.MachineNameOrIP = (exceptionInfo.MachineNameOrIP.Length > 100 ? exceptionInfo.MachineNameOrIP.Substring(0, 100) : exceptionInfo.MachineNameOrIP);
            exceptionInfo.InnerExceptionMessage = (exceptionInfo.InnerExceptionMessage.Length > 1000 ? exceptionInfo.InnerExceptionMessage.Substring(0, 1000) : exceptionInfo.InnerExceptionMessage);
            exceptionInfo.InnerExceptionStackTrace = (exceptionInfo.InnerExceptionStackTrace.Length > 4000 ? exceptionInfo.InnerExceptionStackTrace.Substring(0, 4000) : exceptionInfo.InnerExceptionStackTrace);            
        }

        public void SaveError(Exception ex, string machineName)
        {
            ErrorHandler se = new ErrorHandler();
            se.SaveError(ex,machineName);
            //instance.exceptionQueue.Enqueue(exceptionDetails);                        
        }

        public static LogSystemError Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new LogSystemError();
                    }
                    return instance;
                }
            }
        }
    }

    public class ErrorHandler:ServiceData
    {
        //Log4net removed since it is clashing with the log4net dami is using. His projects won't compile
        
        //private static ILog log = LogManager.GetLogger(typeof(ErrorHandler));
        //private static ILog exceptionlog = LogManager.GetLogger("Exaro.ErrorLog", "ErrorHandlerException");

        public ErrorHandler()
        {
            //This is done to connect with different connectionstrings for different application purposes.
            ConnectionString = ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ApplicationConnection"].ConnectionString;
        }

        public void SaveError(Exception ex, string machineName)
        {
            ExaroSystemException exception = new ExaroSystemException();
            exception.MachineNameOrIP = machineName;
            exception.Message = string.IsNullOrEmpty(ex.Message) ? "" : ex.Message;
            exception.Stacktrace = ex.StackTrace == null ? "" : ex.StackTrace.ToString();

            if (ex.InnerException != null)
            {
                exception.InnerExceptionMessage = string.IsNullOrEmpty(ex.InnerException.Message) ? "" : ex.InnerException.Message;
                exception.InnerExceptionStackTrace = ex.InnerException.StackTrace == null ? "" : ex.InnerException.StackTrace.ToString();
            }

            SaveErrorToDisk(exception);
        }

        public void SaveErrorToDisk(ExaroSystemException exception)
        {
            try
            {
                string qry = @"Insert into EXCEPTIONLOG(ID,EXCEPTIONDATE,MESSAGE,STACKTRACE,MACHINENAMEORIP,InnerMessage,InnerStacktrace,Browser)
                            values(EXCEPTIONLOGID.nextval,sysdate,:MESSAGE,:STACKTRACE,:MACHINENAMEORIP,:InnerMessage,:InnerStacktrace,:BROWSER)";

                OracleParameter[] insertParameters =
                {                 
                    new OracleParameter("MESSAGE", exception.Message),
                    new OracleParameter("STACKTRACE", exception.Stacktrace),
                    new OracleParameter("InnerMessage", exception.InnerExceptionMessage),
                    new OracleParameter("InnerStacktrace", exception.InnerExceptionStackTrace),
                    new OracleParameter("MACHINENAMEORIP", exception.MachineNameOrIP),
                    new OracleParameter("BROWSER", exception.Browser)                
                };

                ExecuteQuery(qry, insertParameters);

                //SaveErrorToLogFile(exception);

                //try
                //{
                //    string saveErrorsToText = ConfigurationManager.AppSettings["SaveErrorsToText"].ToString();
                //    if (saveErrorsToText.ToUpper() == "TRUE")
                //        SaveErrorToTextFile("Original - " + exception.Message + "  " + exception.MachineNameOrIP + "                 " + exception.Stacktrace);
                //}
                //catch { }
            }
            catch (Exception ex2)
            {
                //try
                //{
                //    exceptionlog.Error("Original Message: " + exception.Message + "\r\n" +
                //                       "Original Stacktrace: " + exception.Stacktrace + "\r\n" +
                //                       "Original InnerExceptionMessage: " + exception.InnerExceptionMessage + "\r\n" +
                //                       "Original InnerExceptionStackTrace: " + exception.InnerExceptionStackTrace  + "\r\n" +
                //                       "New on error save Message: " + ex2.Message + "\r\n" +
                //                       "New on error save Stacktrace: " + ex2.StackTrace + "\r\n" +
                //                       "New on error save InnerExceptionMessage: " + ex2.InnerException == null ? "" : ex2.InnerException.Message + "\r\n" +
                //                       "New on error save InnerExceptionStackTrace: " + ex2.InnerException == null ? "" : ex2.InnerException.StackTrace);
                //}
                //catch { }

                string dbErrorString = BuildErrorString(ex2,exception.MachineNameOrIP);

                string actualErrorString = BuildErrorStringExaro(exception, exception.MachineNameOrIP);
                                
                SaveErrorToTextFile(dbErrorString,true);

                SaveErrorToTextFile(actualErrorString, false);
            }
        }

        private string BuildErrorString(Exception exception,string machineName)
        {
            string errorString = "Message " + exception.Message 
                            +" StackTrace: " + exception.StackTrace 
                            +" Machine Name: " + machineName;

            if (exception.InnerException != null)
            {
                errorString += "Inner Exception " + BuildErrorString(exception.InnerException, string.Empty);
            }

            return errorString;
        }

        private string BuildErrorStringExaro(ExaroSystemException exception, string machineName)
        {
            string errorString = "Message " + exception.Message
                            + " StackTrace: " + exception.Stacktrace
                            + " Machine Name: " + machineName;

            if (!string.IsNullOrEmpty(exception.InnerExceptionMessage))
            {
                errorString += "Inner Exception Message " + exception.InnerExceptionMessage
                            + "Inner Exception StackTrace: " + exception.InnerExceptionStackTrace;
            }

            return errorString;
        }

        //private static void SaveErrorToLogFile(ExaroSystemException exception)
        //{
        //    try
        //    {
        //        log.Error("Message: " + exception.Message + "\r\n" +
        //                  "Stacktrace: " + exception.Stacktrace + "\r\n" +
        //                  "InnerExceptionMessage: " + exception.InnerExceptionMessage + "\r\n" +
        //                  "InnerExceptionStackTrace: " + exception.InnerExceptionStackTrace + "\r\n" +
        //                  "MachineNameOrIP: " + exception.MachineNameOrIP + "\r\n" +
        //                  "Browser: " + exception.Browser + "\r\n");
        //    }
        //    catch {}
        //}

        public static void SaveErrorToTextFile(string error,bool dbError)
        {
            string db = "ServiceErrors";

            if(dbError)
                db = "SaveRoDbErrors";

            string errorPath = @"c:\ServiceErrors\"+db+DateTime.Now.ToString("yyyyMMdd")+".txt";
            
            StreamWriter sw = new StreamWriter(errorPath,true);
            sw.WriteLine(DateTime.Now.ToShortTimeString() + " - " + error);
            sw.Flush();
            sw.Close();
        }
  
    }
}
