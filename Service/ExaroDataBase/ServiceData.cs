﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.IO;

namespace Exaro.Data
{
    public class ServiceData
    {
        //public string ConnectionString
        //{
        //    get
        //    {
        //        string conn = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"];
        //        return conn;
        //    }
        //}

        public string ConnectionString
        {
            set
            {
                //connectionString
                oracleConnection = new OracleConnection(value);
                oracleSelectCommand = new OracleCommand(string.Empty, oracleConnection);
                connectionstring = value;
            }
        }

        private OracleConnection oracleConnection;
        private OracleCommand oracleSelectCommand;
        private string connectionstring;

        public ServiceData()
        {
        }

        protected IDataReader SelectQuery(string strQuery, IDataParameter[] parameters)
        {
            if (oracleConnection == null || oracleConnection.ConnectionString == string.Empty)
                throw new ApplicationException("Please provide a connection string before executing a query.");

            oracleConnection.Open();
            oracleSelectCommand.CommandText = strQuery.Replace("\r\n", "\n");

            oracleSelectCommand.Parameters.Clear();

            if (parameters != null && parameters.Length > 0)
                oracleSelectCommand.Parameters.AddRange(ConvertNullToDBNullValue(parameters));

            return oracleSelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
        }

        protected DataSet SelectQuery(string strQuery, IDataParameter[] parameters, Type datasetType, string tableName)
        {
            if (oracleConnection == null || oracleConnection.ConnectionString == string.Empty)
                throw new ApplicationException("Please provide a connection string before executing a query.");

            var dataset = (DataSet)Activator.CreateInstance(datasetType);

            oracleConnection.Open();

            try
            {
                oracleSelectCommand.CommandText = strQuery.Replace("\r\n", "\n");

                oracleSelectCommand.Parameters.Clear();

                if (parameters != null && parameters.Length > 0)
                    oracleSelectCommand.Parameters.AddRange(ConvertNullToDBNullValue(parameters));

                OracleDataAdapter adapter = new OracleDataAdapter(oracleSelectCommand);

                adapter.Fill(dataset, tableName);
            }
            catch { throw; }
            finally
            {
                try
                {
                    oracleConnection.Close();
                }
                catch { }

            }
            return dataset;
        }

        protected void SelectQuery(string strQuery, IDataParameter[] parameters, string tableName, ref DataSet dataset)
        {
            if (oracleConnection == null || oracleConnection.ConnectionString == string.Empty)
                throw new ApplicationException("Please provide a connection string before executing a query.");

            oracleConnection.Open();

            try
            {
                oracleSelectCommand.CommandText = strQuery.Replace("\r\n", "\n");

                oracleSelectCommand.Parameters.Clear();

                if (parameters != null && parameters.Length > 0)
                    oracleSelectCommand.Parameters.AddRange(ConvertNullToDBNullValue(parameters));

                OracleDataAdapter adapter = new OracleDataAdapter(oracleSelectCommand);

                adapter.Fill(dataset, tableName);
            }
            catch { throw; }
            finally
            {
                try
                {
                    oracleConnection.Close();
                }
                catch { }
            }
        }

        protected int ExecuteQuery(string strQuery, IDataParameter[] parameters)
        {
            try
            {
                if (oracleConnection.ConnectionString == string.Empty)
                    throw new ApplicationException("Please provide a connection string before executing a query.");

                int iRowsAffected = -1;

                oracleConnection.Open();
                using (OracleCommand oracleQuery = new OracleCommand(strQuery.Replace("\r\n", "\n")))
                {
                    if (parameters != null && parameters.Length > 0)
                        oracleQuery.Parameters.AddRange(ConvertNullToDBNullValue(parameters));

                    oracleQuery.Connection = oracleConnection;
                    iRowsAffected = oracleQuery.ExecuteNonQuery();
                    oracleQuery.Dispose();
                }
                return iRowsAffected;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //if (oracleConnection.State != ConnectionState.Closed)
                //{
                try
                {
                    oracleConnection.Close();
                }
                catch { }
                //}
            }
        }

        protected int ExecuteBulkQuery<T>(string query, IDataParameter[] parameters, List<T> list) where T : class
        {
            OracleConnection connection = new OracleConnection(connectionstring);
            int rowsupdated = 0;
            try
            {
                if (connectionstring == string.Empty)
                {
                    throw new ApplicationException("Please provide a connection string before executing a query.");
                }

                var properties = typeof(T).GetProperties();

                connection.Open();
                using (OracleCommand command = new OracleCommand(query.Replace("\r\n", "\n")))
                {
                    foreach (T model in list)
                    {
                        command.Parameters.Clear();
                        foreach (IDataParameter parameter in parameters)
                        {
                            parameter.Value = null;

                            var property = properties.Where(x => x.Name == parameter.ParameterName).Single();
                            parameter.Value = property.GetValue(model, null);
                        }

                        command.Parameters.AddRange(ConvertNullToDBNullValue(parameters));
                        command.Connection = connection;
                        rowsupdated += command.ExecuteNonQuery();
                    }
                }
                return rowsupdated;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    try
                    {
                        connection.Close();
                    }
                    catch { }
                }
            }
        }

        protected bool CheckConnection()
        {
            if (oracleConnection.ConnectionString == string.Empty)
                throw new ApplicationException("Please provide a connection string before executing a query.");

            try
            {
                try
                {
                    oracleConnection.Open();
                    return true;
                }
                catch
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //if (oracleConnection.State != ConnectionState.Closed)
                //{
                try
                {
                    oracleConnection.Close();
                }
                catch { }
                //}
            }
        }

        protected void CloseQuery()
        {
            try
            {
                //if (oracleConnection.State != ConnectionState.Closed)
                //{
                   oracleConnection.Close();
                //}
            }
            catch (Exception ex)
            {
                try
                {
                    SaveCloseError(ex, "Data");
                }
                catch { }

                throw ex;
            }
        }

        protected long GetNextSequenceID(string sequenceName)
        {
            Int64 sequenceNumber = 0;

            string query = String.Format("SELECT {0}.nextval as NEXTSEQ from dual", sequenceName);

            try
            {
                OracleDataReader oReader = (OracleDataReader)SelectQuery(query, null);

                if (oReader.Read())
                {
                    string readerString = oReader["NEXTSEQ"].ToString();

                    Int64.TryParse(readerString, out sequenceNumber);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    oracleConnection.Close();
                }
                catch { }
            }

            return sequenceNumber;
        }

        //Erika 2011-05-03
        protected object ExecuteScalar(string query, IDataParameter[] parameters, object defaultValue)
        {
            try
            {
                if (oracleConnection.ConnectionString == string.Empty)
                    throw new ApplicationException("Please provide a connection string before executing a query.");

                object returnValue = defaultValue;

                oracleConnection.Open();
                using (OracleCommand oracleQuery = new OracleCommand(query.Replace("\r\n", "\n")))
                {
                    if (parameters != null && parameters.Length > 0)
                        oracleQuery.Parameters.AddRange(ConvertNullToDBNullValue(parameters));

                    oracleQuery.Connection = oracleConnection;
                    returnValue = IsNullObject(oracleQuery.ExecuteScalar(), defaultValue);
                    oracleQuery.Dispose();
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //if (oracleConnection.State != ConnectionState.Closed)
                //{
                try
                {
                    oracleConnection.Close();
                }
                catch { }
                //}
            }
        }

        public static object IsNullObject(object input, object defaultOutput)
        {
            if (input == null || input == DBNull.Value)
            {
                return defaultOutput;
            }
            else
            {
                return input;
            }
        }

        protected IDataParameter[] ConvertNullToDBNullValue(IDataParameter[] parameters)
        {
            if (parameters == null)
            {
                return null;
            }

            foreach (OracleParameter parameter in parameters)
            {
                parameter.IsNullable = true;
                if (parameter.Value == null)
                {
                    parameter.Value = DBNull.Value;
                }
            }

            return parameters;
        }

        public static void SaveCloseError(Exception ex, string machineName)
        {
            string error = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "   " + ex.Message + "  " + machineName + "                 " + ex.StackTrace + "                 ";

            Exception inner = ex.InnerException;
            while (inner != null)
            {
                error += " INNER : " + inner.Message + "                 " + inner.StackTrace;
                inner = inner.InnerException;
            }

            string errorFilename = "Data_" + DateTime.Now.ToString("yyyy_MM_dd") + ".txt";

            StreamWriter sw = new StreamWriter(errorFilename, true);
            sw.WriteLine(DateTime.Now.ToShortTimeString() + " - " + error);
            sw.Flush();
            sw.Close();
        }
    }
}
