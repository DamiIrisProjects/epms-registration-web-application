﻿using System.Collections.Generic;

namespace PassportApplicationCommon
{
    public static class CommonLists
    {
        public static List<KeyValuePair<int, string>> GetApplicationReasonList()
        {
            //Reissue - Historic Removed from the list that is used in dropdown lists

            List<KeyValuePair<int, string>> applicationReason = new List<KeyValuePair<int, string>>();
            applicationReason.Add(new KeyValuePair<int, string>(1, "First Passport"));
            applicationReason.Add(new KeyValuePair<int, string>(2, "Reissue(Expiry)"));
            applicationReason.Add(new KeyValuePair<int, string>(3, "Reissue(Lost)"));
            applicationReason.Add(new KeyValuePair<int, string>(4, "Reissue(Damaged)"));
            //applicationReason.Add(new KeyValuePair<int, string>(5, "Change Of Details(Historic)"));
            applicationReason.Add(new KeyValuePair<int, string>(6, "Reissue(Stolen)"));
            applicationReason.Add(new KeyValuePair<int, string>(7, "Reissue(Change of Information due to marriage)"));
            applicationReason.Add(new KeyValuePair<int, string>(8, "Reissue(Change of Information due to other reasons)"));
            
            return applicationReason;
        }

        public static List<KeyValuePair<int, string>> GetDocumentTypeList()
        {
            //Reissue - Historic Removed from the list that is used in dropdown lists

            List<KeyValuePair<int, string>> doctypes = new List<KeyValuePair<int, string>>();
            doctypes.Add(new KeyValuePair<int, string>(1, "Ordinary 32 Page"));
            doctypes.Add(new KeyValuePair<int, string>(2, "Ordinary 64 Page"));
            doctypes.Add(new KeyValuePair<int, string>(3, "Official 32 Page"));
            doctypes.Add(new KeyValuePair<int, string>(4, "Official 64 Page"));
            doctypes.Add(new KeyValuePair<int, string>(5, "Diplomatic 32 Page"));
            doctypes.Add(new KeyValuePair<int, string>(6, "Diplomatic 64 Page"));

            return doctypes;
        }

        public static List<KeyValuePair<int, string>> GetMaritalStatusList()
        {
            //Reissue - Historic Removed from the list that is used in dropdown lists

            List<KeyValuePair<int, string>> status = new List<KeyValuePair<int, string>>();
            status.Add(new KeyValuePair<int, string>(1, "Single"));
            status.Add(new KeyValuePair<int, string>(2, "Married"));
            status.Add(new KeyValuePair<int, string>(3, "Widowed"));
            status.Add(new KeyValuePair<int, string>(4, "Divorced"));

            return status;
        }

        public static List<KeyValuePair<int, string>> GetEyeColorList()
        {
            //Reissue - Historic Removed from the list that is used in dropdown lists

            List<KeyValuePair<int, string>> status = new List<KeyValuePair<int, string>>();
            status.Add(new KeyValuePair<int, string>(1, "Brown"));
            status.Add(new KeyValuePair<int, string>(2, "Green"));
            status.Add(new KeyValuePair<int, string>(3, "Grey"));
            status.Add(new KeyValuePair<int, string>(4, "Blue"));
            status.Add(new KeyValuePair<int, string>(5, "Other"));

            return status;
        }

        public static List<KeyValuePair<int, string>> GetHairColorList()
        {
            //Reissue - Historic Removed from the list that is used in dropdown lists

            List<KeyValuePair<int, string>> status = new List<KeyValuePair<int, string>>();
            status.Add(new KeyValuePair<int, string>(1, "Black"));
            status.Add(new KeyValuePair<int, string>(2, "Gray"));
            status.Add(new KeyValuePair<int, string>(3, "White"));
            status.Add(new KeyValuePair<int, string>(4, "Other"));

            return status;
        }
    }
}
