﻿using System.Collections.Generic;

namespace PassportApplicationCommon.Model
{
    public class Operator
    {
        public int OperatorId { get; set; }

        public string OperatorTypeName { get; set; }

        public string Branch { get; set; }

        public string Email { get; set; }

        public string RoleType { get; set; }

        public string Name { get; set; }

        public string Firstname { get; set; }

        public string Surname { get; set; }

        public List<OperatorRole> Roles { get; set; }
    }
}
