﻿using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PassportApplicationCommon.Model
{
    [DataContract]
    public class Address
    {
        [DataMember]
        [Required]
        [DisplayName("Address Line 1")]
        public string AddressLine1 { get; set; }
        
        [DataMember]
        [DisplayName("Address Line 2")]
        public string AddressLine2 { get; set; }

        [Required]
        [DataMember]
        [DisplayName("City")]
        public string City { get; set; }

        [Required]
        [DataMember]
        [DisplayName("Country")]
        public string Country { get; set; }

        [DataMember]
        [Required]
        [DisplayName("State")]
        public string State { get; set; }

        [DataMember]
        [DisplayName("Local Government Area")]
        public string Lga { get; set; }

        [DataMember]
        [DisplayName("District")]
        public string District { get; set; }

        [DataMember]
        [DisplayName("Postal Code")]
        public string PostalCode { get; set; }
    }
}
