﻿using System.Runtime.Serialization;

namespace PassportApplicationCommon.Model
{
    [DataContract]
    public class State
    {
        [DataMember]
        public int StateId { get; set; }

        [DataMember]
        public string StateName { get; set; }

        [DataMember]
        public string Code { get; set; }
    }
}
