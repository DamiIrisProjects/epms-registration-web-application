﻿using System.Runtime.Serialization;

namespace PassportApplicationCommon.Model
{
    [DataContract]
    public class PaymentResponse
    {   
        [DataMember]
        public int BaseAmountUsd { get; set; }

        [DataMember]
        public int BaseAmountNaira { get; set; }

        [DataMember]
        public int ExtraAmountUsd { get; set; }

        [DataMember]
        public int ExtraAmountNaira { get; set; }

        public int TotalAmountNaira
        {
            get
            {
                return BaseAmountNaira + ExtraAmountNaira;
            }

            private set { }
        }
        
        public int TotalAmountUsd
        {
            get
            {
                return BaseAmountUsd + ExtraAmountUsd;
            }

            private set { }
        }
    }
}
