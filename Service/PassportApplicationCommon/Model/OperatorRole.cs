﻿using System.Collections.Generic;

namespace PassportApplicationCommon.Model
{
    public class OperatorRole
    {
        public int RoleId { get; set; }

        public string Name { get; set; }


        public List<OperatorPrivilege> Privileges { get; set; }
    }
}
