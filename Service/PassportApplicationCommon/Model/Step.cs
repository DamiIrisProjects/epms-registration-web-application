﻿namespace PassportApplicationCommon.Model
{
    public class Step
    {
        public void SetError(string errormessage)
        {
            StepMessage = errormessage;
            StepStatus = StepStatus.Error;
        }

        public void SetSuccess()
        {
            StepMessage = string.Empty;
            StepStatus = StepStatus.Succesful;
        }

        public int StepId { get; set; }

        public int? ParentStepId { get; set; }

        public string StepText { get; set; }

        public StepStatus StepStatus { get; set; }

        public string StepMessage { get; set; }

        public string Action { get; set; }

        public string Controller { get; set; }
    }
}