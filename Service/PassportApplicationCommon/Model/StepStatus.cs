﻿namespace PassportApplicationCommon.Model
{
    public enum StepStatus
    {
        New = 1,
        Error = 2,
        Warning = 3,
        Succesful = 4
    }
}