﻿namespace PassportApplicationCommon.Model
{
    public class OperatorPrivilege
    {
        public int PrivilegeId { get; set; }

        public string Name { get; set; }
    }
}
