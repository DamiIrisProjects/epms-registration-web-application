﻿using System.Runtime.Serialization;

namespace PassportApplicationCommon.Model
{
    [DataContract]
    public class ProcessingCountry
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string CountryName { get; set; }

        [DataMember]
        public string CountryCode { get; set; }
    }
}
