﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace PassportApplicationCommon.Model
{
    [DataContract]
    public class PaymentMetadata
    {
        public PaymentMetadata()
        {
            PaymentStatus = 1;
            PayeStatus = 1;
            PaycStatus = 1;
        }

        //Application payment (Payment_enrollment- PayE)
        //Change payment (Payment_change- PayC)

        //Table changes designed by nico to save extra payment info for when payment is not available.
        [DataMember]
        public int PayeDue { get; set; }

        [DataMember]
        public int PaycDue { get; set; }

        [DataMember]
        public int PaymentStatus { get; set; }

        [DataMember]
        public int PayeStatus { get; set; }

        [DataMember]
        public DateTime PayeSysConfirmdate { get; set; }

        [DataMember]
        public string PayeSysConfirmWho { get; set; }

        [DataMember]
        public DateTime PayeDatepaid { get; set; }

        [DataMember]
        public int PayeAmount { get; set; }
             
        [DataMember]
        public int PaycStatus { get; set; }

        [DataMember]
        public DateTime PaycSysConfirmdatetime { get; set; }

        [DataMember]
        public string PaycSysConfirmwho { get; set; }

        [Required]
        [DataMember]
        [DisplayName("Payment Date")]
        public DateTime PaycDatepaid { get; set; }

        [DataMember]
        [Required]
        [DisplayName("Payment Amount")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:#.#}")]  
        public int PaycAmount { get; set; }

        [DataMember]
        public string PayeNamesurname { get; set; }

        [DataMember]
        [Required]
        [DisplayName("Full Name")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:#.#}")]  
        public string PaycNamesurname { get; set; }
    }
}
