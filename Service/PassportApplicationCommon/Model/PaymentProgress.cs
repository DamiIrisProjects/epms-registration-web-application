﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace PassportApplicationCommon.Model
{
    public class PaymentProgress
    {
        public PaymentProgress()
        {
            PaymentExist = false;
            FirstnameMatch = false;
            SurnameMatch = false;
            PaymentMatch = false;
            CalculatedBasePrice = 0;
            CalculatedAdditionalPrice = 0;
            PaymentTotalPrice = 0;
        }

        [DataMember]
        [DisplayName("Payment Exist")]
        public bool PaymentExist { get; set; }

        [DataMember]
        [DisplayName("Firstname Match")]
        public bool FirstnameMatch { get; set; }

        [DataMember]
        [DisplayName("Surname Match")]
        public bool SurnameMatch { get; set; }

        [DataMember]
        [DisplayName("Payment Amount Match")]
        public bool PaymentMatch { get; set; }

        [DataMember]
        [DisplayName("Amount Due")]
        [DisplayFormat(DataFormatString = "{0,10:N0}")]
        public int CalculatedBasePrice { get; set; }

        [DataMember]
        [DisplayName("Additional Amount Due")]
        [DisplayFormat(DataFormatString = "{0,20:N0}")]
        public int CalculatedAdditionalPrice { get; set; }

        [DataMember]
        [DisplayName("Amount Paid")]
        [DisplayFormat(DataFormatString = "{0,10:N0}")]
        public int PaymentTotalPrice { get; set; }
    }
}
