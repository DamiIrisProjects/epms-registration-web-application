﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace PassportApplicationCommon.Model
{
    [DataContract]    
    public class PassportBasic
    {
        //[DataMember]
        //[DisplayName("Family Application")]
        //public bool FamilyApplication { get; set; }

        //Passport processing country, state and office
        [DataMember]
        [DisplayName("Processing State")]
        public string ProcessingState { get; set; }
        
        [DataMember]
        [DisplayName("Passport Office")]
        public string PassportOffice { get; set; }

        [DataMember]
        [DisplayName("Stage Code")]
        public string StageCode { get; set; }
    }
}
