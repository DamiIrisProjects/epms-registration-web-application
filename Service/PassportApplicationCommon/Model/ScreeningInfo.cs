﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;

namespace PassportApplicationCommon.Model
{
    [DataContract]
    public class ScreeningInfo
    {        
        [DataMember]
        [DisplayName("Surname")]
        public string Surname { get; set; }

        [DataMember]
        [DisplayName("First name")]
        public string Firstname { get; set; }

        [DataMember]
        [DisplayName("Date of Birth")]
        public DateTime DateOfBirth { get; set; }

        [DataMember]
        [DisplayName("Application Type")]
        public int ApplicationType { get; set; }

        public string ApplicationTypeString 
        { 
            get 
            {
                if (ApplicationType == 0)
                    return string.Empty;

                return CommonLists.GetApplicationReasonList().First(c => c.Key == ApplicationType).Value;
            }

            private set { } 
        }

        [DataMember]
        [DisplayName("Previous Passport Number")]
        public string PreviousPassportNumber { get; set; }

        [DataMember]
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:#.#}")]  
        [DisplayName("Application ID")]
        public int ApplicationId { get; set; }

        [DataMember]
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:#.#}")]  
        [DisplayName("Reference Number")]
        public int ReferenceNumber { get; set; }

        [DataMember]
        [Required]
        [DisplayName("Form No")]
        public string FormNo { get; set; }


        [DataMember]
        [Required]
        [DisplayName("File No")]
        public string FileNo { get; set; }


        [Required]
        [DataMember]
        [DisplayName("Document Type")]
        public int DocumentType { get; set; }

        [DataMember]
        public PaymentProgress PaymentProgress { get; set; }

        [DataMember]
        public PaymentMetadata PaymentMetadata { get; set; }
    }   
}
