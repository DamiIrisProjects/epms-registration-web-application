﻿using System;
using System.Runtime.Serialization;

namespace PassportApplicationCommon.Model
{
    [DataContract]
    public class ExaroSystemException
    {
        [DataMember]
        public DateTime ExceptionDate { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string Stacktrace { get; set; }

        [DataMember]
        public string InnerExceptionMessage { get; set; }

        [DataMember]
        public string InnerExceptionStackTrace { get; set; }

        [DataMember]
        public string MachineNameOrIp { get; set; }

        [DataMember]
        public string Browser { get; set; }
    }
}
