﻿namespace PassportApplicationCommon.Models
{
    public class InternalActionModel
    {
        public int Id { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }
    }
}
