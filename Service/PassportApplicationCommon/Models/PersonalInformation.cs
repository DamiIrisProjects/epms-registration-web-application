﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using PassportApplicationCommon.Enum;

namespace PassportApplicationCommon.Models
{
    [DataContract]
    public class PersonalInformation
    {
        [Required]
        [DataMember]
        [DisplayName("Surname")]
        public string Surname { get; set; }

        [Required]
        [DataMember]
        [DisplayName("First name")]
        public string Firstname { get; set; }

        [DataMember]
        [DisplayName("Middle name")]
        public string Middlename { get; set; }

        [DataMember]
        [DisplayName("Occupation")]
        public string Occupation { get; set; }

        [Required]
        [DataMember]
        [DisplayName("Gender")]
        public string Gender { get; set; }

        [Required]
        [DataMember]
        [DisplayName("Date of Birth")]
        public DateTime DateOfBirth { get; set; }

        [DataMember]
        [DataType(DataType.PhoneNumber)]
        [DisplayName("Contact Phone")]
        public string ContactPhoneNumber { get; set; }

        [DataMember]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Email Address")]
        public string EmailAddress { get; set; }

        [Required]
        [DataMember]
        [DisplayName("Title")]
        public Title Title { get; set; }

        [DataMember]
        [DisplayName("Identity Number")]
        public string IdentityNumber { get; set; }

        [DataMember]
        [DisplayName("Maiden Name")]
        public string MaidenName { get; set; }

        [Required]
        [DataMember]
        [DisplayName("Maritial Status")]
        public int MaritialStatus { get; set; }

        public string MaritialStatusString
        {
            get
            {
                if (MaritialStatus == 0)
                    return string.Empty;
                return CommonLists.GetMaritalStatusList().Where(x => x.Key == MaritialStatus).FirstOrDefault().Value;
            }
        }

    }
}
