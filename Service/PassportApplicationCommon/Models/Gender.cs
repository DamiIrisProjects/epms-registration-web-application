﻿using System.Runtime.Serialization;

namespace PassportApplicationCommon.Models
{
    [DataContract]
    public class Gender
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string GenderAbbrev { get; set; }

        [DataMember]
        public string Description { get; set; }
    }
}
