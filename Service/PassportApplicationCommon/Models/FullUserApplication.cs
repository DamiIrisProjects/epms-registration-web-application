﻿using System.Runtime.Serialization;

namespace PassportApplicationCommon.Models
{
    [DataContract]
    public class FullUserApplication
    {        
        public FullUserApplication()
        {
            //Passports = new List<PassportApplication>();
            PassportApplication = new PassportApplication();
        }

        [DataMember]
        public PassportApplication PassportApplication { get; set; }

        //Intention was to use the list for family applications, but business restricted this idea
        //[DataMember]
        //public List<PassportApplication> Passports { get; set; }
    }
}
