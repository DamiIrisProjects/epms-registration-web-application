﻿using System.Collections.Generic;

namespace PassportApplicationCommon.Models
{
    public class OperatorRole
    {
        public int RoleId { get; set; }

        public string Name { get; set; }


        public List<OperatorPrivilege> Privileges { get; set; }
    }
}
