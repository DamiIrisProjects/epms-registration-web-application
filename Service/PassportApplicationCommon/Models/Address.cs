﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace PassportApplicationCommon.Models
{
    [DataContract]
    public class Address
    {
        public Address()
        {
            Country = "NGA";
        }

        [Required]
        [DataMember]
        [DisplayName("Address Line 1")]
        public string AddressLine1 { get; set; }

        [DataMember]
        [DisplayName("Address Line 2")]
        public string AddressLine2 { get; set; }

        [Required]
        [DataMember]
        [DisplayName("City")]
        public string City { get; set; }

        [Required]
        [DataMember]
        [DisplayName("Country")]
        public string Country { get; set; }

        [DataMember]
        public string CountryName { get; set; }

        [DataMember]
        [DisplayName("State")]
        public string State { get; set; }

        [DataMember]
        [DisplayName("ForeignState")]
        public string StateForeign{ get; set; }

        [Required]
        [DataMember]
        [DisplayName("Local Government Area")]
        public string Lga { get; set; }

        [DataMember]
        [DisplayName("District")]
        public string District { get; set; }

        [DataMember]
        [DisplayName("Postal Code")]
        public string PostalCode { get; set; }
    }
}
