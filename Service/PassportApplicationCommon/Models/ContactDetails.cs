﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace PassportApplicationCommon.Models
{
    [DataContract]
    public class ContactDetails
    {
        public ContactDetails()
        {
            Address = new Address();
        }

        [DataMember]
        [Required]
        [DataType(DataType.PhoneNumber)]
        [DisplayName("Contact Phone")]
        public string ContactPhoneNumber { get; set; }

        [DataMember]
        [DisplayName("Mobile Phone")]
        public string MobilePhoneNumber { get; set; }

        [DataMember]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Email Address")]
        public string EmailAddress { get; set; }

        [DataMember]
        [DisplayName("Nigerian Address")]
        public Address Address { get; set; }
    }
}
