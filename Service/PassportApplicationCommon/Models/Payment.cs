﻿using System;
using PassportApplicationCommon.Enum;

namespace PassportApplicationCommon.Models
{
    public class Payment
    {
        public Payment()
        {
            PaymentStatus = Enum.PaymentStatus.Unused;
        }

        public int ApplicationId { get; set; }

        public int ReferenceNo { get; set; }

        public int AmountNaira { get; set; }

        public int AmountDollar { get; set; }

        public PaymentStatus PaymentStatus { get; set; }

        public string Firstname { get; set; }

        public string LastName { get; set; }

        public DateTime Dob { get; set; }

        public string StateOfBirth { get; set; }

        public string Gender { get; set; }

        public string ProcessingCountry { get; set; }

        public string ProcessingState { get; set; }

        public string ProcessingOffice { get; set; }

        public DateTime UploadDate { get; set; }

        public DateTime PaymentDate { get; set; }

        public string PassportType { get; set; }

        public DateTime ExpiryDate { get; set; }

        public string FormNo { get; set; }

        public DateTime EnrollmentDate { get; set; }

        public int PassportSize { get; set; }
    }
}
