﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace PassportApplicationCommon.Models
{
    [DataContract]
    public class Passport
    {
        [DataMember]
        [DisplayName("Authorisation Code")]
        public long HistoryId { get; set; }

        [DataMember]        
        public string IdentificationGuid { get; set; }

        [DataMember]
        [DisplayName("Passport Number")]        
        public string DocNo { get; set; }
                
        [DataMember]
        [DisplayName("Stage Code")]
        public string StageCode { get; set; }

        [DataMember]
        [DisplayName("Date of Issue")]
        public DateTime IssueDate { get; set; }

        [DataMember]
        [DisplayName("Document Status")]
        public string StageDescription { get; set; }
        
        [DataMember]
        [DisplayName("Surname")]        
        public string Surname { get; set; }
                
        [DataMember]
        [DisplayName("First Name")]
        public string FirstName { get; set; }
                
        [DataMember]
        [DisplayName("Other Names")]
        public string OtherNames { get; set; }
                
        [DataMember]
        [DisplayName("Gender")]
        public string Gender { get; set; }

        [DisplayName("Gender")]
        public string GenderDesc { 
            get
            {
                if (Gender == "M")
                    return "Male";
                return "Female";
            }            
        }
                
        [DataMember]
        [DisplayName("Date of Birth")]
        public DateTime BirthDate { get; set; }
                
        [XmlIgnore]
        public byte[] TempFace { get; set; }

        [XmlIgnore]
        public Image TempFaceImage { get; set; }

        [XmlIgnore]
        public byte[] TempSign { get; set; }

        [DataMember]
        [DisplayName("Document Type")]
        public string DocType { get; set; }

        [DataMember]
        [DisplayName("Issue Place")]
        public string IssuePlace { get; set; }

        [DataMember]
        [DisplayName("Expiry Date")]
        public DateTime ExpiryDate { get; set; }
    }
}
