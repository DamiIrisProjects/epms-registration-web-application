﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;

namespace PassportApplicationCommon.Models
{
    public class PersonalFeatures
    {
        [DataMember]
        [Required]
        [DisplayName("Colour of Eyes")]
        public int ColourOfEyes { get; set; }

        public string ColourOfEyesString
        {
            get
            {
                if (ColourOfEyes == 0)
                    return string.Empty;
                return CommonLists.GetEyeColorList().Where(x => x.Key == ColourOfEyes).FirstOrDefault().Value;
            }
        }

        [DataMember]
        [Required]
        [DisplayName("Colour of Hair")]
        public int ColourOfHair { get; set; }

        public string ColourOfHairString
        {
            get
            {
                if (ColourOfHair == 0)
                    return string.Empty;
                return CommonLists.GetHairColorList().Where(x => x.Key == ColourOfHair).FirstOrDefault().Value;
            }
        }

        [DataMember]
        [DisplayName("Height in cm")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:#.#}")] 
        public int HeightInCm { get; set; }

        [DataMember]
        [DisplayName("Other Information")]
        public string OtherInformation { get; set; }

        [DataMember]
        [DisplayName("Special Features")]
        public string SpecialFeatures { get; set; }
    }
}
