﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace PassportApplicationCommon.Models
{
    public class NextOfKin
    {
        public NextOfKin()
        {
            Address = new Address();
            Address.Country = "NGA";
        }

        [Required]
        [DataMember]
        [DisplayName("Next of Kin Name")]
        public string NextOfKinName { get; set; }

        [Required]
        [DataMember]
        [DisplayName("Relationship with Next of Kin")]
        public string RelationshipWithNextOfKin { get; set; }

        [Required]
        [DataMember]
        [DataType(DataType.PhoneNumber)]
        [DisplayName("Contact Number of Next of Kin")]
        public string ContactNumberOfNextOfKin { get; set; }

        [Required]
        [DataMember]
        [DisplayName("Next of Kin Address")]
        public Address Address { get; set; }
    }
}
