﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace PassportApplicationCommon.Models
{
    [DataContract]
    public class PaymentInformation
    {
        public PaymentInformation()
        {
            PaymentStatus = 1;
            PayeStatus = 1;
            PaycStatus = 1;
        }

        [DataMember]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:#.#}")] 
        [DisplayName("Application ID")]
        public int ApplicationId { get; set; }

        [DataMember]
        [DisplayName("Reference Number")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:#.#}")] 
        public int ReferenceNumber { get; set; }

        [DataMember]
        public int PayeDue { get; set; }

        public double FirstnameCoefficient { get; set; }

        public double SurnameCoefficient { get; set; }

        [DataMember]
        [Required]
        [DisplayName("First name")]
        public string Firstname { get; set; }

        public string PaymentFirstname { get; set; }

        [DataMember]
        [Required]
        public string Surname { get; set; }

        public string PaymentSurname { get; set; }

        [DataMember]
        [Required]
        public DateTime DateOfBirth { get; set; }

        [DataMember]
        [Required]
        public string Gender { get; set; }

        [DataMember]
        public int PaycDue { get; set; }

        [DataMember]
        [DisplayName("Payment Amount")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:#.#}")] 
        public int PayeAmount { get; set; }

        [DataMember]
        public int PaycStatus { get; set; }

        [DataMember]
        public DateTime PaycSysConfirmdatetime { get; set; }

        [DataMember]
        public string PaycSysConfirmwho { get; set; }

        [Required]
        [DataMember]
        [DisplayName("Payment Date")]
        public DateTime PaycDatepaid { get; set; }

        [DataMember]
        public int PaymentStatus { get; set; }

        [DataMember]
        public int PayeStatus { get; set; }

        [DataMember]
        public DateTime PayeSysConfirmdate { get; set; }

        [DataMember]
        public string PayeSysConfirmWho { get; set; }

        [DataMember]
        public string PayeNamesurname { get; set; }

        [DataMember]
        [DisplayName("Payment Amount")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:#.#}")] 
        public int PaycAmount { get; set; }

        [DataMember]
        public string PaycNamesurname { get; set; }

        [Required]
        [DataMember]
        [DisplayName("Payment Date")]
        public DateTime PayeDatepaid { get; set; }

        [DataMember]
        [DisplayName("Payment Exist")]
        public bool PaymentExist { get; set; }

        [DataMember]
        [DisplayName("Firstname Match")]
        public bool FirstnameMatch { get; set; }

        [DataMember]
        [DisplayName("Surname Match")]
        public bool SurnameMatch { get; set; }

        [DataMember]
        [DisplayName("DOB Match")]
        public bool DobMatch { get; set; }

        [DataMember]
        [DisplayName("Gender Match")]
        public bool GenderMatch { get; set; }

        [DataMember]
        [DisplayName("Payment Amount Match")]
        public bool PaymentMatch { get; set; }

        [DataMember]
        [DisplayName("Amount Due")]
        [DisplayFormat(DataFormatString = "{0,10:N0}")]
        public int CalculatedBasePrice { get; set; }

        [DataMember]
        [DisplayName("Additional Amount Due")]
        [DisplayFormat(DataFormatString = "{0,20:N0}")]
        public int CalculatedAdditionalPrice { get; set; }

        [DataMember]
        [DisplayName("Amount Paid")]
        [DisplayFormat(DataFormatString = "{0,10:N0}")]
        public int PaymentTotalPrice { get; set; }

        [DataMember]
        public int AmountExpected { get; set; }

        [DataMember]
        public int AmountFound { get; set; }
    }
}
