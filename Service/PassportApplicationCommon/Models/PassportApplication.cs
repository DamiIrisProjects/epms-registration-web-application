﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Runtime.Serialization;

namespace PassportApplicationCommon.Models
{
    [DataContract]
    public class PassportApplication
    {
        public PassportApplication()
        {
            PassportInformation = new PassportInformation();
            Payment = new PaymentInformation();
            PersonalInformation = new PersonalInformation();
            ContactDetails = new ContactDetails();
            Locality = new Locality();
            PersonalFeatures = new PersonalFeatures();
            NextOfKin = new NextOfKin();
            BreederDocs = new List<BreederDoc>();
            PreviousPassports = new List<Passport>();
        }

        [DataMember]
        [DisplayName("Application ID")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:#.#}")]
        public int ApplicationId { get; set; }

        [DataMember]
        [DisplayName("Form Number")]
        public string FormNo { get; set; }

        public string FormNoAndName
        {
            get
            {
                if (PersonalInformation != null && !string.IsNullOrEmpty(PersonalInformation.Firstname) && !string.IsNullOrEmpty(PersonalInformation.Surname))
                    return FormNo + " - " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(PersonalInformation.Firstname.ToLower()) + " " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(PersonalInformation.Surname.ToLower());

                return FormNo;
            }            
        }

        [DataMember]
        [DisplayName("File Number")]
        public string FileNo { get; set; }

        [DataMember]
        [DisplayName("Form Number")]
        public int ReferenceNumber { get; set; }

        [DataMember]
        [DisplayName("Stage Code")]
        public string StageCode { get; set; }

        [DataMember]
        public bool IsCompleted { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }

        [DataMember]
        public PassportInformation PassportInformation { get; set; }

        [DataMember]
        public PaymentInformation Payment { get; set; }

        [DataMember]
        public PersonalInformation PersonalInformation { get; set; }

        [DataMember]
        public ContactDetails ContactDetails { get; set; }

        [DataMember]
        public Locality Locality { get; set; }

        [DataMember]
        public NextOfKin NextOfKin { get; set; }

        [DataMember]
        public PersonalFeatures PersonalFeatures { get; set; }

        [DataMember]
        public HistoricReports HistoricReports { get; set; }

        [DataMember]
        public List<Passport> PreviousPassports { get; set; }

        [DataMember]
        public List<BreederDoc> BreederDocs { get; set; }

        [DataMember]
        public bool BreederDocumentsValidated { get; set; }

        [DataMember]
        public int RejectionReason { get; set; }

        [DataMember]
        public string RejectionReasonHeader { get; set; }

        [DataMember]
        public string RejectionReasonDetails { get; set; }
    }
}
