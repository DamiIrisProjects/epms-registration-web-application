﻿using System.Runtime.Serialization;

namespace PassportApplicationCommon.Models
{
    [DataContract]
    public class PaymentResponse
    {   
        [DataMember]
        public int BaseAmountUsd { get; set; }

        [DataMember]
        public int BaseAmountNaira { get; set; }

        [DataMember]
        public int ExtraAmountUsd { get; set; }

        [DataMember]
        public int ExtraAmountNaira { get; set; }

        public int TotalAmountNaira => BaseAmountNaira + ExtraAmountNaira;

        public int TotalAmountUsd => BaseAmountUsd + ExtraAmountUsd;
    }
}
