﻿using System.Runtime.Serialization;

namespace PassportApplicationCommon.Models
{
    [DataContract]
    public class DocumentType
    {
        public DocumentType()
        {
            Description = string.Empty;
        }

        [DataMember]
        public int Id { get; set; }

        //DocTypeCodes
        //P = Ordinary
        //PF = Official
        //PD = Diplomatic

        [DataMember]
        public string DocTypeCode { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int Pages { get; set; }
    }
}
