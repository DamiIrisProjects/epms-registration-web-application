﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace PassportEnrolmentEntities
{
    public class Biometrics
    {
        [DataMember]
        public List<byte[]> FingerList { get; set; }

        [DataMember]
        public byte[] PictureColor { get; set; }

        [DataMember]
        public byte[] PictureBW { get; set; }

        [DataMember]
        public Afisteam.Biometrics.Person Fingers { get; set; }

        [DataMember]
        public byte[] FingerprintArray { get; set; }

        [DataMember]
        public byte[] SignatureData { get; set; }

        [DataMember]
        public BitmapSource SignatureSource { get; set; }
    }
}
