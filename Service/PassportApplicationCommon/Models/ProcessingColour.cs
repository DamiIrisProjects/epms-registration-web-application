﻿using System.Runtime.Serialization;

namespace PassportApplicationCommon.Models
{
    [DataContract]
    public class ProcessingColour
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int Colour { get; set; }

        [DataMember]
        public string Description { get; set; }
    }
}
