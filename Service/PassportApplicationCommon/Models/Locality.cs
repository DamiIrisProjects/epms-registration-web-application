﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace PassportApplicationCommon.Models
{
    [DataContract]
    public class Locality
    {
        public Locality()
        {
            StateOfOrigin = string.Empty;
            Nationality = "NGA";
        }

        [Required]
        [DataMember]
        [DisplayName("Place of Birth")]
        public string PlaceOfBirth { get; set; }

        [Required]
        [DataMember]
        [DisplayName("State of Origin")]
        public string StateOfOrigin { get; set; }

        [Required]
        [DataMember]
        [DisplayName("Nationality")]
        public string Nationality { get; set; }

        [DataMember]
        public string NationalityName { get; set; }

        [Required]
        [DataMember]
        [DisplayName("Home Town")]
        public string HomeTown { get; set; }
    }
}
