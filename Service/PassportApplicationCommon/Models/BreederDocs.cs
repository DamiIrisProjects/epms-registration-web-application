﻿using System.Runtime.Serialization;

namespace PassportApplicationCommon.Models
{
    [DataContract]
    public class BreederDoc
    {
        [DataMember]
        public int DocumentId { get; set; }

        [DataMember]
        public string DocumentName { get; set; }

        [DataMember]
        public bool IsVerified { get; set; }

        [DataMember]
        public int IsRequired { get; set; }
    }
}
