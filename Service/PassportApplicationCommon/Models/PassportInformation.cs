﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;

namespace PassportApplicationCommon.Models
{
    public class PassportInformation
    {
        [Required]
        [DataMember]
        [DisplayName("Document Type")]
        public int DocumentType { get; set; }

        [DataMember]
        [Required]
        [DisplayName("Application Type")]
        public int ApplicationType { get; set; }

        public string DocumentTypeString
        {
            get
            {
                if (DocumentType == 0)
                    return string.Empty;

                return CommonLists.GetDocumentTypeList().First(c => c.Key == DocumentType).Value;
            }

            private set { }
        }

        public string ApplicationTypeString
        {
            get
            {
                if (ApplicationType == 0)
                    return string.Empty;

                return CommonLists.GetApplicationReasonList().First(c => c.Key == ApplicationType).Value;
            }

            private set { }
        }


        [DataMember]
        [DisplayName("Previous Passport Number")]
        public string PreviousPassportNumber { get; set; }
    }
}
