﻿using System.Runtime.Serialization;

namespace PassportApplicationCommon.Models
{
    [DataContract]
   public class PassportBranch
    {
       [DataMember]
        public int BranchCode { get; set; }

        [DataMember]
        public string CountryCode { get; set; }

        //Needed for cases where the current selected country doesn't have branches and nearest branch is returned
        [DataMember]
        public string CountryName { get; set; }

        [DataMember]
        public string BranchName { get; set; }

        //Needed for cases where the current selected country doesn't have branches and nearest branch is returned
        public string FullBranchName => CountryName + " - " + BranchName;
    }
}
