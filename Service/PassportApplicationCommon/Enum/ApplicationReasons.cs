﻿namespace PassportApplicationCommon.Enum
{
    //There is also a list under CommonLists that is used for dropdown Selects
    public enum ApplicationReason
    {
         Unknown = 0,
         FirstPassport = 1,
         ReissueExpiry = 2,
         ReissueLost = 3,
         ReissueDamaged = 4,
         ChangeOfDetailsHistoric = 5,
         ReissueStolen = 6,
         ReissueChangeOfInformationDueToMarriage = 7,
         ReissueChangeOfInformationDueToOtherReasons = 8
    }
}
