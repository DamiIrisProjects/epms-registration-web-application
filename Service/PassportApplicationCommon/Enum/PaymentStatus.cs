﻿namespace PassportApplicationCommon.Enum
{
    public enum PaymentStatus
    {
        Unused = 1,
        Used = 2
    }

    public enum PayStatusEnrollment
    {
        Null = 1,
        Outstanding = 2,
        Different = 3,
        Verified = 4,
        NotRequired = 5
    }
}
