﻿namespace PassportApplicationCommon.Enum
{
    public enum Title
    {
        Mr,
        Mrs,
        Miss,
        Dr
    }
}
