﻿using System.Collections.Generic;

namespace PassportApplicationCommon.Helpers
{
    public static class Sorensen
    {
        //Calculate the difference between strings.

        public static double DiceCoefficient(string stringOne, string stringTwo)
        {
            stringOne = stringOne.ToUpper();

            stringTwo = stringTwo.ToUpper();

            HashSet<string> nx = new HashSet<string>();
            HashSet<string> ny = new HashSet<string>();

            for (int i = 0; i < stringOne.Length - 1; i++)
            {
                char x1 = stringOne[i];
                char x2 = stringOne[i + 1];
                string temp = x1.ToString() + x2.ToString();
                nx.Add(temp);
            }
            for (int j = 0; j < stringTwo.Length - 1; j++)
            {
                char y1 = stringTwo[j];
                char y2 = stringTwo[j + 1];
                string temp = y1.ToString() + y2.ToString();
                ny.Add(temp);
            }

            HashSet<string> intersection = new HashSet<string>(nx);
            intersection.IntersectWith(ny);

            double dbOne = intersection.Count;
            return (2 * dbOne) / (nx.Count + ny.Count);

        }
    }
}
