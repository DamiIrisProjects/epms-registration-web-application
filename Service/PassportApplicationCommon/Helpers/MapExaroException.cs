﻿using PassportApplicationCommon.Model;
using System;

namespace PassportApplicationCommon.Helpers
{
    public static class MapExaroException
    {
        public static ExaroSystemException MapException(Exception ex,string machineName)
        {
            var exception = new ExaroSystemException
            {
                MachineNameOrIp = machineName,
                Message = string.IsNullOrEmpty(ex.Message) ? "" : ex.Message,
                Stacktrace = ex.StackTrace ?? ""
            };

            if (ex.InnerException != null)
            {
                exception.InnerExceptionMessage = string.IsNullOrEmpty(ex.InnerException.Message) ? "" : ex.InnerException.Message;
                exception.InnerExceptionStackTrace = ex.InnerException.StackTrace ?? "";
            }

            return exception;
        }
    }
}
