﻿using PassportApplicationContract;
using System.Configuration;
using System.Net;
using System.ServiceModel;
using System.Xml;

namespace PassportApplicationProxy
{
    public class SecurityProxy
    {
        public ISecurity SecurityChannel { get; set; }

        public string ServiceBaseAddress { get; set; }

        public SecurityProxy()
        {
            //Get from config
            ServiceBaseAddress = ConfigurationManager.AppSettings["ServiceUrl"] + "/SecurityService.svc";

            var binding = new WSHttpBinding
            {
                MaxReceivedMessageSize = 2147483647,
                Security =
                {
                    Mode = SecurityMode.Transport,
                    Transport = {ClientCredentialType = HttpClientCredentialType.None}
                },
                ReaderQuotas = new XmlDictionaryReaderQuotas() {MaxArrayLength = 2147483647}
            };
            //4 mb

            //if (NHISOnlineExtCommon.Utilities.GeneralUtilities.IsDeveloperNigeria ||
            //    NHISOnlineExtCommon.Utilities.GeneralUtilities.IsTestEnvironment)
            //{
            ////
            ////    REMOVE THIS FOR LIVE. THIS IS JUST FOR THE TEMP DEV SSL CERTIFICATE
            ////
            ServicePointManager.ServerCertificateValidationCallback = delegate
            {
                return true;
            };

            ////
            ////    REMOVE ABOVE FOR LIVE. THIS IS JUST FOR THE TEMP DEV SSL CERTIFICATE
            ////
            //}

            var cf = new ChannelFactory<ISecurity>(binding, ServiceBaseAddress);

            //cf.Endpoint.Behaviors.Add(new wsHttpBehavior());
            SecurityChannel = cf.CreateChannel();
        }
    }
}
