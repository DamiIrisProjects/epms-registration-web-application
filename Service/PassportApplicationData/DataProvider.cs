﻿using System;
using System.Data;
using System.Data.OracleClient;

namespace PassportApplicationData
{
    public class DataProvider
    {
        private OracleConnection _oracleConnection;
        private OracleCommand _oracleSelectCommand;
        private string _connectionstring;

        public string ConnectionString
        {
            set
            {
                //connectionString
                _oracleConnection = new OracleConnection(value);
                _oracleSelectCommand = new OracleCommand(string.Empty, _oracleConnection);
                _connectionstring = value;
            }
        }

        public void CreateConnection()
        {
            if (_oracleConnection == null)
            {
                _oracleConnection = new OracleConnection(_connectionstring);
            }
        }

        public int ExecuteQuery(OracleParameter[] parameters)
        {
            int iRowsAffected;
            var cmd = _oracleSelectCommand;

            try
            {
                if (cmd.Connection.State == ConnectionState.Closed)
                    cmd.Connection.Open();

                if (parameters != null && parameters.Length > 0)
                {
                    cmd.Parameters.Clear();

                    for (var i = 0; i < parameters.Length; i++)
                    {
                        if (parameters[i] != null)
                            cmd.Parameters.Add(parameters[i]);
                    }
                }

                iRowsAffected = cmd.ExecuteNonQuery();
            }
            finally
            {
                if (_oracleConnection.State != ConnectionState.Closed)
                {
                    _oracleConnection.Close();
                }
            }

            return iRowsAffected;
        }

        public int ExecuteQuery(string strQuery, OracleParameter[] parameters)
        {
            int iRowsAffected;

            try
            {
                if (_oracleConnection == null)
                {
                    CreateConnection();
                }

                if (_oracleConnection.State != ConnectionState.Open)
                    _oracleConnection.Open();

                using (var cmd = _oracleConnection.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        cmd.Parameters.AddRange(ConvertNullToDbNullValue(parameters));
                    }

                    cmd.Connection = _oracleConnection;
                    iRowsAffected = cmd.ExecuteNonQuery();
                }
            }
            finally
            {
                if (_oracleConnection != null && _oracleConnection.State != ConnectionState.Closed)
                {
                    _oracleConnection.Close();
                }
            }
          
            return iRowsAffected;
        }

        public OracleDataReader SelectQuery(string strQuery, OracleParameter[] parameters)
        {
            try
            {
                // Make sure there is a connection before running this
                if (_oracleConnection == null)
                {
                    CreateConnection();
                }

                if (_oracleConnection.State != ConnectionState.Open)
                    _oracleConnection.Open();

                using (var cmd = _oracleConnection.CreateCommand())
                {
                    cmd.CommandText = strQuery.Replace("\r\n", "\n");
                    if (parameters != null && parameters.Length > 0)
                    {
                        cmd.Parameters.AddRange(ConvertNullToDbNullValue(parameters));
                    }

                    var result = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    return result;
                }
            }
            finally
            {
                if (_oracleConnection != null && _oracleConnection.State != ConnectionState.Closed)
                {
                    _oracleConnection.Close();
                }
            }
        }

        public void RunProcedure(string strQuery, OracleParameter[] parameters, DataSet dataSet)
        {
            try
            {
                // Make sure there is a connection before running this
                if (_oracleConnection == null)
                {
                    CreateConnection();
                }

                if (_oracleConnection.State != ConnectionState.Open)
                    _oracleConnection.Open();

                using (var cmd = _oracleConnection.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        foreach (OracleParameter p in parameters)
                        {
                            cmd.Parameters.Add(p);
                        }
                    }

                    using (var oracleDa = new OracleDataAdapter())
                    {
                        oracleDa.SelectCommand = cmd;
                        oracleDa.Fill(dataSet);
                        _oracleConnection.Close();
                    }
                }
            }
            finally
            {
                if (_oracleConnection != null && _oracleConnection.State != ConnectionState.Closed)
                {
                    _oracleConnection.Close();
                }
            }
        }

        protected object ExecuteScalar(string query, OracleParameter[] parameters, object defaultValue)
        {
            try
            {
                if (_oracleConnection.ConnectionString == string.Empty)
                    throw new ApplicationException("Please provide a connection string before executing a query.");

                object returnValue;

                _oracleConnection.Open();
                using (var oracleQuery = new OracleCommand(query.Replace("\r\n", "\n")))
                {
                    if (parameters != null && parameters.Length > 0)
                        oracleQuery.Parameters.AddRange(ConvertNullToDbNullValue(parameters));

                    oracleQuery.Connection = _oracleConnection;
                    returnValue = IsNullObject(oracleQuery.ExecuteScalar(), defaultValue);
                    oracleQuery.Dispose();
                }
                return returnValue;
            }
            finally
            {
                if (_oracleConnection.State != ConnectionState.Closed)
                {
                    _oracleConnection.Close();
                }
            }
        }

        public void CloseQuery()
        {
            _oracleConnection.Close();
        }

        public int GetLastInsert(string tablename)
        {
            try
            {
                if (_oracleConnection == null)
                {
                    CreateConnection();
                }

                if (_oracleConnection.State != ConnectionState.Open)
                    _oracleConnection.Open();

                using (var cmd = _oracleConnection.CreateCommand())
                {
                    cmd.CommandText = "SELECT IDENT_CURRENT('" + tablename + "')";
                    cmd.Connection = _oracleConnection;
                    return Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
            finally
            {
                if (_oracleConnection != null && _oracleConnection.State != ConnectionState.Closed)
                {
                    _oracleConnection.Close();
                }
            }
        }

        protected long GetNextSequenceId(string sequenceName)
        {
            Int64 sequenceNumber = 0;

            var query = String.Format("SELECT {0}.nextval as NEXTSEQ from dual", sequenceName);

            try
            {
                using (var dset = new DataSet())
                {
                    RunProcedure(query, null, dset);

                    foreach (DataRow oReader in dset.Tables[0].Rows)
                    {
                        var readerString = oReader["NEXTSEQ"].ToString();

                        Int64.TryParse(readerString, out sequenceNumber);
                    }
                }
            }
            finally
            {
                if (_oracleConnection.State != ConnectionState.Closed)
                {
                    _oracleConnection.Close();
                }
            }

            return sequenceNumber;
        }

        protected OracleParameter[] ConvertNullToDbNullValue(OracleParameter[] parameters)
        {
            if (parameters == null)
            {
                return null;
            }

            foreach (OracleParameter parameter in parameters)
            {
                parameter.IsNullable = true;
                if (parameter.Value == null)
                {
                    parameter.Value = DBNull.Value;
                }
            }

            return parameters;
        }

        public static object IsNullObject(object input, object defaultOutput)
        {
            if (input == null || input == DBNull.Value)
            {
                return defaultOutput;
            }
            return input;
        }
    }
}