﻿using PassportApplicationCommon.Enum;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Globalization;
using System.Linq;
using PassportApplicationCommon.Models;

namespace PassportApplicationData
{
    public class EnrollmentData : DataProvider
    {
        #region Reusable variables

        private Dictionary<string, string> _countries; // Instead of multiple queries to the database per request, it just does one 
        
        #endregion

        #region Public operations

        public EnrollmentData()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["ApplicationConnection"].ConnectionString;
        }

        public PassportApplication SaveEnrollment(ref PassportApplication application)
        {
            // Verify if the payment reference has been used before
            if (!string.IsNullOrEmpty(application.FormNo) && PaymentReferenceExist(application.Payment.ApplicationId, application.Payment.ReferenceNumber, application.FormNo))
            {
                application.ErrorMessage = "The payment is already linked to a different application";
                return application;
            }

            // If its a new form
            if (string.IsNullOrEmpty(application.FormNo))
            {
                application.FormNo = CreateFormNo();

                application.FileNo = CreateFileNo(application.FormNo);
            }
            else
            {
                // If we are updating a previously started form
                UpdateEnrollment(application);
                return application;
            }

            // Save Data
            var query = @"INSERT INTO APPLICATION 
                            (
                                FORMNO,APPREASON,BRANCHCODE,OLDDOCNO,DOCUMENTTYPE,FILENO,STAGECODE,AID,REFID,
                                SURNAME,FIRSTNAME,BIRTHDATE,PLACE_OF_BIRTH,HOME_TOWN,SEX,
                                NATIONALITY,PERSONALNO,OTHERNAMES,STATE_OF_ORIGIN,OCCUPATION,HEIGHT,EYECOLOR,HAIRCOLOR,MARITALSTATUS,
                                MAIDENNAME,NOKNAME,NOKADDRESS,TITLE,EMAIL,CONTACTPHONE,MOBILEPHONE,
                                ADDRESS_LINE_1, ADDRESS_LINE_2, CITY, STATE, LGA, DISTRICT, POST_CODE, COUNTRY,
                                NOK_ADDRESS_LINE_1, NOK_ADDRESS_LINE_2, NOK_CITY, NOK_STATE, NOK_LGA, NOK_DISTRICT, NOK_POST_CODE, NOK_COUNTRY, NOK_RELATIONSHIP, NOK_PHONENUMBER,
                                PAYE_DUE,PAYC_DUE,PAYMENT_STATUS,PAYE_STATUS,PAYE_SYS_CONFIRMDATE,PAYE_SYS_CONFIRM_WHO,PAYE_DATEPAID,PAYE_AMOUNT,
                                PAYE_NAMESURNAME,PAYC_STATUS,PAYC_SYS_CONFIRMDATETIME,PAYC_SYS_CONFIRMWHO,PAYC_DATEPAID,PAYC_AMOUNT,PAYC_NAMESURNAME, STAGECODE
                            )
                            VALUES
                            (
                                :FORMNO,:APPREASON,:BRANCHCODE,:OLDDOCNO,:DOCUMENTTYPE,:FILENO,:STAGECODE,:AID,:REFID,
                                :SURNAME,:FIRSTNAME,TO_DATE(:BIRTHDATE,'dd/MM/YYYY'),:PLACE_OF_BIRTH,:HOME_TOWN,:SEX,
                                :NATIONALITY,:PERSONALNO,:OTHERNAMES,:STATE_OF_ORIGIN,:OCCUPATION,:HEIGHT,:EYECOLOR,:HAIRCOLOR,:MARITALSTATUS,
                                :MAIDENNAME,:NOKNAME,:NOKADDRESS,:TITLE,:EMAIL,:CONTACTPHONE,:MOBILEPHONE,
                                :ADDRESS_LINE_1, :ADDRESS_LINE_2, :CITY, :STATE, :LGA, :DISTRICT, :POST_CODE, :COUNTRY,
                                :NOK_ADDRESS_LINE_1, :NOK_ADDRESS_LINE_2, :NOK_CITY, :NOK_STATE, :NOK_LGA, :NOK_DISTRICT, :NOK_POST_CODE, :NOK_COUNTRY, :NOK_RELATIONSHIP, :NOK_PHONENUMBER,
                                :PAYE_DUE,:PAYC_DUE,:PAYMENT_STATUS,:PAYE_STATUS,TO_DATE(:PAYE_SYS_CONFIRMDATE,'dd/MM/YYYY'),:PAYE_SYS_CONFIRM_WHO,TO_DATE(:PAYE_DATEPAID,'dd/MM/YYYY'),:PAYE_AMOUNT,
                                :PAYE_NAMESURNAME,:PAYC_STATUS,TO_DATE(:PAYC_SYS_CONFIRMDATETIME,'dd/MM/YYYY'),:PAYC_SYS_CONFIRMWHO,TO_DATE(:PAYC_DATEPAID,'dd/MM/YYYY'),:PAYC_AMOUNT,:PAYC_NAMESURNAME, :STAGECODE
                            )";

            OracleParameter[] insertParameters =
            {   
                // Passport Details
                new OracleParameter("FORMNO", application.FormNo),
                new OracleParameter("APPREASON",application.PassportInformation.ApplicationType),
                new OracleParameter("BRANCHCODE",ConfigurationManager.AppSettings["BranchCode"]),
                new OracleParameter("OLDDOCNO",application.PassportInformation.PreviousPassportNumber),
                new OracleParameter("DOCUMENTTYPE",application.PassportInformation.DocumentType),
                new OracleParameter("FILENO",application.FileNo),
                new OracleParameter("AID",application.Payment.ApplicationId),
                new OracleParameter("REFID",application.Payment.ReferenceNumber),

                // Personal Details
                new OracleParameter("SURNAME",application.Payment.Surname),
                new OracleParameter("FIRSTNAME",application.Payment.Firstname),
                new OracleParameter("BIRTHDATE",application.Payment.DateOfBirth),
                new OracleParameter("PLACE_OF_BIRTH",application.Locality.HomeTown),
                new OracleParameter("HOME_TOWN",application.Locality.PlaceOfBirth),
                new OracleParameter("SEX",application.Payment.Gender),
                new OracleParameter("NATIONALITY",application.Locality.Nationality),
                new OracleParameter("PERSONALNO",application.PersonalInformation.IdentityNumber),
                new OracleParameter("OTHERNAMES",application.PersonalInformation.Middlename),
                new OracleParameter("STATE_OF_ORIGIN",application.Locality.StateOfOrigin),
                new OracleParameter("OCCUPATION",application.PersonalInformation.Occupation),
                new OracleParameter("MARITALSTATUS",application.PersonalInformation.MaritialStatus),
                new OracleParameter("MAIDENNAME",application.PersonalInformation.MaidenName),                
                new OracleParameter("TITLE",application.PersonalInformation.Title),

                // Personal Features
                new OracleParameter("HEIGHT",application.PersonalFeatures.HeightInCm),
                new OracleParameter("EYECOLOR",application.PersonalFeatures.ColourOfEyes),
                new OracleParameter("HAIRCOLOR",application.PersonalFeatures.ColourOfHair),                

                // Contact Details
                new OracleParameter("EMAIL",application.PersonalInformation.EmailAddress),
                new OracleParameter("CONTACTPHONE",application.ContactDetails.ContactPhoneNumber),
                new OracleParameter("MOBILEPHONE",application.ContactDetails.MobilePhoneNumber),
                new OracleParameter("ADDRESS_LINE_1",application.ContactDetails.Address.AddressLine1),
                new OracleParameter("ADDRESS_LINE_2",application.ContactDetails.Address.AddressLine2),
                new OracleParameter("CITY",application.ContactDetails.Address.City),
                new OracleParameter("STATE",application.ContactDetails.Address.State),
                new OracleParameter("LGA",application.ContactDetails.Address.Lga),
                new OracleParameter("DISTRICT",application.ContactDetails.Address.District),
                new OracleParameter("POST_CODE",application.ContactDetails.Address.PostalCode),
                new OracleParameter("COUNTRY",application.ContactDetails.Address.Country),

                // NOK
                new OracleParameter("NOKNAME",application.NextOfKin.NextOfKinName),
                new OracleParameter("NOK_ADDRESS_LINE_1",application.NextOfKin.Address.AddressLine1),
                new OracleParameter("NOK_ADDRESS_LINE_2",application.NextOfKin.Address.AddressLine2),
                new OracleParameter("NOK_CITY",application.NextOfKin.Address.City),
                new OracleParameter("NOK_STATE",application.NextOfKin.Address.State),
                new OracleParameter("NOK_LGA",application.NextOfKin.Address.Lga),
                new OracleParameter("NOK_DISTRICT",application.NextOfKin.Address.District),
                new OracleParameter("NOK_POST_CODE",application.NextOfKin.Address.PostalCode),
                new OracleParameter("NOK_COUNTRY",application.NextOfKin.Address.Country),
                new OracleParameter("NOK_RELATIONSHIP",application.NextOfKin.RelationshipWithNextOfKin),
                new OracleParameter("NOK_PHONENUMBER",application.NextOfKin.ContactNumberOfNextOfKin),

                // Payment
                new OracleParameter("PAYE_DUE",application.Payment.PayeDue),
                new OracleParameter("PAYC_DUE",application.Payment.PaycDue),
                new OracleParameter("PAYMENT_STATUS",application.Payment.PaymentStatus),
                new OracleParameter("PAYE_STATUS",application.Payment.PayeStatus),
                new OracleParameter("PAYE_SYS_CONFIRMDATE",application.Payment.PayeSysConfirmdate == DateTime.MinValue?null:application.Payment.PayeSysConfirmdate.ToString("dd/MM/yyyy")),
                new OracleParameter("PAYE_SYS_CONFIRM_WHO",application.Payment.PayeSysConfirmWho),
                new OracleParameter("PAYE_DATEPAID",application.Payment.PayeDatepaid == DateTime.MinValue?null:application.Payment.PayeDatepaid.ToString("dd/MM/yyyy")),
                new OracleParameter("PAYE_AMOUNT",application.Payment.PayeAmount),
                new OracleParameter("PAYE_NAMESURNAME",application.Payment.PayeNamesurname),
                new OracleParameter("PAYC_STATUS",application.Payment.PaycStatus),
                new OracleParameter("PAYC_SYS_CONFIRMDATETIME",application.Payment.PaycSysConfirmdatetime == DateTime.MinValue?null:application.Payment.PaycSysConfirmdatetime.ToString("dd/MM/yyyy")),
                new OracleParameter("PAYC_SYS_CONFIRMWHO",application.Payment.PaycSysConfirmwho),
                new OracleParameter("PAYC_DATEPAID",application.Payment.PaycDatepaid == DateTime.MinValue?null:application.Payment.PaycDatepaid.ToString("dd/MM/yyyy")),
                new OracleParameter("PAYC_AMOUNT",application.Payment.PaycAmount),
                new OracleParameter("PAYC_NAMESURNAME",application.Payment.PaycNamesurname),

                // If form completed, update the application status
                new OracleParameter("STAGECODE", application.IsCompleted ? "EM1500" : "EM1000")
            };

            ExecuteQuery(query, insertParameters);

            return application;
        }

        public PassportApplication GetEnrollment(int applicationId, int referenceNo)
        {
            // Prepare reusable lists
            SetReusableVariables();

            var application = new PassportApplication();

            var query = @"SELECT FORMNO,APPREASON,BRANCHCODE,OLDDOCNO,DOCUMENTTYPE,FILENO,STAGECODE,AID,REFID,
                                SURNAME,FIRSTNAME,to_char(BIRTHDATE,'yyyy-mm-dd') as DOB,PLACE_OF_BIRTH,HOME_TOWN,SEX,
                                NATIONALITY,PERSONALNO,OTHERNAMES,STATE_OF_ORIGIN,OCCUPATION,HEIGHT,EYECOLOR,HAIRCOLOR,MARITALSTATUS,
                                MAIDENNAME,NOKNAME,TITLE,EMAIL,CONTACTPHONE,MOBILEPHONE,
                                ADDRESS_LINE_1, ADDRESS_LINE_2, CITY, STATE, LGA, DISTRICT, POST_CODE, COUNTRY, 
                                NOK_ADDRESS_LINE_1, NOK_ADDRESS_LINE_2, NOK_CITY, NOK_STATE, NOK_LGA, NOK_DISTRICT, NOK_POST_CODE, NOK_COUNTRY, NOK_RELATIONSHIP, NOK_PHONENUMBER, STAGECODE
                                From APPLICATION Where AID = :ApplicationID and REFID = :ReferenceNo";

            OracleParameter[] selectParameters =
            {                 
                new OracleParameter("ApplicationID", applicationId),
                new OracleParameter("ReferenceNo", referenceNo)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, selectParameters, dset);

                foreach (DataRow oReader in dset.Tables[0].Rows)
                {
                    int convInt;

                    // Application Details
                    application.FormNo = oReader["FORMNO"].ToString();
                    application.FileNo = oReader["FILENO"].ToString();
                    application.StageCode = oReader["STAGECODE"].ToString();

                    if (int.TryParse(oReader["AID"].ToString(), out convInt))
                    {
                        application.Payment.ApplicationId = convInt;
                        application.ApplicationId = convInt;
                    }

                    if (int.TryParse(oReader["REFID"].ToString(), out convInt))
                        application.Payment.ReferenceNumber = convInt;

                    // Passport information
                    application.PassportInformation.PreviousPassportNumber = oReader["OLDDOCNO"].ToString();
                    if (int.TryParse(oReader["APPREASON"].ToString(), out convInt))
                        application.PassportInformation.ApplicationType = convInt;

                    if (int.TryParse(oReader["DOCUMENTTYPE"].ToString(), out convInt))
                        application.PassportInformation.DocumentType = convInt;

                    if (int.TryParse(oReader["AID"].ToString(), out convInt))
                        application.Payment.ApplicationId = convInt;

                    if (int.TryParse(oReader["REFID"].ToString(), out convInt))
                        application.Payment.ReferenceNumber = convInt;

                    // Personal Information
                    application.PersonalInformation.Surname = application.Payment.Surname = oReader["SURNAME"].ToString().ToUpper();
                    application.PersonalInformation.Firstname = application.Payment.Firstname = oReader["FIRSTNAME"].ToString().ToUpper();
                    application.PersonalInformation.Gender = application.Payment.Gender = oReader["SEX"].ToString();
                    application.PersonalInformation.Middlename = oReader["OTHERNAMES"].ToString().ToUpper();
                    application.PersonalInformation.IdentityNumber = oReader["PERSONALNO"].ToString();
                    application.PersonalInformation.Occupation = oReader["OCCUPATION"].ToString();
                    application.PersonalInformation.MaritialStatus = string.IsNullOrEmpty(oReader["MARITALSTATUS"].ToString()) ? 0 : int.Parse(oReader["MARITALSTATUS"].ToString());
                    application.PersonalInformation.MaidenName = oReader["MAIDENNAME"].ToString();
                    application.PersonalInformation.Title = (Title)Enum.Parse(typeof(Title), oReader["TITLE"].ToString(), true);
                    application.PersonalInformation.EmailAddress = oReader["EMAIL"].ToString();

                    DateTime convDate;
                    if (DateTime.TryParseExact(oReader["DOB"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out convDate))
                    {
                        application.PersonalInformation.DateOfBirth = application.Payment.DateOfBirth = convDate;
                    }

                    // Locality
                    application.Locality.HomeTown = oReader["PLACE_OF_BIRTH"].ToString();
                    application.Locality.PlaceOfBirth = oReader["HOME_TOWN"].ToString();
                    application.Locality.Nationality = oReader["NATIONALITY"].ToString();
                    application.Locality.NationalityName = GetCountryName(application.Locality.Nationality);
                    application.Locality.StateOfOrigin = oReader["STATE_OF_ORIGIN"].ToString();

                    // Personal features
                    if (int.TryParse(oReader["HEIGHT"].ToString(), out convInt))
                        application.PersonalFeatures.HeightInCm = convInt;

                    application.PersonalFeatures.ColourOfEyes = string.IsNullOrEmpty(oReader["EYECOLOR"].ToString()) ? 0 : int.Parse(oReader["EYECOLOR"].ToString());
                    application.PersonalFeatures.ColourOfHair = string.IsNullOrEmpty(oReader["HAIRCOLOR"].ToString()) ? 0 : int.Parse(oReader["HAIRCOLOR"].ToString());

                    // NOK
                    application.NextOfKin.NextOfKinName = oReader["NOKNAME"].ToString();
                    application.NextOfKin.Address.AddressLine1 = oReader["NOK_ADDRESS_LINE_1"].ToString();
                    application.NextOfKin.Address.AddressLine2 = oReader["NOK_ADDRESS_LINE_2"].ToString();
                    application.NextOfKin.Address.City = oReader["NOK_CITY"].ToString();
                    application.NextOfKin.Address.Country = oReader["NOK_COUNTRY"].ToString();
                    application.NextOfKin.Address.CountryName = GetCountryName(application.NextOfKin.Address.Country);
                    application.NextOfKin.Address.District = oReader["NOK_DISTRICT"].ToString();
                    application.NextOfKin.Address.Lga = oReader["NOK_LGA"].ToString();
                    application.NextOfKin.Address.PostalCode = oReader["NOK_POST_CODE"].ToString();
                    application.NextOfKin.Address.State = oReader["NOK_STATE"].ToString();
                    application.NextOfKin.RelationshipWithNextOfKin = oReader["NOK_RELATIONSHIP"].ToString();
                    application.NextOfKin.ContactNumberOfNextOfKin = oReader["NOK_PHONENUMBER"].ToString();

                    // Contact Details
                    application.ContactDetails.MobilePhoneNumber = oReader["MOBILEPHONE"].ToString();
                    application.ContactDetails.ContactPhoneNumber = oReader["CONTACTPHONE"].ToString();
                    application.ContactDetails.Address.AddressLine1 = oReader["ADDRESS_LINE_1"].ToString();
                    application.ContactDetails.Address.AddressLine2 = oReader["ADDRESS_LINE_2"].ToString();
                    application.ContactDetails.Address.City = oReader["CITY"].ToString();
                    application.ContactDetails.Address.Country = oReader["COUNTRY"].ToString();
                    application.ContactDetails.Address.CountryName = GetCountryName(application.ContactDetails.Address.Country);
                    application.ContactDetails.Address.District = oReader["DISTRICT"].ToString();
                    application.ContactDetails.Address.Lga = oReader["LGA"].ToString();
                    application.ContactDetails.Address.PostalCode = oReader["POST_CODE"].ToString();
                    application.ContactDetails.Address.State = oReader["STATE"].ToString();
                }
            }

            // Get breeder docs attached to this formno
            application.BreederDocs = GetBreederDocuments(application.FormNo);

            return application;
        }

        public PassportApplication GetEnrollmentByFormNumber(string formNumber)
        {
            var application = new PassportApplication();

            var query = @"SELECT FORMNO,APPREASON,BRANCHCODE,OLDDOCNO,DOCUMENTTYPE,FILENO,STAGECODE,AID,REFID,
                                SURNAME,FIRSTNAME,to_char(BIRTHDATE,'yyyy-mm-dd') as DOB,PLACE_OF_BIRTH,HOME_TOWN,SEX,
                                NATIONALITY,PERSONALNO,OTHERNAMES,STATE_OF_ORIGIN,OCCUPATION,HEIGHT,EYECOLOR,HAIRCOLOR,MARITALSTATUS,
                                MAIDENNAME,NOKNAME,TITLE,EMAIL,CONTACTPHONE,MOBILEPHONE,
                                ADDRESS_LINE_1, ADDRESS_LINE_2, CITY, STATE, LGA, DISTRICT, POST_CODE, COUNTRY, 
                                NOK_ADDRESS_LINE_1, NOK_ADDRESS_LINE_2, NOK_CITY, NOK_STATE, NOK_LGA, NOK_DISTRICT, NOK_POST_CODE, NOK_COUNTRY, NOK_RELATIONSHIP, NOK_PHONENUMBER, STAGECODE,
                                E.REJECT_REASON_ID, SCREENING_COMMENT, REJECT_REASON
                                From APPLICATION E LEFT JOIN LOOKUP_ENROL_REJECTREASONS L ON E.REJECT_REASON_ID = L.REJECT_REASON_ID Where FORMNO = :FORMNO";

            OracleParameter[] selectParameters =
            {                 
                new OracleParameter("FORMNO", formNumber)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, selectParameters, dset);

                foreach (DataRow oReader in dset.Tables[0].Rows)
                {
                    int convInt;

                    // Application Details
                    application.FormNo = oReader["FORMNO"].ToString();
                    application.PassportInformation.PreviousPassportNumber = oReader["OLDDOCNO"].ToString();
                    application.FileNo = oReader["FILENO"].ToString();
                    application.StageCode = oReader["STAGECODE"].ToString();

                    if (int.TryParse(oReader["AID"].ToString(), out convInt))
                    {
                        application.Payment.ApplicationId = convInt;
                        application.ApplicationId = convInt;
                    }

                    if (int.TryParse(oReader["REFID"].ToString(), out convInt))
                        application.Payment.ReferenceNumber = convInt;

                    if (int.TryParse(oReader["APPREASON"].ToString(), out convInt))
                        application.PassportInformation.ApplicationType = convInt;

                    if (int.TryParse(oReader["DOCUMENTTYPE"].ToString(), out convInt))
                        application.PassportInformation.DocumentType = convInt;

                    if (int.TryParse(oReader["AID"].ToString(), out convInt))
                        application.Payment.ApplicationId = convInt;

                    if (int.TryParse(oReader["REFID"].ToString(), out convInt))
                        application.Payment.ReferenceNumber = convInt;

                    // Personal Information
                    application.PersonalInformation.Surname = application.Payment.Surname = oReader["SURNAME"].ToString().ToUpper();
                    application.PersonalInformation.Firstname = application.Payment.Firstname = oReader["FIRSTNAME"].ToString().ToUpper();
                    application.PersonalInformation.Gender = application.Payment.Gender = oReader["SEX"].ToString();
                    application.PersonalInformation.Middlename = oReader["OTHERNAMES"].ToString().ToUpper();
                    application.PersonalInformation.IdentityNumber = oReader["PERSONALNO"].ToString();
                    application.PersonalInformation.Occupation = oReader["OCCUPATION"].ToString();
                    application.PersonalInformation.MaritialStatus = string.IsNullOrEmpty(oReader["MARITALSTATUS"].ToString()) ? 0 : int.Parse(oReader["MARITALSTATUS"].ToString());
                    application.PersonalInformation.MaidenName = oReader["MAIDENNAME"].ToString();
                    application.PersonalInformation.Title = (Title)Enum.Parse(typeof(Title), oReader["TITLE"].ToString(), true);
                    application.PersonalInformation.EmailAddress = oReader["EMAIL"].ToString();

                    DateTime convDate;
                    if (DateTime.TryParseExact(oReader["DOB"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out convDate))
                    {
                        application.PersonalInformation.DateOfBirth = application.Payment.DateOfBirth = convDate;
                    }

                    // Locality
                    application.Locality.HomeTown = oReader["PLACE_OF_BIRTH"].ToString();
                    application.Locality.PlaceOfBirth = oReader["HOME_TOWN"].ToString();
                    application.Locality.Nationality = oReader["NATIONALITY"].ToString();
                    application.Locality.StateOfOrigin = oReader["STATE_OF_ORIGIN"].ToString();

                    // Personal features
                    if (int.TryParse(oReader["HEIGHT"].ToString(), out convInt))
                        application.PersonalFeatures.HeightInCm = convInt;

                    application.PersonalFeatures.ColourOfEyes = string.IsNullOrEmpty(oReader["EYECOLOR"].ToString()) ? 0 : int.Parse(oReader["EYECOLOR"].ToString());
                    application.PersonalFeatures.ColourOfHair = string.IsNullOrEmpty(oReader["HAIRCOLOR"].ToString()) ? 0 : int.Parse(oReader["HAIRCOLOR"].ToString());

                    // NOK
                    application.NextOfKin.NextOfKinName = oReader["NOKNAME"].ToString();
                    application.NextOfKin.Address.AddressLine1 = oReader["NOK_ADDRESS_LINE_1"].ToString();
                    application.NextOfKin.Address.AddressLine2 = oReader["NOK_ADDRESS_LINE_2"].ToString();
                    application.NextOfKin.Address.City = oReader["NOK_CITY"].ToString();
                    application.NextOfKin.Address.Country = oReader["NOK_COUNTRY"].ToString();
                    application.NextOfKin.Address.District = oReader["NOK_DISTRICT"].ToString();
                    application.NextOfKin.Address.Lga = oReader["NOK_LGA"].ToString();
                    application.NextOfKin.Address.PostalCode = oReader["NOK_POST_CODE"].ToString();
                    application.NextOfKin.Address.State = oReader["NOK_STATE"].ToString();
                    application.NextOfKin.RelationshipWithNextOfKin = oReader["NOK_RELATIONSHIP"].ToString();
                    application.NextOfKin.ContactNumberOfNextOfKin = oReader["NOK_PHONENUMBER"].ToString();

                    // Contact Details
                    application.ContactDetails.MobilePhoneNumber = oReader["MOBILEPHONE"].ToString();
                    application.ContactDetails.ContactPhoneNumber = oReader["CONTACTPHONE"].ToString();
                    application.ContactDetails.Address.AddressLine1 = oReader["ADDRESS_LINE_1"].ToString();
                    application.ContactDetails.Address.AddressLine2 = oReader["ADDRESS_LINE_2"].ToString();
                    application.ContactDetails.Address.City = oReader["CITY"].ToString();
                    application.ContactDetails.Address.Country = oReader["COUNTRY"].ToString();
                    application.ContactDetails.Address.District = oReader["DISTRICT"].ToString();
                    application.ContactDetails.Address.Lga = oReader["LGA"].ToString();
                    application.ContactDetails.Address.PostalCode = oReader["POST_CODE"].ToString();
                    application.ContactDetails.Address.State = oReader["STATE"].ToString();

                    // Reject reasons if they exist
                    application.RejectionReason = int.Parse(oReader["REJECT_REASON_ID"].ToString());
                    application.RejectionReasonDetails = oReader["SCREENING_COMMENT"].ToString();
                    application.RejectionReasonHeader = oReader["REJECT_REASON"].ToString();
                }
            }

            SetReusableVariables();
            application.Locality.NationalityName = GetCountryName(application.Locality.Nationality);
            application.NextOfKin.Address.CountryName = GetCountryName(application.NextOfKin.Address.Country);
            application.ContactDetails.Address.CountryName = GetCountryName(application.ContactDetails.Address.Country);

            // Get breeder docs attached to this formno
            application.BreederDocs = GetBreederDocuments(application.FormNo);

            return application;
        }

        public List<PassportApplication> GetEnrollments(int type)
        {
            var result = new List<PassportApplication>();

            var query = @"Select FORMNO,DOCUMENTTYPE,FILENO,STAGECODE,AID,REFID,
                                SURNAME,FIRSTNAME,to_char(BIRTHDATE,'yyyy-mm-dd') as DOB, SEX
                                From APPLICATION";

            if (type == 1)
                query += " where stagecode = 'EM0500' OR stagecode = 'EM1000'";

            if (type == 2)
                query += " where stagecode = 'EM1500'";

            if (type == 3)
                query += " where stagecode = 'EM1400' and REJECT_REASON_ID != 0";

            if (type == 4)
                query += " where stagecode = 'EM2000'";

            query += " order by FormNo";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow oReader in dset.Tables[0].Rows)
                {
                    var application = new PassportApplication();
                    int convInt;

                    // Application Details
                    application.FormNo = oReader["FORMNO"].ToString();
                    application.FileNo = oReader["FILENO"].ToString();

                    if (int.TryParse(oReader["AID"].ToString(), out convInt))
                    {
                        application.ApplicationId = convInt;
                    }

                    if (int.TryParse(oReader["REFID"].ToString(), out convInt))
                        application.ReferenceNumber = convInt;

                    if (int.TryParse(oReader["DOCUMENTTYPE"].ToString(), out convInt))
                        application.PassportInformation.DocumentType = convInt;

                    if (int.TryParse(oReader["AID"].ToString(), out convInt))
                        application.Payment.ApplicationId = convInt;

                    // Personal Information
                    application.PersonalInformation.Surname = oReader["SURNAME"].ToString();
                    application.PersonalInformation.Firstname = oReader["FIRSTNAME"].ToString();
                    application.PersonalInformation.Gender = oReader["SEX"].ToString();

                    DateTime convDate;
                    if (DateTime.TryParseExact(oReader["DOB"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out convDate))
                    {
                        application.PersonalInformation.DateOfBirth = convDate;
                    }

                    result.Add(application);
                }
            }

            return result;
        }

        public PassportApplication GetNextScreening()
        {
            var application = new PassportApplication();

            var query = @"Select FORMNO,APPREASON,BRANCHCODE,OLDDOCNO,DOCUMENTTYPE,FILENO,STAGECODE,AID,REFID,
                                SURNAME,FIRSTNAME,to_char(BIRTHDATE,'yyyy-mm-dd') as DOB,PLACE_OF_BIRTH,HOME_TOWN,SEX,
                                NATIONALITY,PERSONALNO,OTHERNAMES,STATE_OF_ORIGIN,OCCUPATION,HEIGHT,EYECOLOR,HAIRCOLOR,MARITALSTATUS,
                                MAIDENNAME,NOKNAME,NOKADDRESS,TITLE,EMAIL,CONTACTPHONE,MOBILEPHONE
                                From APPLICATION"; //"Where STAGECODE = 'EM1000'";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow oReader in dset.Tables[0].Rows)
                {
                    int convInt;

                    // Application Details
                    application.FormNo = oReader["FORMNO"].ToString();
                    application.PassportInformation.PreviousPassportNumber = oReader["OLDDOCNO"].ToString();
                    application.FileNo = oReader["FILENO"].ToString();
                    application.StageCode = oReader["STAGECODE"].ToString();

                    if (int.TryParse(oReader["AID"].ToString(), out convInt))
                    {
                        application.Payment.ApplicationId = convInt;
                        application.ApplicationId = convInt;
                    }

                    if (int.TryParse(oReader["REFID"].ToString(), out convInt))
                        application.Payment.ReferenceNumber = convInt;

                    if (int.TryParse(oReader["APPREASON"].ToString(), out convInt))
                        application.PassportInformation.ApplicationType = convInt;

                    if (int.TryParse(oReader["DOCUMENTTYPE"].ToString(), out convInt))
                        application.PassportInformation.DocumentType = convInt;

                    if (int.TryParse(oReader["AID"].ToString(), out convInt))
                        application.Payment.ApplicationId = convInt;

                    if (int.TryParse(oReader["REFID"].ToString(), out convInt))
                        application.Payment.ReferenceNumber = convInt;

                    // Personal Information
                    application.PersonalInformation.Surname = application.Payment.Surname = oReader["SURNAME"].ToString();
                    application.PersonalInformation.Firstname = application.Payment.Firstname = oReader["FIRSTNAME"].ToString();
                    application.PersonalInformation.Gender = application.Payment.Gender = oReader["SEX"].ToString();
                    application.PersonalInformation.Middlename = oReader["OTHERNAMES"].ToString();
                    application.PersonalInformation.IdentityNumber = oReader["PERSONALNO"].ToString();
                    application.PersonalInformation.Occupation = oReader["OCCUPATION"].ToString();
                    application.PersonalInformation.MaritialStatus = string.IsNullOrEmpty(oReader["MARITALSTATUS"].ToString()) ? 0 : int.Parse(oReader["MARITALSTATUS"].ToString());
                    application.PersonalInformation.MaidenName = oReader["MAIDENNAME"].ToString();
                    application.PersonalInformation.Title = (Title)Enum.Parse(typeof(Title), oReader["TITLE"].ToString(), true);
                    application.PersonalInformation.EmailAddress = oReader["EMAIL"].ToString();

                    DateTime convDate;
                    if (DateTime.TryParseExact(oReader["DOB"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out convDate))
                    {
                        application.PersonalInformation.DateOfBirth = application.Payment.DateOfBirth = convDate;
                    }

                    // Locality
                    application.Locality.HomeTown = oReader["PLACE_OF_BIRTH"].ToString();
                    application.Locality.PlaceOfBirth = oReader["HOME_TOWN"].ToString();
                    application.Locality.Nationality = oReader["NATIONALITY"].ToString();
                    application.Locality.StateOfOrigin = oReader["STATE_OF_ORIGIN"].ToString();

                    // Personal features
                    if (int.TryParse(oReader["HEIGHT"].ToString(), out convInt))
                        application.PersonalFeatures.HeightInCm = convInt;

                    application.PersonalFeatures.ColourOfEyes = string.IsNullOrEmpty(oReader["EYECOLOR"].ToString()) ? 0 : int.Parse(oReader["EYECOLOR"].ToString());
                    application.PersonalFeatures.ColourOfHair = string.IsNullOrEmpty(oReader["HAIRCOLOR"].ToString()) ? 0 : int.Parse(oReader["HAIRCOLOR"].ToString());

                    // NOK
                    application.NextOfKin.NextOfKinName = oReader["NOKNAME"].ToString();
                    application.NextOfKin.Address.AddressLine1 = oReader["NOKADDRESS"].ToString();

                    // Contact Details
                    application.ContactDetails.MobilePhoneNumber = oReader["MOBILEPHONE"].ToString();
                    application.ContactDetails.ContactPhoneNumber = oReader["CONTACTPHONE"].ToString();
                }
            }

            return application;
        }

        public DocumentType GetDocumentType(int documentTypeId)
        {
            var documentType = new DocumentType();

            var query = @"SELECT ID, DOCTYPE, DESCRIPTION, PAGES, STATUS FROM LOOKUP_DOCUMENTTYPE Where ID = :DocumentTypeID";

            OracleParameter[] selectParameters =
            {                 
                new OracleParameter("DocumentTypeID", documentTypeId)                
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, selectParameters, dset);

                foreach (DataRow oReader in dset.Tables[0].Rows)
                {
                    documentType = SharedClassMap.MapDocumentType(oReader);
                }
            }

            return documentType;
        }

        public int SaveBreederDocs(string docsvariable, string formno)
        {
            foreach (var doc in docsvariable.Split(',').ToList())
            {
                if (!string.IsNullOrEmpty(doc))
                {
                    // Check if the document is already saved
                    var query = "select formno from enrollment_docs_received where formno = :formno and document_id = :document_id";

                    OracleParameter[] selectParameters =
                    {                 
                        new OracleParameter("formno", formno),
                        new OracleParameter("document_id", doc)                
                    };

                    bool exists;

                    using (var dset = new DataSet())
                    {
                        RunProcedure(query, selectParameters, dset);

                        exists = dset.Tables[0].Rows.Count != 0;
                    }

                    if (exists)
                    {
                        query = "update enrollment_docs_received set is_active = 1 where formno = :formno";

                        OracleParameter[] insertParameters =
                        {                 
                            new OracleParameter("formno", formno)               
                        };

                        ExecuteQuery(query, insertParameters);
                    }
                    else
                    {
                        // Add the document
                        query = "insert into enrollment_docs_received (formno, document_id) values (:formno, :document_id)";

                        OracleParameter[] insertParameters =
                        {                 
                            new OracleParameter("formno", formno),
                            new OracleParameter("document_id", doc)               
                        };

                        ExecuteQuery(query, insertParameters);
                    }
                }
            }

            return 1;
        }

        public int RejectEnrollment(string formno, string reasonid, string reason)
        {
            var query = "update APPLICATION set stagecode = 'EM1400', reject_reason_id = :reject_reason_id, screening_comment = :screening_comment where formno = :formno";

            OracleParameter[] insertParameters =
            {                 
                new OracleParameter("reject_reason_id", reasonid),
                new OracleParameter("screening_comment", reason),
                new OracleParameter("formno", formno)  
            };

            return ExecuteQuery(query, insertParameters);
        }

        public int AcceptEnrollment(string formno)
        {
            var query = "update APPLICATION set stagecode = 'EM2000', reject_reason_id = 0, screening_comment = '' where formno = :formno";

            OracleParameter[] insertParameters =
            {                 
                new OracleParameter("formno", formno)  
            };

            return ExecuteQuery(query, insertParameters);
        }

        #endregion

        #region Private operations

        private void UpdateEnrollment(PassportApplication application)
        {
            var query = @"Update APPLICATION set 
                                APPREASON = :APPREASON,BRANCHCODE = :BRANCHCODE,OLDDOCNO = :OLDDOCNO,DOCUMENTTYPE = :DOCUMENTTYPE,FILENO = :FILENO, AID = :AID,REFID = :REFID, 
                                
                                SURNAME = :SURNAME,FIRSTNAME = :FIRSTNAME,BIRTHDATE = TO_DATE(:BIRTHDATE,'dd/MM/YYYY'),PLACE_OF_BIRTH = :PLACE_OF_BIRTH,
                                HOME_TOWN = :HOME_TOWN,SEX = :SEX, NATIONALITY = :NATIONALITY,PERSONALNO = :PERSONALNO,OTHERNAMES = :OTHERNAMES,STATE_OF_ORIGIN = :STATE_OF_ORIGIN,
                                OCCUPATION = :OCCUPATION, MARITALSTATUS = :MARITALSTATUS,MAIDENNAME = :MAIDENNAME, TITLE = :TITLE,

                                HEIGHT = :HEIGHT,EYECOLOR = :EYECOLOR,HAIRCOLOR = :HAIRCOLOR,

                                EMAIL = :EMAIL,CONTACTPHONE = :CONTACTPHONE,MOBILEPHONE = :MOBILEPHONE, ADDRESS_LINE_1 = :ADDRESS_LINE_1, 
                                ADDRESS_LINE_2 = :ADDRESS_LINE_2, CITY = :CITY, STATE = :STATE, LGA = :LGA, DISTRICT = :DISTRICT, POST_CODE = :POST_CODE, COUNTRY = :COUNTRY,

                                NOKNAME = :NOKNAME, NOK_ADDRESS_LINE_1 = :NOK_ADDRESS_LINE_1, NOK_ADDRESS_LINE_2 = :NOK_ADDRESS_LINE_2, NOK_CITY = :NOK_CITY, 
                                NOK_STATE = :NOK_STATE, NOK_LGA = :NOK_LGA, NOK_DISTRICT = :NOK_DISTRICT, NOK_POST_CODE = :NOK_POST_CODE, NOK_COUNTRY = :NOK_COUNTRY, NOK_RELATIONSHIP = :NOK_RELATIONSHIP, NOK_PHONENUMBER = :NOK_PHONENUMBER,

                                PAYE_DUE = :PAYE_DUE,PAYC_DUE = :PAYC_DUE,PAYMENT_STATUS = :PAYMENT_STATUS,PAYE_STATUS = :PAYE_STATUS,PAYE_SYS_CONFIRMDATE = TO_DATE(:PAYE_SYS_CONFIRMDATE,'dd/MM/YYYY'),
                                PAYE_SYS_CONFIRM_WHO = :PAYE_SYS_CONFIRM_WHO,PAYE_DATEPAID = TO_DATE(:PAYE_DATEPAID,'dd/MM/YYYY'),PAYE_AMOUNT = :PAYE_AMOUNT,
                                PAYE_NAMESURNAME = :PAYE_NAMESURNAME,PAYC_STATUS = :PAYC_STATUS,PAYC_SYS_CONFIRMDATETIME = TO_DATE(:PAYC_SYS_CONFIRMDATETIME,'dd/MM/YYYY'),
                                PAYC_SYS_CONFIRMWHO = :PAYC_SYS_CONFIRMWHO, PAYC_DATEPAID = TO_DATE(:PAYC_DATEPAID,'dd/MM/YYYY'),PAYC_AMOUNT = :PAYC_AMOUNT,PAYC_NAMESURNAME = :PAYC_NAMESURNAME,
                                
                                STAGECODE = :STAGECODE
                            Where FORMNO = :FORMNO";
                        
            OracleParameter[] updateParameters =
            {   
                // Passport Details               
                new OracleParameter("APPREASON",application.PassportInformation.ApplicationType),
                new OracleParameter("BRANCHCODE",ConfigurationManager.AppSettings["BranchCode"]),
                new OracleParameter("OLDDOCNO",application.PassportInformation.PreviousPassportNumber),
                new OracleParameter("DOCUMENTTYPE",application.PassportInformation.DocumentType),
                new OracleParameter("FILENO",application.FileNo),
                new OracleParameter("AID",application.Payment.ApplicationId),
                new OracleParameter("REFID",application.Payment.ReferenceNumber),

                // Personal Details
                new OracleParameter("SURNAME",application.Payment.Surname),
                new OracleParameter("FIRSTNAME",application.Payment.Firstname),
                new OracleParameter("BIRTHDATE",application.Payment.DateOfBirth.ToString("dd/MM/yyyy")),
                new OracleParameter("PLACE_OF_BIRTH",application.Locality.HomeTown),
                new OracleParameter("HOME_TOWN",application.Locality.PlaceOfBirth),
                new OracleParameter("SEX",application.Payment.Gender),
                new OracleParameter("NATIONALITY",application.Locality.Nationality),
                new OracleParameter("PERSONALNO",application.PersonalInformation.IdentityNumber),
                new OracleParameter("OTHERNAMES",application.PersonalInformation.Middlename),
                new OracleParameter("STATE_OF_ORIGIN",application.Locality.StateOfOrigin),
                new OracleParameter("OCCUPATION",application.PersonalInformation.Occupation),
                new OracleParameter("MARITALSTATUS",application.PersonalInformation.MaritialStatus),
                new OracleParameter("MAIDENNAME",application.PersonalInformation.MaidenName),                
                new OracleParameter("TITLE",application.PersonalInformation.Title),

                // Personal Features
                new OracleParameter("HEIGHT",application.PersonalFeatures.HeightInCm),
                new OracleParameter("EYECOLOR",application.PersonalFeatures.ColourOfEyes),
                new OracleParameter("HAIRCOLOR",application.PersonalFeatures.ColourOfHair),                

                // Contact Details
                new OracleParameter("EMAIL",application.PersonalInformation.EmailAddress),
                new OracleParameter("CONTACTPHONE",application.ContactDetails.ContactPhoneNumber),
                new OracleParameter("MOBILEPHONE",application.ContactDetails.MobilePhoneNumber),
                new OracleParameter("ADDRESS_LINE_1",application.ContactDetails.Address.AddressLine1),
                new OracleParameter("ADDRESS_LINE_2",application.ContactDetails.Address.AddressLine2),
                new OracleParameter("CITY",application.ContactDetails.Address.City),
                new OracleParameter("STATE",application.ContactDetails.Address.State),
                new OracleParameter("LGA",application.ContactDetails.Address.Lga),
                new OracleParameter("DISTRICT",application.ContactDetails.Address.District),
                new OracleParameter("POST_CODE",application.ContactDetails.Address.PostalCode),
                new OracleParameter("COUNTRY",application.ContactDetails.Address.Country),

                // NOK
                new OracleParameter("NOKNAME",application.NextOfKin.NextOfKinName),
                new OracleParameter("NOK_ADDRESS_LINE_1",application.NextOfKin.Address.AddressLine1),
                new OracleParameter("NOK_ADDRESS_LINE_2",application.NextOfKin.Address.AddressLine2),
                new OracleParameter("NOK_CITY",application.NextOfKin.Address.City),
                new OracleParameter("NOK_STATE",application.NextOfKin.Address.State),
                new OracleParameter("NOK_LGA",application.NextOfKin.Address.Lga),
                new OracleParameter("NOK_DISTRICT",application.NextOfKin.Address.District),
                new OracleParameter("NOK_POST_CODE",application.NextOfKin.Address.PostalCode),
                new OracleParameter("NOK_COUNTRY",application.NextOfKin.Address.Country),
                 new OracleParameter("NOK_RELATIONSHIP",application.NextOfKin.RelationshipWithNextOfKin),
                new OracleParameter("NOK_PHONENUMBER",application.NextOfKin.ContactNumberOfNextOfKin),

                // Payment
                new OracleParameter("PAYE_DUE",application.Payment.PayeDue),
                new OracleParameter("PAYC_DUE",application.Payment.PaycDue),
                new OracleParameter("PAYMENT_STATUS",application.Payment.PaymentStatus),
                new OracleParameter("PAYE_STATUS",application.Payment.PayeStatus),
                new OracleParameter("PAYE_SYS_CONFIRMDATE",application.Payment.PayeSysConfirmdate == DateTime.MinValue?null:application.Payment.PayeSysConfirmdate.ToString("dd/MM/yyyy")),
                new OracleParameter("PAYE_SYS_CONFIRM_WHO",application.Payment.PayeSysConfirmWho),
                new OracleParameter("PAYE_DATEPAID",application.Payment.PayeDatepaid == DateTime.MinValue?null:application.Payment.PayeDatepaid.ToString("dd/MM/yyyy")),
                new OracleParameter("PAYE_AMOUNT",application.Payment.PayeAmount),
                new OracleParameter("PAYE_NAMESURNAME",application.Payment.PayeNamesurname),
                new OracleParameter("PAYC_STATUS",application.Payment.PaycStatus),
                new OracleParameter("PAYC_SYS_CONFIRMDATETIME",application.Payment.PaycSysConfirmdatetime == DateTime.MinValue?null:application.Payment.PaycSysConfirmdatetime.ToString("dd/MM/yyyy")),
                new OracleParameter("PAYC_SYS_CONFIRMWHO",application.Payment.PaycSysConfirmwho),
                new OracleParameter("PAYC_DATEPAID",application.Payment.PaycDatepaid == DateTime.MinValue?null:application.Payment.PaycDatepaid.ToString("dd/MM/yyyy")),
                new OracleParameter("PAYC_AMOUNT",application.Payment.PaycAmount),
                new OracleParameter("PAYC_NAMESURNAME",application.Payment.PaycNamesurname),

                // If form completed, update the application status
                new OracleParameter("STAGECODE", application.IsCompleted ? "EM1500" : "EM1000"),

                new OracleParameter("FORMNO", application.FormNo)
            };

            ExecuteQuery(query, updateParameters);            
        }

        private List<BreederDoc> GetBreederDocuments(string formno)
        {
            var result = new List<BreederDoc>();
            var query = "select document_id from application_docs_received where formno = :formno";

            OracleParameter[] selectParameters =
            {                 
                new OracleParameter("formno", formno)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, selectParameters, dset);

                foreach (DataRow oReader in dset.Tables[0].Rows)
                {
                    result.Add(new BreederDoc() { DocumentId = int.Parse(oReader["document_id"].ToString()) });
                }
            }

            return result;
        }

        private bool PaymentReferenceExist(int applicationId, int referenceNumber, string formNo)
        {
            var query = @"Select FORMNO From APPLICATION Where AID = :ApplicationID and REFID = :ReferenceNo";
            
            OracleParameter[] selectParameters =
            {                 
                new OracleParameter("ApplicationID", applicationId),
                new OracleParameter("ReferenceNo", referenceNumber)                
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, selectParameters, dset);

                foreach (DataRow oReader in dset.Tables[0].Rows)
                {
                    if (string.IsNullOrEmpty(formNo) || oReader["FORMNO"].ToString() != formNo)
                        return true;
                    return false;
                }
            }

            return false;
        }

        private string CreateFileNo(string formNo)
        {
            var fileno = string.Empty;
            //NIS/IKO/1234567

            //NIS – fixed
            //IKO – 2 or 3 letter acronym for a branch (read from the new branch table or config file)
            //1234567 – Sequential file number

            var query = "select GenerateFileno(:FormNo,:BranchCode) as FileNo from dual";

            OracleParameter[] selectParameters =
            {                 
                new OracleParameter("FormNo", formNo),
                new OracleParameter("BranchCode", ConfigurationManager.AppSettings["BranchAcronym"])
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, selectParameters, dset);

                foreach (DataRow oReader in dset.Tables[0].Rows)
                {
                    fileno = oReader["FileNo"].ToString();
                }
            }

            return fileno;
        }

        private string CreateFormNo()
        {
            var formId = GetNextSequenceId("FORMNOBRANCH");

            var paddedFormId = formId.ToString("000000000");

            return ConfigurationManager.AppSettings["BranchCode"] + paddedFormId;
        }

        private void SetReusableVariables()
        {
            _countries = new Dictionary<string, string>();
            var query = "select code, name from lookup_countries";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow oReader in dset.Tables[0].Rows)
                {
                    _countries.Add(oReader["code"].ToString(), oReader["name"].ToString());
                }
            }
        }

        private string GetCountryName(string countrycode)
        {
            if (string.IsNullOrEmpty(countrycode))
                return string.Empty;

            return _countries[countrycode];
        }

        #endregion
    }
}
