﻿using System.Data;
using PassportApplicationCommon.Models;

namespace PassportApplicationData
{
    public static class SharedClassMap
    {
        internal static DocumentType MapDocumentType(DataRow oReader)
        {
            var docType = new DocumentType();

            int convint;

            if (int.TryParse(oReader["ID"].ToString(), out convint))
                docType.Id = convint;

            docType.DocTypeCode = oReader["DOCTYPE"].ToString();

            docType.Description = oReader["DESCRIPTION"].ToString();

            if (int.TryParse(oReader["PAGES"].ToString(), out convint))
                docType.Pages = convint;

            return docType;
        }
    }
}
