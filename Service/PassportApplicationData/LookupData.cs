﻿using PassportApplicationCommon.Model;
using PassportApplicationCommon.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Globalization;
using System.Linq;

namespace PassportApplicationData
{
    public class LookupData : DataProvider
    {
        public LookupData()
        {
            ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ApplicationConnection"].ConnectionString;
        }

        public List<ProcessingCountry> GetCountries()
        {
            var countries = new List<ProcessingCountry>();

            var query = "SELECT ID, NAME, CODE FROM LOOKUP_COUNTRIES ORDER BY NAME";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow oReader in dset.Tables[0].Rows)
                {
                    var country = new ProcessingCountry
                    {
                        CountryName = oReader["NAME"].ToString(),
                        CountryCode = oReader["CODE"].ToString()
                    };
                    countries.Add(country);
                }
            }

            return countries;
        }

        public List<State> GetNigeriaStates()
        {
            var states = new List<State>();

            var query = "SELECT ID, NAME, CODE FROM LOOKUP_STATES ORDER BY NAME";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var state = new State
                    {
                        StateId = int.Parse(row["ID"].ToString()),
                        StateName = row["NAME"].ToString(),
                        Code = row["CODE"].ToString()
                    };
                    states.Add(state);
                }
            }

            return states;
        }

        public List<PassportBranch> GetBranchesForCountry(string countryCode)
        {
            var passportBranches = new List<PassportBranch>
            {
                new PassportBranch()
                {
                    CountryCode = "NG",
                    CountryName = "Nigeria",
                    BranchName = "Akure",
                    BranchCode = 351
                },
                new PassportBranch()
                {
                    CountryCode = "NG",
                    CountryName = "Nigeria",
                    BranchName = "Bauchi",
                    BranchCode = 380
                },
                new PassportBranch()
                {
                    CountryCode = "NG",
                    CountryName = "Nigeria",
                    BranchName = "Gombe",
                    BranchCode = 320
                },
                new PassportBranch()
                {
                    CountryCode = "NG",
                    CountryName = "Nigeria",
                    BranchName = "Sokoto",
                    BranchCode = 331
                },
                new PassportBranch()
                {
                    CountryCode = "US",
                    CountryName = "United States",
                    BranchName = "Atlanta",
                    BranchCode = 547
                },
                new PassportBranch()
                {
                    CountryCode = "US",
                    CountryName = "United States",
                    BranchName = "New York",
                    BranchCode = 537
                },
                new PassportBranch()
                {
                    CountryCode = "US",
                    CountryName = "United States",
                    BranchName = "Washington DC",
                    BranchCode = 557
                },
                new PassportBranch()
                {
                    CountryCode = "ZA",
                    CountryName = "South Africa",
                    BranchName = "Johannesburg",
                    BranchCode = 274
                },
                new PassportBranch()
                {
                    CountryCode = "ZW",
                    CountryName = "South Africa",
                    BranchName = "Johannesburg",
                    BranchCode = 274
                }
            };
            
            return passportBranches.Where(c => c.CountryCode == countryCode).ToList();
        }

        public PassportBranch GetNigeriaMainOffice()
        {
            return new PassportBranch() { CountryCode = "NG", CountryName = "Nigeria", BranchName = "Headquarter", BranchCode = 369 };
        }

        public List<Passport> GetPreviousPassports(string previousPassportNumber)
        {
            var passportList = new List<Passport>();

            #region passport1

            var passport1 = new Passport
            {
                DocNo = "A00001234",
                FirstName = "KOLAWOLE",
                Gender = "M",
                OtherNames = "OSHIOBUGHIE",
                StageCode = "EM5000",
                StageDescription = "ACTIVE",
                Surname = "ADELEYE",
                IssuePlace = "ABUJA",
                DocType = "ORDINARY PASSPORT",
                BirthDate = new DateTime(1950, 10, 23),
                IssueDate = new DateTime(2014, 1, 2),
                ExpiryDate = new DateTime(2019, 1, 2)
            };

            //passport1.TempFace = oReader["FaceImage"] == DBNull.Value ? null : (byte[])oReader["FaceImage"];
            //passport1.TempSign = oReader["SignImage"] == DBNull.Value ? null : (byte[])oReader["SignImage"];

            passportList.Add(passport1);

            #endregion

            #region passport2

            var passport2 = new Passport
            {
                DocNo = "A00001235",
                FirstName = "SANI",
                Gender = "M",
                OtherNames = "OSHIOBUGHIE",
                StageCode = "EM6002",
                StageDescription = "NOT ACTIVE-DOCUMENT RENEW",
                Surname = "ADELEYE",
                IssuePlace = "ABUJA",
                DocType = "DIPLOMATIC PASSPORT",
                BirthDate = new DateTime(1950, 10, 23),
                IssueDate = new DateTime(2012, 1, 2),
                ExpiryDate = new DateTime(2017, 1, 2)
            };

            //passport2.TempFace = oReader["FaceImage"] == DBNull.Value ? null : (byte[])oReader["FaceImage"];
            //passport2.TempSign = oReader["SignImage"] == DBNull.Value ? null : (byte[])oReader["SignImage"];

            passportList.Add(passport2);
            #endregion

            #region passport2

            var passport3 = new Passport
            {
                DocNo = "A00001235",
                FirstName = "SANI",
                Gender = "M",
                OtherNames = "OSHIOBUGHIE",
                StageCode = "EM6003",
                StageDescription = "NOT ACTIVE-DOCUMENT LOST",
                Surname = "ADELEYE",
                IssuePlace = "ABUJA",
                DocType = "OFFICIAL PASSPORT",
                BirthDate = new DateTime(1950, 10, 23),
                IssueDate = new DateTime(2011, 1, 2),
                ExpiryDate = new DateTime(2016, 1, 2)
            };

            //passport2.TempFace = oReader["FaceImage"] == DBNull.Value ? null : (byte[])oReader["FaceImage"];
            //passport2.TempSign = oReader["SignImage"] == DBNull.Value ? null : (byte[])oReader["SignImage"];
            
            passportList.Add(passport3);
            #endregion

            return passportList;
        }

        public List<KeyValuePair<int, string>> GetMaritialStatusList()
        {
            var marStatusList = new List<KeyValuePair<int, string>>();

            var query = "SELECT MARITIALSTATUS, DESCRIPTION FROM LOOKUP_MARITIALSTATUS";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow oReader in dset.Tables[0].Rows)
                {
                    int convint;

                    if (!int.TryParse(oReader["MARITIALSTATUS"].ToString(), out convint))
                        throw new ApplicationException("Could not convert maritial status key to int.");

                    var marStat = new KeyValuePair<int, string>(convint, oReader["DESCRIPTION"].ToString());

                    marStatusList.Add(marStat);
                }
            }

            return marStatusList;
        }

        public List<Gender> GetGenderList()
        {
            var genderList = new List<Gender>();

            var query = "SELECT ID, SEX, DESCRIPTION FROM LOOKUP_SEX where ID < 3";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow oReader in dset.Tables[0].Rows)
                {
                    var gender = new Gender();

                    int convint;

                    if (!int.TryParse(oReader["ID"].ToString(), out convint))
                        throw new ApplicationException("Could not convert gender ID to int.");
                    gender.Id = convint;

                    gender.GenderAbbrev = oReader["SEX"].ToString();

                    gender.Description = oReader["DESCRIPTION"].ToString();

                    genderList.Add(gender);
                }
            }

            return genderList;
        }

        public List<ProcessingColour> GetEyeColours()
        {
            var colours = new List<ProcessingColour>();

            var query = "SELECT ID, EYECOLOR, DESCRIPTION FROM LOOKUP_EYECOLOR";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow oReader in dset.Tables[0].Rows)
                {
                    var colour = new ProcessingColour();

                    int convint;

                    if (!int.TryParse(oReader["EYECOLOR"].ToString(), out convint))
                        throw new ApplicationException("Could not convert EYECOLOR to int.");
                    colour.Colour = convint;

                    colour.Description = oReader["DESCRIPTION"].ToString();

                    colours.Add(colour);
                }
            }

            return colours;
        }

        public List<ProcessingColour> GetHairColours()
        {
            var colours = new List<ProcessingColour>();

            var query = "SELECT ID, HAIRCOLOR, DESCRIPTION FROM LOOKUP_HAIRCOLOR";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow oReader in dset.Tables[0].Rows)
                {
                    var colour = new ProcessingColour();

                    int convint;

                    if (!int.TryParse(oReader["HAIRCOLOR"].ToString(), out convint))
                        throw new ApplicationException("Could not convert HAIRCOLOR to int.");
                    colour.Colour = convint;

                    colour.Description = oReader["DESCRIPTION"].ToString();

                    colours.Add(colour);
                }
            }

            return colours;
        }

        public List<DocumentType> GetDocumentTypes()
        {
            var docTypeList = new List<DocumentType>();

            var query = @"SELECT ID, DOCTYPE, DESCRIPTION, PAGES, STATUS FROM LOOKUP_DOCUMENTTYPE Where Status = 1";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow oReader in dset.Tables[0].Rows)
                {
                    var documentType = SharedClassMap.MapDocumentType(oReader);
                    docTypeList.Add(documentType);
                }
            }

            return docTypeList;
        }

        public List<BreederDoc> GetApplicationTypeDocuments(int appType)
        {
            var docs = new List<BreederDoc>();

            var query = @"SELECT D.DOCUMENT_ID, D.DOCUMENT_NAME, L.IS_MANDATORY FROM APPLICATION_DOCS_APPTYPE_LINK L LEFT JOIN APPLICATION_DOCS D ON D.DOCUMENT_ID = L.DOCUMENT_ID Where L.APPLICATION_TYPE_ID = :APPLICATION_TYPE_ID";
            OracleParameter[] param = {
                new OracleParameter("APPLICATION_TYPE_ID", appType)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param, dset);

                foreach (DataRow oReader in dset.Tables[0].Rows)
                {
                    var doc = new BreederDoc
                    {
                        DocumentId = int.Parse(oReader["DOCUMENT_ID"].ToString()),
                        DocumentName = oReader["DOCUMENT_NAME"].ToString(),
                        IsRequired = int.Parse(oReader["IS_MANDATORY"].ToString()),
                    };

                    docs.Add(doc);
                }
            }

            return docs;
        }

        public List<KeyValuePair<int, string>> GetApplicationReasons()
        {
            var result = new List<KeyValuePair<int, string>>();

            var query = @"SELECT APPLICATION_TYPE_ID, APPLICATION_TYPE FROM LOOKUP_APPTYPE";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow oReader in dset.Tables[0].Rows)
                {
                    result.Add(new KeyValuePair<int, string>(int.Parse(oReader["APPLICATION_TYPE_ID"].ToString()), oReader["APPLICATION_TYPE"].ToString()));
                }
            }

            return result;
        }

        public List<KeyValuePair<int, string>> GetRejectReasons()
        {
            var result = new List<KeyValuePair<int, string>>();
            var query = "SELECT REJECT_REASON_ID, REJECT_REASON FROM LOOKUP_ENROL_REJECTREASONS";

            using (var dset = new DataSet())
            {
                RunProcedure(query, null, dset);

                foreach (DataRow oReader in dset.Tables[0].Rows)
                {
                    result.Add(new KeyValuePair<int, string>(int.Parse(oReader["REJECT_REASON_ID"].ToString()), oReader["REJECT_REASON"].ToString()));
                }
            }

            return result;
        }

        public List<Passport> GetPassportHistory(string previousPassportNumber)
        {
            var passportList = new List<Passport>();

            // Use EPMS database
            ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["PassportConnectionTest"].ConnectionString;

            //string query = @"select Docno,StageCode,Surname,Firstname,OtherName1,Sex,to_char(BirthDate,'yyyy/mm/dd') as BirthDate,FaceImage,SignImage,to_char(issuedate,'yyyy/mm/dd') as IssueDate,DOCTYPE,ISSUEPLACE,to_char(EXPIRYDATE,'yyyy/mm/dd') as EXPIRYDATE from table(get_docholder_hist_V3(:PassportNo))";
            var query = @"select Docno,StageCode,Surname,Firstname,OtherName1,Sex,to_char(BirthDate,'yyyy/mm/dd') as BirthDate,FaceImage,SignImage,to_char(issuedate,'yyyy/mm/dd') as IssueDate,DOCTYPE,ISSUEPLACE,to_char(EXPIRYDATE,'yyyy/mm/dd') as EXPIRYDATE from table(get_docholder_hist_AllTypes_V3(:PassportNo))";
          

            OracleParameter[] selectParameters =
            {                 
                new OracleParameter("PassportNo", previousPassportNumber)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, selectParameters, dset);

                foreach (DataRow oReader in dset.Tables[0].Rows)
                {
                    var passport = new Passport
                    {
                        DocNo = oReader["Docno"].ToString(),
                        FirstName = oReader["Firstname"].ToString(),
                        Gender = oReader["Sex"].ToString(),
                        OtherNames = oReader["OtherName1"].ToString(),
                        StageCode = oReader["StageCode"].ToString(),
                        Surname = oReader["Surname"].ToString(),
                        TempFace = oReader["FaceImage"] == DBNull.Value ? null : (byte[]) oReader["FaceImage"],
                        TempSign = oReader["SignImage"] == DBNull.Value ? null : (byte[]) oReader["SignImage"],
                        IssuePlace = oReader["ISSUEPLACE"].ToString(),
                        DocType = GetDocTypeDescription(oReader["DOCTYPE"].ToString())
                    };



                    DateTime dateConv;

                    if (DateTime.TryParseExact(oReader["BirthDate"].ToString(), "yyyy/MM/dd", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out dateConv))
                    {
                        passport.BirthDate = dateConv;
                    }

                    if (DateTime.TryParseExact(oReader["IssueDate"].ToString(), "yyyy/MM/dd", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out dateConv))
                    {
                        passport.IssueDate = dateConv;
                    }

                    if (DateTime.TryParseExact(oReader["EXPIRYDATE"].ToString(), "yyyy/MM/dd", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out dateConv))
                    {
                        passport.ExpiryDate = dateConv;
                    }

                    passportList.Add(passport);
                }
            }

            return passportList;
        }

        public string GetStageDescription(string stageCode)
        {
            ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["PassportConnection2"].ConnectionString;

            var query = @"SELECT DESCRIPTION FROM STAGECODE where STAGECODE = :STAGECODEID";

            OracleParameter[] parameters =
            {                 
                new OracleParameter("STAGECODEID", stageCode)    
            };

            return ExecuteScalar(query, parameters, "This Document is not in an Issued state.").ToString();
        }

        private string GetDocTypeDescription(string docTypeCode)
        {
            switch (docTypeCode)
            {
                case "P": return "Standard Passport";
                case "PD": return "Diplomatic Passport";
                case "PF": return "Official Passport";
                default: return "Unknown Type";
            }
        }
    }
}


