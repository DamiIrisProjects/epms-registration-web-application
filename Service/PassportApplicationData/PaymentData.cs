﻿using System;
using System.Data;
using System.Data.OracleClient;
using System.Globalization;
using PassportApplicationCommon.Models;

namespace PassportApplicationData
{
    public class PaymentData: DataProvider
    {
        public PaymentData()
        {
            ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ApplicationConnection"].ConnectionString;
        }

        public int GetPaymentAmount(int paymentAmountId, DateTime paymentDate)
        {
            //Go to database to get payment amount
            //Newly created PAYMENT_FEE and PAYMENT_FEE_SCHEDULE Table
            var query = @"SELECT AMOUNT
                            FROM PAYMENT_FEE_SCHEDULE 
                            Where START_DATE < TO_DATE(:PAYDATE,'MM/DD/YYYY') and PAYMENT_FEE_ID = :PAYFEEID
                            Order by START_DATE desc";

            OracleParameter[] selectParameters =
            {   
                new OracleParameter("PAYDATE", paymentDate.ToString("MM/dd/yyyy")),
                new OracleParameter("PAYFEEID", paymentAmountId)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, selectParameters, dset);

                foreach (DataRow oReader in dset.Tables[0].Rows)
                {
                    int convInt;

                    if (int.TryParse(oReader["AMOUNT"].ToString(), out convInt))
                        return convInt;
                }
            }

            throw new ApplicationException("Could not find a valid amount in PAYMENT_FEE_SCHEDULE.");
        }
        
        public Payment GetPayment(int applicationId, int referenceNo)
        {
            Payment payment = new Payment();

            var query = @"SELECT AID, REFID, FIRSTNAME, LASTNAME,to_char(DOB,'yyyy-mm-dd') as DOB, STATEOFBIRTH, SEX, PROCESSINGCOUNTRY, PROCESSINGSTATE, PROCESSINGOFFICE, to_char(MYDATETIME,'yyyy-mm-dd') as MYDATETIME, to_char(PAYMENTDATE,'yyyy-mm-dd') as PAYMENTDATE, 
                            PASSPORTTYPE, to_char(EXPIRYDATE,'yyyy-mm-dd') as EXPIRYDATE, FORMNO, to_char(ENROLLMENTDATE,'yyyy-mm-dd') as ENROLLMENTDATE, AMOUNT_NAIRA, AMOUNT_DOLLAR, PASSPORTSIZE
                            FROM PAYMENT_REF_TABLE WHERE AID = :AID and REFID = :REFID and ApplicationStatus = 'EM0500'";

            //Applicationstatus should always be EM0500 as gerhard only updates the materialised view with unused to conserve bandwidth to the branch
            //The filter is used to enforce the assumption if the DB side do change unexpectedly

            //According to brian wong The flow of applicationStatus is as follows:
            //1) a new payment comes in with the APPLICATIONSTATUS of EM0500(new payment)
            //2) once an payment has been tied to an enrolment ( once it has been cleared for AFIS), the APPLICATIONSTATUS changes to EM1000 (used), the FORMNO of the enrolment, and the ENROLMENTDATE.
            //3) when the enrolment is issued, the APPLICATIONSTATUS is changed to PM5000 (issued).
            
            OracleParameter[] selectParameters =
            {                 
                new OracleParameter("AID", applicationId),
                new OracleParameter("REFID", referenceNo)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, selectParameters, dset);

                foreach (DataRow oReader in dset.Tables[0].Rows)
                {
                    int convInt;

                    if (int.TryParse(oReader["AID"].ToString(), out convInt))
                        payment.ApplicationId = convInt;

                    if (int.TryParse(oReader["REFID"].ToString(), out convInt))
                        payment.ReferenceNo = convInt;

                    payment.Firstname = oReader["FIRSTNAME"].ToString();
                    payment.LastName = oReader["LASTNAME"].ToString();

                    DateTime convDate;

                    if (DateTime.TryParseExact(oReader["DOB"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out convDate))
                    {
                        payment.Dob = convDate;
                    }

                    payment.StateOfBirth = oReader["STATEOFBIRTH"].ToString();
                    payment.Gender = oReader["SEX"].ToString();
                    payment.ProcessingCountry = oReader["PROCESSINGCOUNTRY"].ToString();
                    payment.ProcessingState = oReader["PROCESSINGSTATE"].ToString();
                    payment.ProcessingOffice = oReader["PROCESSINGOFFICE"].ToString();

                    if (DateTime.TryParseExact(oReader["MYDATETIME"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out convDate))
                    {
                        payment.UploadDate = convDate;
                    }

                    if (DateTime.TryParseExact(oReader["PAYMENTDATE"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out convDate))
                    {
                        payment.PaymentDate = convDate;
                    }

                    if (DateTime.TryParseExact(oReader["EXPIRYDATE"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out convDate))
                    {
                        payment.ExpiryDate = convDate;
                    }

                    if (DateTime.TryParseExact(oReader["ENROLLMENTDATE"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out convDate))
                    {
                        payment.EnrollmentDate = convDate;
                    }

                    payment.PassportType = oReader["PASSPORTTYPE"].ToString();
                    payment.FormNo = oReader["FORMNO"].ToString();

                    if (int.TryParse(oReader["AMOUNT_NAIRA"].ToString(), out convInt))
                        payment.AmountNaira = convInt;

                    if (int.TryParse(oReader["AMOUNT_DOLLAR"].ToString(), out convInt))
                        payment.AmountDollar = convInt;

                    if (int.TryParse(oReader["PASSPORTSIZE"].ToString(), out convInt))
                        payment.PassportSize = convInt;

                    return payment;
                }
            }

            // Otherwise get payment details
            payment = GetPaymentDetails(applicationId, referenceNo);
            payment.PaymentStatus = PassportApplicationCommon.Enum.PaymentStatus.Used;

            return payment;
        }

        public Payment GetPaymentDetails(int applicationId, int referenceNo)
        {
            Payment payment = new Payment();

            var query = @"SELECT AID, REFID, FIRSTNAME, LASTNAME,to_char(DOB,'yyyy-mm-dd') as DOB, STATEOFBIRTH, SEX, PROCESSINGCOUNTRY, PROCESSINGSTATE, PROCESSINGOFFICE, to_char(MYDATETIME,'yyyy-mm-dd') as MYDATETIME, to_char(PAYMENTDATE,'yyyy-mm-dd') as PAYMENTDATE, 
                            PASSPORTTYPE, to_char(EXPIRYDATE,'yyyy-mm-dd') as EXPIRYDATE, FORMNO, to_char(ENROLLMENTDATE,'yyyy-mm-dd') as ENROLLMENTDATE, AMOUNT_NAIRA, AMOUNT_DOLLAR, PASSPORTSIZE
                            FROM PAYMENT_REF_TABLE WHERE AID = :AID and REFID = :REFID and ApplicationStatus = 'EM1000'";

            OracleParameter[] selectParameters =
            {                 
                new OracleParameter("AID", applicationId),
                new OracleParameter("REFID", referenceNo)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, selectParameters, dset);

                foreach (DataRow oReader in dset.Tables[0].Rows)
                {
                    int convInt;

                    if (int.TryParse(oReader["AID"].ToString(), out convInt))
                        payment.ApplicationId = convInt;

                    if (int.TryParse(oReader["REFID"].ToString(), out convInt))
                        payment.ReferenceNo = convInt;

                    payment.Firstname = oReader["FIRSTNAME"].ToString();
                    payment.LastName = oReader["LASTNAME"].ToString();

                    DateTime convDate;

                    if (DateTime.TryParseExact(oReader["DOB"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out convDate))
                    {
                        payment.Dob = convDate;
                    }

                    payment.StateOfBirth = oReader["STATEOFBIRTH"].ToString();
                    payment.Gender = oReader["SEX"].ToString();
                    payment.ProcessingCountry = oReader["PROCESSINGCOUNTRY"].ToString();
                    payment.ProcessingState = oReader["PROCESSINGSTATE"].ToString();
                    payment.ProcessingOffice = oReader["PROCESSINGOFFICE"].ToString();

                    if (DateTime.TryParseExact(oReader["MYDATETIME"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out convDate))
                    {
                        payment.UploadDate = convDate;
                    }

                    if (DateTime.TryParseExact(oReader["PAYMENTDATE"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out convDate))
                    {
                        payment.PaymentDate = convDate;
                    }

                    if (DateTime.TryParseExact(oReader["EXPIRYDATE"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out convDate))
                    {
                        payment.ExpiryDate = convDate;
                    }

                    if (DateTime.TryParseExact(oReader["ENROLLMENTDATE"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out convDate))
                    {
                        payment.EnrollmentDate = convDate;
                    }

                    payment.PassportType = oReader["PASSPORTTYPE"].ToString();
                    payment.FormNo = oReader["FORMNO"].ToString();

                    if (int.TryParse(oReader["AMOUNT_NAIRA"].ToString(), out convInt))
                        payment.AmountNaira = convInt;

                    if (int.TryParse(oReader["AMOUNT_DOLLAR"].ToString(), out convInt))
                        payment.AmountDollar = convInt;

                    if (int.TryParse(oReader["PASSPORTSIZE"].ToString(), out convInt))
                        payment.PassportSize = convInt;
                }
            }

            return payment;
        }

        public void SavePaymentReference(int applicationId, int referenceNumber, string formNo, bool paymentMatch)
        {
            var query = @"Update PAYMENT_REF_TABLE Set FORMNO = :FORMNO, PAYMENTMATCHED = :PAYMENTMATCHED, APPLICATIONSTATUS = :APPLICATIONSTATUS 
                            WHERE AID = :AID and REFID = :REFID";

            OracleParameter[] updateParameters =
            {   
                new OracleParameter("FORMNO", formNo),
                new OracleParameter("PAYMENTMATCHED",paymentMatch),                
                new OracleParameter("AID",applicationId),
                new OracleParameter("REFID",referenceNumber),
                new OracleParameter("APPLICATIONSTATUS", "EM1000") // Used
            };

            ExecuteQuery(query, updateParameters);
        }
    }
}
