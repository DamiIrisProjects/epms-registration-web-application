﻿create or replace FUNCTION GENERATEFILENO 
(
  FORMNO IN VARCHAR2, 
  BRANCHCODE IN VARCHAR2
) RETURN VARCHAR2 AS 
BEGIN
  
    --NIS/IKO/1234567

    --NIS – fixed
    --IKO – 2 or 3 letter acronym for a branch (read from the new branch table or config file)
    --1234567 – Sequential file number
    
        RETURN 'NIS/'||BRANCHCODE||'/'||SUBSTR(FORMNO, 4);
    
END GENERATEFILENO;