﻿--------------------------------------------------------
--  DDL for Table PAYMENT_FEE
--------------------------------------------------------

  CREATE TABLE "EPMS_BRANCH"."PAYMENT_FEE" 
   (	"PAYMENT_FEE_ID" NUMBER, 
	"DESCRIPTION" VARCHAR2(100 BYTE), 
	"CURRENCY" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index PAYMENT_FEE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "EPMS_BRANCH"."PAYMENT_FEE_PK" ON "EPMS_BRANCH"."PAYMENT_FEE" ("PAYMENT_FEE_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table PAYMENT_FEE
--------------------------------------------------------

  ALTER TABLE "EPMS_BRANCH"."PAYMENT_FEE" ADD CONSTRAINT "PAYMENT_FEE_PK" PRIMARY KEY ("PAYMENT_FEE_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 NOCOMPRESS LOGGING
  TABLESPACE "USERS"  ENABLE;
 
  ALTER TABLE "EPMS_BRANCH"."PAYMENT_FEE" MODIFY ("PAYMENT_FEE_ID" NOT NULL ENABLE);
 
  ALTER TABLE "EPMS_BRANCH"."PAYMENT_FEE" MODIFY ("DESCRIPTION" NOT NULL ENABLE);
 
  ALTER TABLE "EPMS_BRANCH"."PAYMENT_FEE" MODIFY ("CURRENCY" NOT NULL ENABLE);

Insert into EPMS_BRANCH.PAYMENT_FEE (PAYMENT_FEE_ID,DESCRIPTION,CURRENCY) values (1,'Standard 32 Page < 18 or > 60 or Change due to Marriage','N');
Insert into EPMS_BRANCH.PAYMENT_FEE (PAYMENT_FEE_ID,DESCRIPTION,CURRENCY) values (2,'Standard 32 Page < 18 or > 60 or Change due to Marriage','USD');
Insert into EPMS_BRANCH.PAYMENT_FEE (PAYMENT_FEE_ID,DESCRIPTION,CURRENCY) values (3,'Standard 32 Page 18 - 60','N');
Insert into EPMS_BRANCH.PAYMENT_FEE (PAYMENT_FEE_ID,DESCRIPTION,CURRENCY) values (4,'Standard 32 Page 18 - 60','USD');
Insert into EPMS_BRANCH.PAYMENT_FEE (PAYMENT_FEE_ID,DESCRIPTION,CURRENCY) values (5,'Standard 64 Page','N');
Insert into EPMS_BRANCH.PAYMENT_FEE (PAYMENT_FEE_ID,DESCRIPTION,CURRENCY) values (6,'Standard 64 Page','USD');
Insert into EPMS_BRANCH.PAYMENT_FEE (PAYMENT_FEE_ID,DESCRIPTION,CURRENCY) values (7,'Official 32 Page','N');
Insert into EPMS_BRANCH.PAYMENT_FEE (PAYMENT_FEE_ID,DESCRIPTION,CURRENCY) values (8,'Official 64 Page','N');
Insert into EPMS_BRANCH.PAYMENT_FEE (PAYMENT_FEE_ID,DESCRIPTION,CURRENCY) values (9,'Addidional - Reissue due to loss','N');
Insert into EPMS_BRANCH.PAYMENT_FEE (PAYMENT_FEE_ID,DESCRIPTION,CURRENCY) values (10,'Addidional - Reissue due to loss','USD');
Insert into EPMS_BRANCH.PAYMENT_FEE (PAYMENT_FEE_ID,DESCRIPTION,CURRENCY) values (11,'Additional - Reissue - Change of Information - Other reasons','N');
Insert into EPMS_BRANCH.PAYMENT_FEE (PAYMENT_FEE_ID,DESCRIPTION,CURRENCY) values (12,'Additional - Reissue - Change of Information - Other reasons','USD');

