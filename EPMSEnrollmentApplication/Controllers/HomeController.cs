﻿using EPMSEnrollmentApplication.Helpers;
using Newtonsoft.Json;
using PassportApplicationCommon.Models;
using PassportApplicationProxy;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace EPMSEnrollmentApplication.Controllers
{
    [UserAuthorized]
    public class HomeController : Controller
    {
        #region Variables

        private readonly string _hostUriString = ConfigurationManager.AppSettings["exturl"]; 

        #endregion

        #region Actions

        public ActionResult Index()
        {
            // Decide which options to show
            var opr = Session["User"] as Operator;

            // Question: Wonder if we should hard code these or query the database. Will we every really change the privilege names?
            if (opr != null)
                foreach (var role in opr.Roles)
                {
                    // Screening
                    if (role.Privileges.FirstOrDefault(x => x.Name == "View Screening") != null)
                        ViewBag.ShowScreeningMenuOptions = 1;

                    // Enrollment
                    if (role.Privileges.FirstOrDefault(x => x.Name == "New Enrollment" || x.Name == "Edit Enrollment" ||
                                                   x.Name == "View Completed Enrollments") != null)
                        ViewBag.ShowEnrollmentMenuOptions = 1;

                    if (role.Privileges.FirstOrDefault(x => x.Name == "New Enrollment") != null)
                        ViewBag.ShowNewEnrollmentOption = 1;

                    if (role.Privileges.FirstOrDefault(x => x.Name == "Edit Enrollment") != null)
                        ViewBag.ShowEditEnrollmentOption = 1;

                    if (role.Privileges.FirstOrDefault(x => x.Name == "View Completed Enrollments") != null)
                        ViewBag.ShowCompletedEnrollmentOption = 1;
                }

            // Check for screening responses
            var passports = new EnrollmentProxy().EnrollmentChannel.GetEnrollments(3);
            ViewBag.ScreeningResponses = passports.Count;

            return View();
        }

        [AllowAnonymous]
        public async Task<ActionResult> Login(string id)
        {
            // If a guid has been supplied, Get token from server and log user in
            if (!string.IsNullOrEmpty(id))
            {
                if (User.Identity.IsAuthenticated)
                    FormsAuthentication.SignOut();

                var tokenDictionary = await GetTokenDictionary(id);

                if (tokenDictionary?["access_token"] != null)
                {
                    FormsAuthentication.SetAuthCookie(tokenDictionary["Name"], false);

                    // Create Session User
                    var opr = new Operator
                    {
                        Name = tokenDictionary["Name"],
                        OperatorId = int.Parse(tokenDictionary["UserId"]),
                        Email = tokenDictionary["Email"],
                        RoleType = tokenDictionary["RoleTypeName"],
                        Roles = new JavaScriptSerializer().Deserialize<List<OperatorRole>>(tokenDictionary["Roles"])
                    };
                    Session["User"] = opr;

                    return RedirectToAction("Index");
                }
                ViewBag.ErrorMsg = "Invalid login credentials";
            }

            return View();
        }

        [HttpPost]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(FormCollection form)
        {
            // Do login detail here

            return RedirectToAction("NewEnrollment", "Enrollment");
        }

        #endregion

        #region Operations

        public ActionResult SaveAndRedirect(FormCollection form, string toAction)
        {
            return RedirectToAction(toAction);
        }

        public async Task<Dictionary<string, string>> GetTokenDictionary(string token)
        {
            try
            {
                HttpResponseMessage response;

                var baseUri = new Uri(_hostUriString);
                var baseRequestUri = new Uri(baseUri, "api/operator/ExtToken/" + token);
                using (var client = new HttpClient())
                {
                    // Allow untrusted certificates since we are using self-certificates on the server
                    ServicePointManager.ServerCertificateValidationCallback +=
                        (sender, cert, chain, sslPolicyErrors) => true;

                    response = await client.GetAsync(baseRequestUri);
                }

                var responseContent = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception($"Error: {responseContent}");
                }

                // Get details of variables in token
                var result = GetTokenDictionaryAsync(responseContent);
                return GetTokenDictionaryAsync(result["ticket"]);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static Dictionary<string, string> GetTokenDictionaryAsync(string responseContent)
        {
            return JsonConvert.DeserializeObject<Dictionary<string, string>>(responseContent);
        }

        #endregion
    }
}
