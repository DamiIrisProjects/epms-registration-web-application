﻿using EPMSEnrollmentApplication.Helpers;
using EPMSEnrollmentApplication.Models;
using PassportApplicationCommon.Models;
using PassportApplicationProxy;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Address = PassportApplicationCommon.Models.Address;

namespace EPMSEnrollmentApplication.Controllers
{
    [UserAuthorized]
    public class ScreeningController : Controller
    {
        #region Actions

        public ActionResult Index(string msg)
        {
            var model = new PassportSearch();

            // Get all uncompleted applications
            ViewData["Enrollments"] = new CommonHelper().SelectCompletedEnrollments();

            // Set viewable options            
            model.CompletedType = 2;

            // If there is a message, show it
            ViewBag.Msg = msg;

            return View(model);
        }

        public ActionResult ViewApplicationScreening()
        {
            ViewBag.ShowScreeningMenuOptions = 1;
            ViewBag.ShowScreeningSubMenuOptions = 1;

            // Set Model
            var model = Session["CurrentApplication"] as PassportApplication;

            // Update payment details
            new PaymentProxy().PaymentChannel.CheckPaymentDetails(ref model);

            //// Previous Passports
            //ViewData["PreviousPassports"] = new LookupProxy().LookupChannel.GetPreviousPassports("PassportNumber");

            // Set breeder docs
            if (model.PassportInformation.ApplicationType != 0)
            {
                var breederDocs = GetPassportBreederDocs(model.PassportInformation.ApplicationType);

                foreach (var doc in breederDocs)
                {
                    if (model.BreederDocs.Where(x => x.DocumentId == doc.DocumentId).FirstOrDefault() != null)
                    {
                        doc.IsVerified = true;
                    }
                }

                model.BreederDocs = breederDocs;
            }

            // Reject reasons
            ViewData["RejectionReasons"] = new CommonHelper().SelectRejectReasons();

            if (!string.IsNullOrEmpty(model.PassportInformation.PreviousPassportNumber))
            {
                // Previous passports
                model.PreviousPassports = GetPreviousPassportsDetails(model.PassportInformation.PreviousPassportNumber);
            }

            return View("_ScreeningSummary", model);
        }

        #endregion

        #region Json

        public ActionResult RejectPassport(string formno, string reasonid, string reason)
        {
            if (!string.IsNullOrEmpty(formno) && !string.IsNullOrEmpty(reasonid) && !string.IsNullOrEmpty(reason))
            {
                new EnrollmentProxy().EnrollmentChannel.RejectEnrollment(formno, reasonid, reason);
                return Json(new { status = 1 }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AcceptPassport(string formno)
        {
            if (!string.IsNullOrEmpty(formno))
            {
                new EnrollmentProxy().EnrollmentChannel.AcceptEnrollment(formno);
                return Json(new { status = 1 }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PassportImage(string passportNo, bool isFaceImage)
        {
            var passportImage = new LookupProxy().LookupChannel.GetPassportImage(passportNo, isFaceImage);

            if (passportImage != null)
            {
                return File(passportImage, "image/jpg");
            }
            return File(HttpContext.Server.MapPath("~/Content/Images/EmptySignatureImage.jpg"), "image/jpg");
        }

        #endregion

        #region Operations

        private static List<Passport> GetPreviousPassportsDetails(string passportnum)
        {
            if (!string.IsNullOrEmpty(passportnum))
            {
                // var message = string.Empty;

                if (!new CommonHelper().ValidPassportNo(passportnum))
                {
                    // message = "Please check the passport number. It needs to be 9 characters long, start with an A,D or F followed by 8 numbers.";
                }
                
                passportnum = passportnum.ToUpper();

                // Search Passport
                var passportsVerified = new LookupProxy().LookupChannel.GetPassportHistory(passportnum);

                var passportVerified = passportsVerified.SingleOrDefault(c => c.DocNo == passportnum);

                if (passportVerified?.IssueDate.AddYears(5) < DateTime.Now)
                {
                    if (passportVerified.StageCode == "EM5000")
                    {
                        passportVerified.StageCode = "0";
                        passportVerified.StageDescription = "EXPIRED";
                    }
                    else
                    {
                        passportVerified.StageCode = "0";
                        passportVerified.StageDescription += " / EXPIRED";
                    }
                }

                // order by time
                passportsVerified = passportsVerified.OrderByDescending(x => x.IssueDate).ToList();

                // put original in front
                passportsVerified = passportsVerified.OrderByDescending(x => x.DocNo == passportnum).ToList();

                return passportsVerified;
            }

            return null;
        }       

        private PassportApplication GetTempData()
        {
            var model = new PassportApplication
            {
                PassportInformation = new PassportInformation()
                {
                    ApplicationType = 1,
                    DocumentType = 1
                },
                Payment = new PaymentInformation()
                {
                    ApplicationId = 9080032,
                    ReferenceNumber = 1543187815,
                    Firstname = "ABOSEDE",
                    Surname = "ABIJO",
                    DateOfBirth = DateTime.Parse("10-Jun-49"),
                    Gender = "F",
                    PayeAmount = 8950,
                    PayeDatepaid = DateTime.Parse("20-Apr-2015"),
                    PaymentExist = true,
                    PaymentMatch = true,
                    FirstnameMatch = true,
                    SurnameMatch = true,
                    CalculatedBasePrice = 8750,
                    CalculatedAdditionalPrice = 0,
                    PaymentTotalPrice = 8750
                },
                PersonalInformation = new PersonalInformation()
                {
                    Firstname = "ABOSEDE",
                    Surname = "ABIJO",
                    DateOfBirth = DateTime.Parse("10-Jun-49"),
                    Gender = "F",
                    MaritialStatus = 1,
                }
            };

            model.PersonalInformation.MaritialStatus = 1;

            model.ContactDetails = new ContactDetails()
            {
                MobilePhoneNumber = "08094562884",
                Address = new Address()
                {
                    AddressLine1 = "14 Ado Ekiti Road",
                    City = "Kaduna",
                    Country = "Nigeria",
                    State = "Kaduna",
                    Lga = "KadoEkiti"
                }
            };

            model.Locality = new Locality()
            {
                PlaceOfBirth = "Kaduna",
                StateOfOrigin = "Abia",
                HomeTown = "Kado",
                Nationality = "Nigeria"
            };

            model.PersonalFeatures = new PersonalFeatures()
            {
                ColourOfEyes = 1,
                ColourOfHair = 2,
                HeightInCm = 182
            };

            model.NextOfKin = new NextOfKin()
            {
                NextOfKinName = "Johnson Bado",
                ContactNumberOfNextOfKin = "08093345667",
                RelationshipWithNextOfKin = "Brother",
                Address = new Address()
                {
                    AddressLine1 = "14 Ado Ekiti Road",
                    City = "Kaduna",
                    Country = "Nigeria",
                    State = "Kaduna",
                    Lga = "KadoEkiti"
                }
            };

            return model;
        }

        private List<BreederDoc> GetPassportBreederDocs(int applicationType)
        {
            return new LookupProxy().LookupChannel.GetApplicationTypeDocuments(applicationType);
        }

        public string JsonPartialView(Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);

                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);

                return sw.ToString();
            }
        }

        #endregion
    }
}