﻿using EPMSEnrollmentApplication.Helpers;
using EPMSEnrollmentApplication.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using PassportApplicationCommon.Models;
using PassportApplicationProxy;

namespace EPMSEnrollmentApplication.Controllers
{
    [OptionalAuthorize]
    public class EnrollmentController : Controller
    {
        #region Actions

        public ActionResult NewEnrollment(string uncompleted)
        {
            // Get lists
            ViewData["States"] = new CommonHelper().BuildStatesList();
            ViewData["GenderList"] = new CommonHelper().BuildGenderList();
            ViewData["MaritialStatusList"] = new CommonHelper().BuildMaritialStatusList();
            ViewData["ApplicationReasons"] = new CommonHelper().GetApplicationReasons();
            ViewData["DocumentTypes"] = new CommonHelper().GetDocumentTypeList();
            ViewData["Countries"] = new CommonHelper().GetProcessingCountriesList();
            ViewData["HairColors"] = new CommonHelper().GetHairColours();
            ViewData["EyeColors"] = new CommonHelper().GetEyeColours();

            // Set current application
            if (uncompleted != "1")
                Session["CurrentApplication"] = new PassportApplication();

            // Set Model
            var model = Session["CurrentApplication"] as PassportApplication;
           
            // Set breeder docs
            if (model.PassportInformation.ApplicationType != 0)
            {
                SetBreederDocuments(model);
            }

            // Set viewable options
            ViewBag.ShowEnrollmentSubMenuOptions = 1;
            ViewBag.NewEnrollmentMenuSubOptions = 1;

            // Temp data
            //AddTempData(model);

            return View(model);
        }

        public ActionResult RefreshEnrollment(string msg)
        {
            if (msg == "1")
            {
                var app = Session["CurrentApplication"] as PassportApplication;

                if (app.IsCompleted)
                {
                    TempData["ShowCompletedMsg"] = 1;
                    TempData["FileNo"] = app.FileNo;
                    TempData["AppNo"] = app.Payment.ApplicationId;
                    TempData["Firstname"] = app.PersonalInformation.Firstname;
                    TempData["Surname"] = app.PersonalInformation.Surname;
                }
            }

            // Clear session details
            Session["CurrentApplication"] = new PassportApplication();

            return RedirectToAction("NewEnrollment");
        }

        public ActionResult SearchEnrollment()
        {
            var search = new PassportSearch();

            // Get all uncompleted applications
            ViewData["Enrollments"] = new CommonHelper().SelectUncompletedEnrollments();

            return View(search);
        }

        public ActionResult SearchCompletedEnrollment()
        {
            var search = new PassportSearch();
            search.CompletedType = 1;

            // Get all uncompleted applications
            ViewData["Enrollments"] = new CommonHelper().SelectCompletedEnrollments();

            return View("SearchEnrollment", search);
        }

        public ActionResult ViewCompletedEnrollments()
        {
            // Set Model
            var model = Session["CurrentApplication"] as PassportApplication;

            new PaymentProxy().PaymentChannel.CheckPaymentDetails(ref model);

            // Set breeder docs
            if (model.PassportInformation.ApplicationType != 0)
            {
                SetBreederDocuments(model);
            }

            return View("_ViewCompletedEnrollment", model);
        }

        public ActionResult ViewScreeningResponses()
        {
            var search = new PassportSearch();

            // Get all rejected applications
            ViewData["ScreeningResponses"] = new CommonHelper().SelectRejectedSceeningApplications();

            return View(search);
        }

        #endregion

        #region Json

        public ActionResult SavePassportDetails(PassportInformation pi)
        {
            if (ModelState.IsValid)
            {
                // Update current application
                (Session["CurrentApplication"] as PassportApplication).PassportInformation = pi;

                // Set the documents required for this
                var docs = GetPassportBreederDocs(pi.ApplicationType);

                if (!string.IsNullOrEmpty(pi.PreviousPassportNumber))
                {
                    try
                    {
                        // Get Previous Passport
                        (Session["CurrentApplication"] as PassportApplication).PreviousPassports = GetPreviousPassportsDetails(pi.PreviousPassportNumber);
                    }
                    catch(Exception ex)
                    {
                        // Seeing as anyone not having access to EPMS database will probably break here, lets leave this open for now
                    }
                }

                return Json(new { status = 1, breeder = JsonPartialView(this, "_BreederDocuments", docs) }, JsonRequestBehavior.AllowGet);
            }
            var errors2 = ModelState.Values.SelectMany(x => x.Errors);

            return Json(new { status = 0, err = "<div>The information entered failed validation.</div><div>Please ensure javascript is turned on or contact an Iris Administrator</div>" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveBreederDocuments(string docsvariable)
        {
            if (!string.IsNullOrEmpty(docsvariable))
            {
                var model = Session["CurrentApplication"] as PassportApplication;
                var isValid = true;
                var msg = string.Empty;

                // Get required documents for this application
                var reqdocs = GetPassportBreederDocs(model.PassportInformation.ApplicationType);

                // Check that each document required is there
                foreach (var doc in reqdocs)
                {
                    if (doc.IsRequired == 1 && !docsvariable.Split(',').ToArray().Contains(doc.DocumentId.ToString()))
                    {
                        isValid = false;
                        msg = string.IsNullOrEmpty(msg) ? doc.DocumentName + " is required" : msg + ", " + doc.DocumentName + " is required";
                    }
                }

                if (isValid)
                {
                    // Save to database
                    new EnrollmentProxy().EnrollmentChannel.SaveBreederDocs(docsvariable, model.FormNo);

                    // Update current application
                    model.BreederDocumentsValidated = true;
                    Session["CurrentApplication"] = model;

                    return Json(new { status = 1, msg = msg }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { status = 0, msg = msg }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = 0, err = "<div>The information entered failed validation.</div><div>Please ensure javascript is turned on or contact an Iris Administrator</div>" }, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult SavePaymentInfo(PaymentInformation pi)
        {
            if (ModelState.IsValid)
            {
                // Nothing to save here as it was already done at validation

                // If the person has previous passports, prepopulate details
                return Json(new { status = 1 }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = 0, err = "<div>The information entered failed validation.</div><div>Please ensure javascript is turned on or contact an Iris Administrator</div>" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SavePersonalDetails(PersonalInformation pi)
        {
            if (ModelState.IsValid)
            {
                // Set current application
                var currapp = Session["CurrentApplication"] as PassportApplication;

                // Only save the things which were not set via validation to prevent tampering
                currapp.PersonalInformation.Middlename = pi.Middlename;
                currapp.PersonalInformation.IdentityNumber = pi.IdentityNumber;
                currapp.PersonalInformation.MaritialStatus = pi.MaritialStatus;
                currapp.PersonalInformation.MaidenName = pi.MaidenName;

                // Save to database
                SaveData(currapp);

                return Json(new { status = 1 }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = 0, err = "<div>The information entered failed validation.</div><div>Please ensure javascript is turned on or contact an Iris Administrator</div>" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveContactDetails(ContactDetails details)
        {
            if (ModelState.IsValid)
            {
                // Set current application
                var currapp = Session["CurrentApplication"] as PassportApplication;
                currapp.ContactDetails = details;

                // Save to database
                SaveData(currapp);

                return Json(new { status = 1 }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = 0, err = "<div>The information entered failed validation.</div><div>Please ensure javascript is turned on or contact an Iris Administrator</div>" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveLocality(Locality locality)
        {
            if (ModelState.IsValid)
            {
                // Set current application
                var currapp = Session["CurrentApplication"] as PassportApplication;
                currapp.Locality = locality;

                // Save to database
                SaveData(currapp);

                return Json(new { status = 1 }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = 0, err = "<div>The information entered failed validation.</div><div>Please ensure javascript is turned on or contact an Iris Administrator</div>" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SavePersonalFeatures(PersonalFeatures pf)
        {
            if (ModelState.IsValid)
            {
                // Set current application
                var currapp = Session["CurrentApplication"] as PassportApplication;
                currapp.PersonalFeatures = pf;

                // Save to database
                SaveData(currapp);

                return Json(new { status = 1 }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = 0, err = "<div>The information entered failed validation.</div><div>Please ensure javascript is turned on or contact an Iris Administrator</div>" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveNokInfo(NextOfKin nok)
        {
            if (ModelState.IsValid)
            {
                // Set current application
                var currapp = Session["CurrentApplication"] as PassportApplication;
                currapp.NextOfKin = nok;

                // Application should be complete. Do a verification then save
                var validationResults = new List<ValidationResult>();

                if (ValidateAll(currapp, validationResults))
                {
                    currapp.IsCompleted = true;

                    // Save to database
                    SaveData(currapp);

                    return Json(new { status = 1 }, JsonRequestBehavior.AllowGet);
                }
                // Return validation errors
                return Json(new { status = 0, err = string.Join(",", validationResults.Select(x => x.ErrorMessage).ToArray()) }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = 0, err = "<div>The information entered failed validation.</div><div>Please ensure javascript is turned on or contact an Iris Administrator</div>" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ValidatePayment(PaymentInformation info)
        {
            var app = Session["CurrentApplication"] as PassportApplication;
            app.Payment = info;

            if (DoPaymentValidation(ref app))
            {
                // Update personal information
                app.PersonalInformation.Firstname = app.Payment.Firstname;
                app.PersonalInformation.Surname = app.Payment.Surname;
                app.PersonalInformation.DateOfBirth = app.Payment.DateOfBirth;
                app.PersonalInformation.Gender = app.Payment.Gender;

                // Save the application and get formno/fileno
                app = new EnrollmentProxy().EnrollmentChannel.SaveEnrollment(app);

                // Get previous passport details
                app.PreviousPassports = GetPreviousPassportsDetails(app.PassportInformation.PreviousPassportNumber);

                if (string.IsNullOrEmpty(app.ErrorMessage))
                {
                    // Update session variable
                    Session["CurrentApplication"] = app;

                    // Last passport
                    var prevpassport = app.PreviousPassports.OrderByDescending(x => x.ExpiryDate).FirstOrDefault();

                    // Return information to populate views
                    return Json(new
                    {
                        status = 1,
                        paymentinfo = JsonPartialView(this, "_PaymentProcess", app.Payment),
                        firstname = app.Payment.Firstname,
                        prevfirstname = prevpassport.FirstName,
                        surname = app.Payment.Surname,
                        prevsurname = prevpassport.Surname,
                        dob = app.Payment.DateOfBirth.ToString("dd/MM/yyyy"),
                        prevdob = prevpassport.BirthDate.ToString("dd/MM/yyyy"),
                        gender = app.Payment.Gender == "F" ? "Female" : "Male",
                        formno = app.FormNo,
                        fileno = app.FileNo
                    }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { status = 0, 
                err = app.ErrorMessage, 
                paid = app.Payment.AmountFound, 
                expected = app.Payment.AmountExpected,
                paymentinfo = JsonPartialView(this, "_PaymentProcess", app.Payment)  
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewSelectedEnrollment(string formNo, string completed)
        {
            if (completed == "3")
            {
                var app = new EnrollmentProxy().EnrollmentChannel.GetEnrollmentByFormumber(formNo);
                new PaymentProxy().PaymentChannel.CheckPaymentDetails(ref app);

                if (app != null)
                {
                    SetBreederDocuments(app);
                    return Json(new { status = 1, result = JsonPartialView(this, "_ViewCompletedEnrollment", app), response = JsonPartialView(this, "_ReasonForRejection", app) }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (completed == "0")
                    Session["Enrollments"] = new EnrollmentProxy().EnrollmentChannel.GetEnrollments(1); // Uncompleted
                else if (completed == "1")
                    Session["Enrollments"] = new EnrollmentProxy().EnrollmentChannel.GetEnrollments(2); // Completed

                var app = (Session["Enrollments"] as List<PassportApplication>).Where(x => x.FormNo == formNo).FirstOrDefault();

                if (app != null)
                {
                    var result = new EnrollmentProxy().EnrollmentChannel.GetEnrollment(app.ApplicationId, app.ReferenceNumber);

                    if (result != null)
                    {
                        return Json(new { status = 1, result = JsonPartialView(this, "_SearchResult", new List<PassportApplication>() { result }) }, JsonRequestBehavior.AllowGet);
                    }

                    return Json(new { status = 0, err = "No enrollment found matching this description" }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { status = 0, err = "Error" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchForApplication(PassportSearch search)
        {
            var app = new EnrollmentProxy().EnrollmentChannel.GetEnrollment(int.Parse(search.ApplicationId), int.Parse(search.ReferenceNum));

            if (app != null)
            {
                if (app.StageCode == "EM1000" || app.StageCode == "EM0500") // Uncompleted
                {
                    // Validate payment
                    new PaymentProxy().PaymentChannel.CheckPaymentDetails(ref app);

                    // Set the current passport we are working with
                    Session["CurrentApplication"] = app;

                    return Json(new { status = 1 }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { status = 0, err = "Invalid request" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchForCompletedApplication(PassportSearch search)
        {
            var app = new EnrollmentProxy().EnrollmentChannel.GetEnrollment(int.Parse(search.ApplicationId), int.Parse(search.ReferenceNum));

            if (app != null)
            {
                if (app.StageCode == "EM1500") // Completed
                {
                    // Set the current passport we are working with
                    Session["CurrentApplication"] = app;

                    return Json(new { status = 1 }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { status = 0, err = "Invalid request" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Operations

        private List<Passport> GetPreviousPassportsDetails(string passportnum)
        {
            if (!string.IsNullOrEmpty(passportnum))
            {
                if (!new CommonHelper().ValidPassportNo(passportnum))
                {
                    var msg = "Please check the passport number. It needs to be 9 characters long, start with an A,D or F followed by 8 numbers.";
                }

                var message = string.Empty;
                passportnum = passportnum.ToUpper();

                //Search Passport
                var passportsVerified = new LookupProxy().LookupChannel.GetPassportHistory(passportnum);

                var passportVerified = passportsVerified.SingleOrDefault(c => c.DocNo == passportnum);

                if (passportVerified.IssueDate.AddYears(5) < DateTime.Now)
                {
                    if (passportVerified.StageCode == "EM5000")
                    {
                        passportVerified.StageCode = "0";
                        passportVerified.StageDescription = "EXPIRED";
                    }
                    else
                    {
                        passportVerified.StageCode = "0";
                        passportVerified.StageDescription += " / EXPIRED";
                    }
                }

                // order by time
                passportsVerified = passportsVerified.OrderByDescending(x => x.IssueDate).ToList();

                // put original in front
                passportsVerified = passportsVerified.OrderByDescending(x => x.DocNo == passportnum).ToList();

                return passportsVerified;
            }
            return null;
        }
        
        private bool DoPaymentValidation(ref PassportApplication app)
        {
            new PaymentProxy().PaymentChannel.ValidatePayment(ref app);

            if (!string.IsNullOrEmpty(app.ErrorMessage))
                return false;

            return true;
        }

        private bool ValidateAll(PassportApplication currapp, List<ValidationResult> validationResults)
        {
            var isValid = true;

            // Personal information
            var context = new ValidationContext(currapp.PassportInformation, serviceProvider: null, items: null);
            isValid = Validator.TryValidateObject(currapp.PassportInformation, context, validationResults);

            // Payment
            context = new ValidationContext(currapp.Payment, serviceProvider: null, items: null);
            isValid = Validator.TryValidateObject(currapp.Payment, context, validationResults);
            isValid = DoPaymentValidation(ref currapp);

            // Personal Details
            context = new ValidationContext(currapp.PersonalInformation, serviceProvider: null, items: null);
            isValid = Validator.TryValidateObject(currapp.PersonalInformation, context, validationResults);

            // Contact Details
            context = new ValidationContext(currapp.ContactDetails, serviceProvider: null, items: null);
            isValid = Validator.TryValidateObject(currapp.ContactDetails, context, validationResults);

            // Locality
            context = new ValidationContext(currapp.Locality, serviceProvider: null, items: null);
            isValid = Validator.TryValidateObject(currapp.Locality, context, validationResults);

            // Personal Features
            context = new ValidationContext(currapp.PersonalFeatures, serviceProvider: null, items: null);
            isValid = Validator.TryValidateObject(currapp.PersonalFeatures, context, validationResults);

            // NOK
            context = new ValidationContext(currapp.NextOfKin, serviceProvider: null, items: null);
            isValid = Validator.TryValidateObject(currapp.NextOfKin, context, validationResults);

            return isValid;
        }

        private List<BreederDoc> GetPassportBreederDocs(int applicationType)
        {
            var result = new List<BreederDoc>();

            return new LookupProxy().LookupChannel.GetApplicationTypeDocuments(applicationType);
        }

        private ActionResult SaveData(PassportApplication currapp)
        {
            // Save data
            var result = new EnrollmentProxy().EnrollmentChannel.SaveEnrollment(currapp);

            // If there is a msg to be sent back, return it
            if (string.IsNullOrEmpty(result.ErrorMessage))
                return Json(new { status = 1, formno = result.FormNo, fileno = result.FileNo }, JsonRequestBehavior.AllowGet);
            return Json(new { status = 0, err = result.ErrorMessage }, JsonRequestBehavior.AllowGet);
        }
        
        private void SetBreederDocuments(PassportApplication model)
        {
            var breederDocs = GetPassportBreederDocs(model.PassportInformation.ApplicationType);

            foreach (var doc in breederDocs)
            {
                if (model.BreederDocs.Where(x => x.DocumentId == doc.DocumentId).FirstOrDefault() != null)
                {
                    doc.IsVerified = true;
                }
            }

            model.BreederDocs = breederDocs;
        }

        private void AddTempData(PassportApplication model)
        {
            model.PassportInformation = new PassportInformation()
            {
                ApplicationType = 1,
                DocumentType = 1
            };

            model.Payment = new PaymentInformation()
            {
                ApplicationId = 9080032,
                ReferenceNumber = 1543187815,
                Firstname = "ABOSEDE",
                Surname = "ABIJO",
                DateOfBirth = DateTime.Parse("10-Jun-49"),
                Gender = "F",
                PayeAmount = 8950,
                PayeDatepaid = DateTime.Parse("20-Apr-2015")
            };

            model.PersonalInformation.MaritialStatus = 1;

            model.ContactDetails = new ContactDetails()
            {
                MobilePhoneNumber = "08094562884",
                Address = new Address()
                {
                    AddressLine1 = "14 Ado Ekiti Road",
                    City = "Kaduna",
                    Country = "Nigeria",
                    State = "Kaduna",
                    Lga = "KadoEkiti"
                }
            };

            model.Locality = new Locality()
            {
                PlaceOfBirth = "Kaduna",
                StateOfOrigin = "1",
                HomeTown = "Kado",
                Nationality = "NGA"                
            };

            model.PersonalFeatures = new PersonalFeatures()
            {
                ColourOfEyes = 1,
                ColourOfHair = 2,
                HeightInCm = 182
            };

            model.NextOfKin = new NextOfKin()
            {
                NextOfKinName = "Johnson Bado",
                ContactNumberOfNextOfKin = "08093345667",
                RelationshipWithNextOfKin = "Brother",
                Address = new Address()
                {
                    AddressLine1 = "14 Ado Ekiti Road",
                    City = "Kaduna",
                    Country = "Nigeria",
                    State = "Kaduna",
                    Lga = "KadoEkiti"
                }
            };
        }

        public string JsonPartialView(Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);

                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);

                return sw.ToString();
            }
        }

        #endregion
	}
}