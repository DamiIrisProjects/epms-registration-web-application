﻿using EPMSEnrollmentApplication.Helpers;
using EPMSEnrollmentApplication.Models;
using PassportApplicationCommon.Models;
using System.Web;

namespace EPMSEnrollmentApplication.Cache
{
    public class PassportCache
    {     
        public static FullUserApplication Application
        {
            get
            {
                return GetSessionData.Application;
            }
            set
            {
                GetSessionData.Application = value;
            }
        }

        public static LookupData LookupData
        {
            get
            {
                return GetSessionData.LookupData;
            }
            set
            {
                GetSessionData.LookupData = value;
            }
        }

        public static UserData UserData
        {
            get
            {
                return GetSessionData.UserData;
            }
            set
            {
                GetSessionData.UserData = value;
            }
        }

        private static PassportCacheModel GetSessionData
        {
            get
            {
                PassportCacheModel sessionDataMod;

                if (HttpContext.Current.Session["SessionData"] == null)
                {
                    sessionDataMod = new PassportCacheModel();
                    HttpContext.Current.Session["SessionData"] = sessionDataMod;
                }
                else
                {
                    sessionDataMod = (PassportCacheModel)HttpContext.Current.Session["SessionData"];
                }
                return sessionDataMod;
            }
        }
    }

    public class PassportCacheModel
    {
        public PassportCacheModel()
        {
            Application = new FullUserApplication {PassportApplication = new PassportApplication()};
            LookupData = LoadLookupData();
            UserData = new UserData();
        }

        private static LookupData LoadLookupData()
        {
            var data = new LookupData();

            var helper = new CommonHelper();

            data.ApplicationReasons = helper.GetApplicationReasons();

            data.DocumentTypes = helper.GetDocumentTypeList();

            data.Countries = helper.GetProcessingCountriesList();

            data.EyeColors = helper.GetEyeColours();

            data.HairColors = helper.GetHairColours();

            data.Gender = helper.BuildGenderList();

            data.MaritialStatus = helper.BuildMaritialStatusList();

            return data;
        }

        public FullUserApplication Application { get; set; }

        public LookupData LookupData { get; set; }

        public UserData UserData { get; set; }

        public SessionCache SessionCache;
    }
}