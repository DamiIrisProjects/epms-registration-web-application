﻿using System.Web.Mvc;
using System.Web.Routing;

namespace EPMSEnrollmentApplication
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
              name: "Login",
              url: "Login/{id}",
              defaults: new { controller = "Home", action = "Login", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Search",
                url: "Search/{id}",
                defaults: new { controller = "Enrollment", action = "SearchEnrollment", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              name: "SearchCompleted",
              url: "SearchCompleted/{id}",
              defaults: new { controller = "Enrollment", action = "SearchCompletedEnrollment", id = UrlParameter.Optional }
          );

            routes.MapRoute(
                name: "Enroll",
                url: "Enroll/{id}",
                defaults: new { controller = "Enrollment", action = "NewEnrollment", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "ApplicationScreening",
               url: "ApplicationScreening/{action}/{id}",
               defaults: new { controller = "Screening", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ViewScreeningResponses",
                url: "ViewScreeningResponses/{id}",
                defaults: new { controller = "Enrollment", action = "ViewScreeningResponses", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "ViewCompletedEnrollments",
               url: "ViewCompletedEnrollments/{id}",
               defaults: new { controller = "Enrollment", action = "ViewCompletedEnrollments", id = UrlParameter.Optional }
           );
                        
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
