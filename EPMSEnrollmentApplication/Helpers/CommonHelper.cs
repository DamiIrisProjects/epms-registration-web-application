﻿using PassportApplicationCommon.Models;
using PassportApplicationProxy;
using System;
using System.Linq;
using System.Web.Mvc;

namespace EPMSEnrollmentApplication.Helpers
{
    public class CommonHelper
    {
        internal SelectList BuildStatesList()
        {
            var lp = new LookupProxy();

            var states = lp.LookupChannel.GetNigeriaStates();

            var selectList = new SelectList(states, "StateName", "StateName", "");

            return selectList;
        }

        internal SelectList GetProcessingCountriesList()
        {
            var lp = new LookupProxy();

            var countries = lp.LookupChannel.GetCountries();

            var selectList = new SelectList(countries, "CountryCode", "CountryName", "");

            return selectList;
        }

        internal SelectList GetHairColours()
        {
            var lp = new LookupProxy();

            var colours = lp.LookupChannel.GetHairColours();

            var selectList = new SelectList(colours, "colour", "Description", "");

            return selectList;
        }

        internal SelectList GetEyeColours()
        {
            var lp = new LookupProxy();

            var colours = lp.LookupChannel.GetEyeColours();

            var selectList = new SelectList(colours, "colour", "Description", "");

            return selectList;
        }

        internal SelectList BuildGenderList()
        {
            var lp = new LookupProxy();

            var genderList = lp.LookupChannel.GetGenderList();
            
            var selectList = new SelectList(genderList, "GenderAbbrev", "Description", "");

            return selectList;
        }

        internal SelectList GetApplicationReasons()
        {
            var applicationReason = new LookupProxy().LookupChannel.GetApplicationReasons();

            var selectList = new SelectList(applicationReason, "key", "value", "");

            return selectList;
        }

        internal SelectList SelectUncompletedEnrollments()
        {
            var enrollments = new EnrollmentProxy().EnrollmentChannel.GetEnrollments(1);

            var selectList = new SelectList(enrollments, "FormNo", "FormNoAndName", "");

            return selectList;
        }

        internal SelectList SelectCompletedEnrollments()
        {
            var enrollments = new EnrollmentProxy().EnrollmentChannel.GetEnrollments(2);

            var selectList = new SelectList(enrollments, "FormNo", "FormNoAndName", "");

            return selectList;
        }

        internal SelectList SelectRejectedSceeningApplications()
        {
            var enrollments = new EnrollmentProxy().EnrollmentChannel.GetEnrollments(3);

            var selectList = new SelectList(enrollments, "FormNo", "FormNoAndName", "");

            return selectList;
        }

        internal SelectList SelectRejectReasons()
        {
            var rejectReasons = new LookupProxy().LookupChannel.GetRejectReasons();
            var selectList = new SelectList(rejectReasons, "key", "value", "");
            return selectList;
        }
        

        internal SelectList GetDocumentTypeList()
        {
            var documentTypes = new LookupProxy().LookupChannel.GetDocumentTypes();

            var selectList = new SelectList(documentTypes, "ID", "Description", "");

            return selectList;
        }

        internal DocumentType GetDocumentType(int documentTypeId)
        {
            if (documentTypeId == 0)
                documentTypeId = 1;

            var lp = new LookupProxy();

            return lp.LookupChannel.GetDocumentType(documentTypeId);
        }

        internal SelectList BuildMaritialStatusList()
        {
            var lp = new LookupProxy();

            var marStatus = lp.LookupChannel.GetMaritialStatusList();

            var selectList = new SelectList(marStatus, "key", "value", "");

            return selectList;
        }

        public bool ValidPassportNo(string passportNo)
        {
            if (passportNo.Trim().Length != 9)
                return false;

            var options = ("ADF").ToCharArray();

            if (!options.Contains(passportNo.ToUpper()[0]))
                return false;

            foreach (var chr in passportNo.Substring(1, 8))
            {
                if (!Char.IsNumber(chr))
                    return false;
            }

            return true;
        }


    }
}