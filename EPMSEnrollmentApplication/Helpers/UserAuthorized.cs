﻿using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace EPMSEnrollmentApplication.Helpers
{
    public class OptionalAuthorizeAttribute : AuthorizeAttribute
    {
        private class Http401Result : ActionResult
        {
            public override void ExecuteResult(ControllerContext context)
            {
                // Set the response code to 401.
                context.HttpContext.Response.StatusCode = 401;
                context.HttpContext.Response.End();
            }
        }

        private readonly bool _authorize;

        public OptionalAuthorizeAttribute()
        {
            _authorize = true;
        }

        //OptionalAuthorize is turned on on base controller class, so it has to be turned off on some controller. 
        //That is why parameter is introduced.
        public OptionalAuthorizeAttribute(bool authorize)
        {
            _authorize = authorize;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            //When authorize parameter is set to false, not authorization should be performed.
            if (!_authorize)
                return true;

            // Ensure that User details are in session, otherwise mark as unauthorized
            if (httpContext.Session != null && httpContext.Session["User"] == null)
            {
                FormsAuthentication.SignOut();
                return false;
            }

            var result = base.AuthorizeCore(httpContext);

            return result;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
            {
                //Ajax request doesn't return to login page, it just returns 401 error.
                filterContext.Result = new Http401Result();
            }
            else
                base.HandleUnauthorizedRequest(filterContext);
        }
    }

    public class UserAuthorized : AuthorizeAttribute
    {
        // Custom property
        public string AccessLevel { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var isAuthorized = base.AuthorizeCore(httpContext);
            if (!isAuthorized)
            {
                httpContext.Response.StatusCode = 403;
                return false;
            }

            // Ensure that User details are in session, otherwise mark as unauthorized
            if (httpContext.Session != null && httpContext.Session["User"] == null)
            {
                FormsAuthentication.SignOut();
                httpContext.Response.StatusCode = 403;
                return false;
            }

            return true;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpStatusCodeResult(403);
        }
    }

}