﻿using PassportApplicationProxy;
using System;
using System.Linq;
using System.Web.Mvc;

namespace EPMSEnrollmentApplication.Helpers
{
    public class RestrictAccess : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                var controller = filterContext.RouteData.Values["Controller"].ToString();
                var action = filterContext.RouteData.Values["Action"].ToString();

                var internalAction = Cache.PassportCache.UserData.AllowedInternalActions;

                var count = (from c in internalAction
                             where string.Equals(c.Controller, controller, StringComparison.CurrentCultureIgnoreCase)
                             && string.Equals(c.Action, action, StringComparison.CurrentCultureIgnoreCase)
                             select c).Count();
                
                if (count == 0)
                {
                    filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary {{"controller", "Home"}, 
                                                                                                              {"action", "Index"}});
                }
            }
            catch (Exception ex)
            {
                try
                {                    
                    var sp = new SecurityProxy();

                    sp.SecurityChannel.SaveException(PassportApplicationCommon.Helpers.MapExaroException.MapException(ex,""));
                }
                catch
                {
                    // ignored
                }

                throw;
            }
        }
    }
}