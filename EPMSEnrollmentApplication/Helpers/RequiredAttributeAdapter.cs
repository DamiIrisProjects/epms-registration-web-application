﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using EPMSEnrollmentApplication.App_GlobalResources;

namespace EPMSEnrollmentApplication.Helpers
{
    public class MyRequiredAttributeAdapter : RequiredAttributeAdapter
    {
        public MyRequiredAttributeAdapter(
            ModelMetadata metadata,
            ControllerContext context,
            RequiredAttribute attribute
        )
            : base(metadata, context, attribute)
        {
            attribute.ErrorMessageResourceType = typeof(CustomValidation);
            attribute.ErrorMessageResourceName = "PropertyValueRequired";
        }
    }
}