﻿using System.Linq;
using System.Web.Mvc;

namespace EPMSEnrollmentApplication.Models
{
    public class LookupData
    {
        public SelectList ApplicationReasons { get; set; }

        public SelectList GetApplicationReasons()
        {
            return new SelectList(ApplicationReasons, "Value", "Text");
        }
                
        public SelectList DocumentTypes { get; set; }

        public SelectList GetDocumentTypes()
        {
            return new SelectList(DocumentTypes, "Value", "Text");
        }

        public SelectList Gender { get; set; }

        public SelectList GetGender(string selectedValue)
        {
            return new SelectList(Gender, "Value", "Text");
        }

        public SelectList HairColors { get; set; }

        public SelectList GetHairColors(string selectedValue)
        {
            return new SelectList(HairColors, "Value", "Text");
        }

        public SelectList EyeColors { get; set; }

        public SelectList GetEyeColors(string selectedValue)
        {
            return new SelectList(EyeColors, "Value", "Text");
        }

        public SelectList Countries { get; set; }

        public SelectList GetCountries(string selectedValue)
        {
            return new SelectList(Countries, "Value", "Text");
        }

        public string GetGenderString(string selectedValue)
        {
            if (string.IsNullOrEmpty(selectedValue))
                return string.Empty;

            return Gender.First(c => c.Value == selectedValue).Text;
        }

        public SelectList MaritialStatus { get; set; }

        public SelectList GetMaritialStatus(string selectedValue)
        {
            return new SelectList(MaritialStatus, "Value", "Text");
        }

        public string GetMaritialStatusString(string selectedValue)
        {
            if (string.IsNullOrEmpty(selectedValue))
                return string.Empty;

            return MaritialStatus.First(c => c.Value == selectedValue).Text;
        }

        public string GetCountryString(string selectedValue)
        {
            if (string.IsNullOrEmpty(selectedValue))
                return string.Empty;

            return Countries.First(c => c.Value == selectedValue).Text;
        }

        public string GetDocumentTypeString(int documentId)
        {
            if (documentId == 0)
                return string.Empty;

            return DocumentTypes.First(c => c.Value == documentId.ToString()).Text;
        }
    }
}