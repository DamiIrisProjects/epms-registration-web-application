﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using PassportApplicationCommon.Models;

namespace EPMSEnrollmentApplication.Models
{
    public class PassportSearch
    {
        [DisplayName("Application Id")]
        public string ApplicationId { get; set; }

        [DisplayName("Reference Number")]
        public string ReferenceNum { get; set; }

        [Required]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Invalid file number entered")]
        [Range(0, int.MaxValue, ErrorMessage = "Invalid application number entered")]
        [DisplayName("Form Number")]
        public string FormNumber { get; set; }

        public PassportApplication App { get; set; }

        public PassportApplication SelectedEnrollment { get; set; }

        public int CompletedType { get; set; }
    }
}