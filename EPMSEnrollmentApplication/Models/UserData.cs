﻿using PassportApplicationCommon.Models;
using System.Collections.Generic;

namespace EPMSEnrollmentApplication.Models
{
    public class UserData
    {
        public UserData()
        {
            AllowedInternalActions = new List<InternalActionModel>
            {
                new InternalActionModel() {Action = "Index", Controller = "Enrollement", Id = 1},
                new InternalActionModel() {Action = "SubmitEnrollment", Controller = "Enrollement", Id = 2}
            };

            //Add dummy actions

        }

       public List<InternalActionModel> AllowedInternalActions { get; set; }
    }
}