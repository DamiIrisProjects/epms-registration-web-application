﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EPMSEnrollmentApplication.Startup))]
namespace EPMSEnrollmentApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
