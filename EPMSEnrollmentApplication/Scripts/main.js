﻿$(function () {
    // Login stuff on the top right
    $(".accountoptions").click(function () {
        $("#menu2").fadeIn();
    });

    // Minor adjustment to set width of login dropdown depending on name width
    $(".outersub").css("min-width", ($("#divLoggedIn").width()) + "px").css("left", "-" + ($("#divLoggedIn").width() - 28) + "px");

    $("#menu li.menuitem").hoverIntent(function () {
        if (!$(this).is(":animated"))
            $(this).find("div.outersub").hide().fadeIn();
    },
     function () {
         $(this).find("div.outersub").fadeOut(100);
     });

    $("#divLoggedIn").hoverIntent(function () {
        if (!$(this).is(":animated"))
            $(this).find("div.outersub").hide().slideDown();
    },
    function () {
        if (!$(this).is(":animated"))
            $(this).find("div.outersub").slideUp(100);
    });
    
    $("#menu li.menuitem div.submenu div.ssmenu").hoverIntent(function () {
        if (!$(this).is(":animated"))
            $(this).find("div.submenu").hide().fadeIn();
    },
    function () {
        if (!$(this).is(":animated"))
            $(this).find("div.submenu").fadeOut(100);
    });

    // Pop ups
    $(document).on("click", ".closeme, .closebtn, .closeButton", function () {
        $(".PopupWindow").fadeOut(function () { $(this).trigger("close") });
    });

    // Support div
    $(".vertical").click(function () {
        $(".slideout").animate({ width: "toggle" }, 1000);
    });

    // Logged out errors
    $(document).ajaxError(function (event, jqxhr, settings, exception) {
        if (jqxhr.status == 401) {
            window.location.href = "/Login";
        }
    });
});

