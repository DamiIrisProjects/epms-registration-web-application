﻿$(function () {
    // Setup all combo boxes
    $(".chosen-select").select2({
        placeholder: "Select an option.",
        maximumSelectionSize: 10,
        width: "100%"
    });

    // On search
    $(document).on("click", "#Searchbtn", function () {
        var currform = $("#searchdiv").find("form");
        if (currform.valid())
        {
            $("#screeningsearchload").addClass("loading"); // add loading gif
            $("#searchmsg").fadeOut(function () {
                $.ajax({
                    type: "POST",
                    url: "/Enrollment/SearchForApplication",
                    data: currform.serialize(),
                    dataType: "json",
                    error: function (result) {
                        ShowPopup("Unable to access server. Please ensure you are still connected.");
                        $("#screeningsearchload").removeClass("loading"); // remove loading gif
                    },
                    success: function (result) {
                        $("#screeningsearchload").removeClass("loading"); // remove loading gif

                        if (result.status == "1") {
                            window.location.href = "/Enroll";
                        }
                        else {
                            $("#searchmsg").text(result.err).fadeIn();
                        }
                    }
                });
            });
        }
    });

    // On Select
    $("#selectedEnrollment").on("change", function () {
        var FormNo = $(this).val();
        $("#selectmsg").text("");
        var iscomp = $("#IsCompleted").val();
        $("#selectload").addClass("loading");
        $.ajax({
            type: "POST",
            url: "/Enrollment/ViewSelectedEnrollment",
            data: { FormNo: FormNo, Completed : iscomp },
            dataType: "json",
            error: function (result) {
                ShowPopup("Unable to access server. Please ensure you are still connected.");
                $("#selectload").removeClass("loading"); // remove loading gif
            },
            success: function (result) {
                $("#selectload").removeClass("loading"); // remove loading gif

                if (result.status == "1") {
                    $("#searchresultsdiv").html(result.result);
                    
                    // If it is completed, change the text to View instead of Open
                    if (iscomp == 1 || iscomp == 2)
                        $(".openenrollment").text("View Enrollment");

                    // Show
                    $("#searchresultsdiv").fadeIn();

                    if (iscomp == 3)
                        $("#reasondiv").html(result.response).fadeIn();
                }
                else {
                    $("#selectmsg").text(result.err).fadeIn();
                }
            }
        });
    });

    // G0 t0 enrollment
    $(document).on("click", ".openenrollment", function () {
        $("#selectload").addClass("loading");
        var iscomp = $("#IsCompleted").val();
        var searchurl = iscomp == 0 ? "/Enrollment/SearchForApplication" : "/Enrollment/SearchForCompletedApplication";
        var form = $(this).closest("form");
        $.ajax({
            type: "POST",
            url: searchurl,
            data: form.serialize(),
            dataType: "json",
            error: function (result) {
                ShowPopup("Unable to access server. Please ensure you are still connected.");
                $("#selectload").removeClass("loading"); // remove loading gif
            },
            success: function (result) {
                $("#selectload").removeClass("loading"); // remove loading gif

                if (result.status == "1") {
                    if (iscomp == 1)
                        window.location.href = "/ViewCompletedEnrollments";
                    else if (iscomp == 0)
                        window.location.href = "/Enroll/NewEnrollment?uncompleted=1";
                    else
                        window.location.href = "/ApplicationScreening/ViewApplicationScreening";
                }
                else {
                    $("#selectmsg").text(result.err).fadeIn();
                }
            }
        });
    });

    var ShowPopup = function (msg) {
        $("#popupmsgInner").html(msg);
        $("#PopupMsg").lightbox_me({
            centered: true
        });
    };
});