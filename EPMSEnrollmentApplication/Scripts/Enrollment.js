﻿$(function () {
    // Chrome sometimes gets funny with datepicker. This fixes it
    $.validator.addMethod(
        "date",
        function (value, element) {
            var bits = value.match(/([0-9]+)/gi), str;
            if (!bits)
                return this.optional(element) || false;
            str = bits[1] + "-" + bits[0] + "-" + bits[2];
            return this.optional(element) || !/Invalid|NaN/.test(new Date(str));
        },
        "Please enter a date in the format dd/MM/yyyy"
    );

    // Functions
    var currdiv = $("#passportdetails"); // Set default current div being viewed

    var SetPrevEnrollmentDetails = function () {
        // Set form number and file number
        var formnum = $("#EnrollmentFormNumber").val();
        var fileno = $("#EnrollmentFileNumber").val();
        var continueval = true;

        if (formnum || fileno) {
            $("#formno").text(formnum);
            $("#fileno").text(fileno);
            $("#noforminfodiv").hide();
            $("#forminfodiv").show();
            currdiv.hide();

            // Also continue from where you left off
            // If a form isnt valid, go to that form and show errors
            var gotoform, gotoformlink, currformlink;

            if ($("#passportdetails").find("form").valid()) {
                gotoform = $("#breederdocuments");
                gotoformlink = $("#breederdocumentslink");
                currformlink = $("#passportdetailslink");
                currformlink.addClass("complete");
                gotoformlink.addClass("active");
            }
            else
                continueval = false;

            if (VerifyDocumentChecks()) {
                gotoform = $("#paymentinfo");
                gotoformlink = $("#paymentinfolink");
                currformlink = $("#breederdocumentslink");
                currformlink.addClass("complete");
                gotoformlink.addClass("active");
            }
            else
                continueval = false;


            if (continueval && ($("#paymentinfo").find("form").valid() && VerifyPaymentChecks())) {
                gotoform = $("#personaldetails");
                gotoformlink = $("#personaldetailslink");
                currformlink = $("#paymentinfolink");
                $(".linkoptionsubheader").removeClass("active"); // clear all active
                currformlink.addClass("complete"); // tick this one
                gotoformlink.addClass("active"); // set next one to active
            }
            else
                continueval = false;

            if (continueval && $("#personaldetails").find("form").valid()) {
                gotoform = $("#contactdetails");
                gotoformlink = $("#contactdetailslink");
                currformlink = $("#personaldetailslink");
                $(".linkoptionsubheader").removeClass("active");
                currformlink.addClass("complete");
                gotoformlink.addClass("active");
            }
            else
                continueval = false;

            if (continueval && $("#contactdetails").find("form").valid()) {
                gotoform = $("#locality");
                gotoformlink = $("#localitylink");
                currformlink = $("#contactdetailslink");
                $(".linkoptionsubheader").removeClass("active");
                currformlink.addClass("complete");
                gotoformlink.addClass("active");
            }
            else
                continueval = false;

            if (continueval && $("#locality").find("form").valid()) {
                gotoform = $("#personalfeatures");
                gotoformlink = $("#personalfeatureslink");
                currformlink = $("#localitylink");
                $(".linkoptionsubheader").removeClass("active");
                currformlink.addClass("complete");
                gotoformlink.addClass("active");
            }
            else
                continueval = false;

            if (continueval && $("#personalfeatures").find("form").valid()) {
                gotoform = $("#nokinfo");
                gotoformlink = $("#nokinfolink");
                currformlink = $("#personalfeatureslink");
                $(".linkoptionsubheader").removeClass("active"); // clear all active
                currformlink.addClass("complete"); // tick this one
                gotoformlink.addClass("active"); // set next one to active
            }
            else
                continueval = false;

            ShowForm(gotoform, gotoformlink, currformlink, true);
        }
    };

    var GoToForm = function (gotoformlinkid, isValid) {
        var gotoformlink = $("#" + gotoformlinkid);
        var gotoformid = gotoformlinkid.replace("link", "");
        var gotoform = $("#" + gotoformid);
        var currformlink = $("#" + currdiv.attr("id") + "link");
        var currform = currdiv.find("form");

        // If the same option is clicked, don't do anything
        if (gotoformid == currdiv.attr("id"))
            return;

        // If the form is valid, save it. Otherwise just show the form clicked on
        if (isValid) {
            // if the form was changed, save it. Otherwise just move on
            if (currform.hasClass("dirty") || true) {
                SaveFormData(gotoform, gotoformlink, currform, currformlink, false, currdiv.attr("id"));
            }
            else
                ShowForm(gotoform, gotoformlink, currformlink, isValid);
        }
        else
            ShowForm(gotoform, gotoformlink, currformlink, isValid);
    };

    var ShowForm = function (gotoform, gotoformlink, currformlink, isValid) {
        // hide buttons
        $(".buttons").fadeOut();
        $(".submitbtn").hide();

        // update active divlink
        $(".linkoptionsubheader").fadeOut(function () {
            $(this).removeClass("active");
            gotoformlink.addClass("active");
        });

        // update main div
        currdiv.fadeOut(function () {
            // Show movement buttons then decide which to hide
            $(".prevbtn").show();
            $(".nextbtn").show();

            // if the prev div isnt a form, disable nextbtn
            if (!gotoform.prev().hasClass("formdiv")) {
                $(".prevbtn").hide();
            }

            // if the next div isnt a form, disable nextbtn
            if (!gotoform.next().hasClass("formdiv")) {
                $(".nextbtn").hide();

                // For final form show submite instead of next
                $(".submitbtn").show();
            }

            if (isValid) {
                // mark form as completed
                currformlink.addClass("complete");
            }

            // update the current div variable
            currdiv = gotoform;

            // show divs & buttons
            gotoform.fadeIn();
            $(".buttons").fadeIn();

            // show links
            $(".linkoptionsubheader").fadeIn();
        });
    }

    var ValidatePayment = function () {
        var currformlink = $("#" + currdiv.attr("id") + "link");
        var currform = currdiv.find("form");

        // In case the person tried to go to next form before clicking on validate
        $("#validationMsg").fadeOut(function () {
            if ($("#paymentinfo").find("form").valid()) {
                $("#validatePaymentload").addClass("loading"); // show loading gif

                $.ajax({
                    type: "POST",
                    url: "/Enrollment/ValidatePayment",
                    data: currform.serialize(),
                    dataType: "json",
                    error: function (result) {
                        ShowPopup("Unable to access server. Please ensure you are still connected.");
                        $("#validatePaymentload").removeClass("loading"); // remove loading gif
                    },
                    success: function (result) {
                        $("#validatePaymentload").removeClass("loading"); // remove loading gif

                        if (result.status == "1") {
                            // mark the form as 'clean' now that we have saved it
                            currform.trigger("reinitialize.areYouSure");

                            // Update the form with the new formnumber
                            $("#formno").text(result.formno);
                            $("#FormNumber").val(result.formno);

                            // Update payment validation
                            $("#PaymentValidationDiv").fadeOut(function () {
                                $(this).html(result.paymentinfo).fadeIn();
                            })

                            // Update Personal details as linked to payment
                            $("#pifirstname").val(result.firstname);
                            $("#pisurname").val(result.surname);
                            $("#pidob").val(result.dob);
                            $("#pigender").val(result.gender);

                            // Update the formnumber and the filenumber
                            $("#noforminfodiv").fadeOut(function () {
                                $("#formno").text(result.formno);
                                $("#fileno").text(result.fileno);

                                $("#forminfodiv").fadeIn();
                            });

                            // Redo the gender dropdown for the new personal details
                            $("#personaldetails select.chosen-select").select2({
                                placeholder: "Select an option.",
                                maximumSelectionSize: 10,
                                width: "100%"
                            });
                        }
                        else {
                            // Update the payment checks to show values where it exists

                            if (result.expected)
                                $("#amountexpectedtxt").addClass("redtext").text(result.expected);

                            if (result.paid)
                                $("#amountpaidtxt").text(result.paid);

                            // Update payment validation
                            $("#PaymentValidationDiv").fadeOut(function () {
                                $(this).html(result.paymentinfo).fadeIn();
                            })

                            ShowPopup(result.err);
                        }
                    }
                });
            }
        });
    };

    var ValidateAllForms = function () {
        // If a form isnt valid, go to that form and show errors
        if (!$("#passportdetails").find("form").valid()) {
            GoToForm("passportdetailslink");
            return false;
        }

        if (!VerifyDocumentChecks()) {
            GoToForm("breederdocumentslink");
            return false;
        }

        if (!$("#paymentinfo").find("form").valid() || !VerifyPaymentChecks()) {
            GoToForm("paymentinfolink");
            return false;
        }

        if (!$("#personaldetails").find("form").valid()) {
            GoToForm("personaldetailslink");
            return false;
        }

        if (!$("#contactdetails").find("form").valid() || !VerifyAddressChecks("contactdetails")) {
            GoToForm("contactdetailslink");
            return false;
        }

        if (!$("#locality").find("form").valid()) {
            GoToForm("localitylink");
            return false;
        }

        if (!$("#personalfeatures").find("form").valid()) {
            GoToForm("personalfeatureslink");
            return false;
        }

        if (!$("#nokinfo").find("form").valid() || !VerifyAddressChecks("nokinfo")) {
            GoToForm("nokinfolink");
            return false;
        }

        return true;
    };

    var VerifyPaymentChecks = function () {
        if ($("#verifyPaymentChecksDiv input:checkbox:not(:checked)").length != 0) {
            $("#validationMsg").fadeIn();
            return false;
        }

        $("#validationMsg").fadeOut();
        return true;
    };

    var VerifyDocumentChecks = function () {
        var result = true;
        $("#docsvariable").val(""); // reset variable

        $("#breederdocuments input[type=checkbox]").each(function () {
            var checkbox = $(this);

            // if the document is required and isnt checked
            if ($(this).hasClass("required") && !this.checked)
            {
                $("#validatebreederdocs").fadeIn();
                result = false;
                return false;
            }

            if (this.checked)
            {
                // Add document id to variable which we will post
                if ($("#docsvariable").val() == "")
                    $("#docsvariable").val(checkbox.parent().siblings(".docid").val());
                else
                    $("#docsvariable").val($("#docsvariable").val() + "," + checkbox.parent().siblings(".docid").val());
            }
        });
        
        if (!result)
            return false;

        $("#validatebreederdocs").fadeOut();
        return true;
    };

    var VerifyAddressChecks = function (divname) {
        if ($("#" + divname).find(".statesdropdown").val() == "" && $("#" + divname).find(".statestext").val() == "")
        {
            $("#" + divname).find(".stateserr").show();
            return false;
        }
        else
        {
            $("#" + divname).find(".stateserr").hide();
            return true;
        }
    };    

    var SaveFormData = function (gotoform, gotoformlink, currform, currformlink, formcompleted, url) {
        // show loading gif
        $("#saveformload").addClass("loading");

        $.ajax({
            type: "POST",
            url: "/Enrollment/Save" + url,
            data: currform.serialize(),
            dataType: "json",
            error: function (result) {
                ShowPopup("Unable to access server. Please ensure you are still connected.");
                $("#saveformload").removeClass("loading"); // remove loading gif
            },
            success: function (result) {
                $("#saveformload").removeClass("loading"); // remove loading gif

                // mark the form as 'clean' now that we have saved it
                currform.trigger("reinitialize.areYouSure");

                if (result.status == "1") {
                    if (formcompleted) {
                        // Send to new application and show popup msg
                        window.location.href = "/Enrollment/RefreshEnrollment?msg=1";
                    }
                    else {
                        // if its passport type select, update the documents required
                        if (gotoform.attr("id") == "breederdocuments")
                            $("#breederdocuments").html(result.breeder);

                        // Go to next form
                        ShowForm(gotoform, gotoformlink, currformlink, true);
                    }
                }
                else {
                    ShowPopup(result.err);
                }
            }
        });
    };

    var ShowPopup = function (msg) {
        $("#popupmsgInner").html(msg);
        $("#PopupMsg").lightbox_me({
            centered: true
        });
    };

    // If form being continued, set form number and application id
    SetPrevEnrollmentDetails();

    // Setup all combo boxes
    $(".chosen-select").select2({
        placeholder: "Select an option.",
        maximumSelectionSize: 10,
        width: "100%"
    });

    // Setup calenders
    $(".jdpicker").datepicker({
        yearRange: "-120:+0",
        dateFormat: "dd/mm/yy",
        changeMonth: true,
        changeYear: true
    });

    // Enable AreYouSure on forms to check when changes are made. So we only save info when there is a change
    $("form").areYouSure({ 'silent': true });
    
    // Form link clicked
    $(document).on("click", ".linkoptionsubheader", function () {
        GoToForm($(this).attr("id"));
    });

    // Go to previous form
    $(document).on("click", ".prevbtn", function () {
        // Get previous link
        var prevlink = currdiv.prev().attr("id") + "link";

        // Go to the form
        GoToForm(prevlink, false);
    });

    // Go to next form
    $(document).on("click", ".nextbtn", function () {
        // Validate form
        var form = currdiv.find("form");

        // Manual validation for Addresses
        if (currdiv.attr("id") == "contactdetails") {
            var addressvalid = VerifyAddressChecks("contactdetails");
            addressvalid = currdiv.find("form").valid();

            if (!addressvalid)
                return;
        }

        if (currdiv.find("form").valid() || (currdiv.attr("id") == "breederdocuments" && VerifyDocumentChecks())) {
            // Manual validation for breeder documents
            if (currdiv.attr("id") == "breederdocuments" && !VerifyDocumentChecks())
                return;
            
            // In the case of payment, also verify checkboxes are ticked
            if (currdiv.attr("id") != "paymentinfo" || VerifyPaymentChecks()) {
                // Get next link
                var nextlink = currdiv.next().attr("id") + "link";

                // Go to the form
                GoToForm(nextlink, true);
            }
        }
    });

    // Validate payment button
    $(document).on("click", "#ValidatePayment", function () {
        ValidatePayment();
    });

    // Submitting the application
    $(document).on("click", ".submitbtn", function () {
        // Validate whole form
        if (ValidateAllForms()) {
            SaveFormData(null, null, $("#nokinfo").find("form"), null, true, currdiv.attr("id"));
        }
    });

    // For fresh enrollment, show divs if they arent already visible
    if ($(".buttons").css("display") == "none")
        $(".buttons").fadeIn();

    // For country dropdowns where the user changes it from Nigeria
    $(document).on("change", ".countrydropdown", function () {
        var nearesttable = $(this).closest("table");

        if ($(this).val() != "NGA") {            
            nearesttable.find(".nigerianstates").hide();
            nearesttable.find(".nonnigerianstates").fadeIn();
        }
        else {
            nearesttable.find(".nonnigerianstates").hide();
            nearesttable.find(".nigerianstates").fadeIn();
        }
    });

    var allhidden = 1;
    $(".formdiv").each(function(){
        if ($(this).css("display") != "none")
            allhidden = 0;
    });

    if (allhidden == 1)
        $("#passportdetails").fadeIn();

    // At the start of a new enrollment, set the previous passport visibility
    reason = $("#ddlApplicationReason").val();
    if (reason == "First Passport" || reason == "") {
        $("#textBoxPrevious").prop("disabled", true);
        $("#textBoxPrevious").val("");
    }
    else {
        $("#textBoxPrevious").prop("disabled", false);
    }

    // Allow passport entry only if its first passport
    $(document).on("change", "#ddlApplicationReason", function () {
        if ($(this).val() == 1) {
            $("#textBoxPrevious").prop("disabled", true);
            $("#textBoxPrevious").val("");
        }
        else {
            $("#textBoxPrevious").prop("disabled", false);
        }
    });
});