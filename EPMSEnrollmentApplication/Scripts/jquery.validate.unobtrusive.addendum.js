﻿// Conditional validation
jQuery.validator.addMethod("compare", function (value, element, params) {
    return false;
}, "");

// and an unobtrusive adapter
jQuery.validator.unobtrusive.adapters.add("requiredif", {}, function (options) {
    options.rules["compare"] = true;
    options.messages["compare"] = options.message;
});