﻿$(function () {
    // Keep the options menu always in the screen for easy access
    if ($("#screeningSuboptions").length) {
        var $window = $(window),
            $stickyEl = $("#screeningSuboptions"),
            elTop = $stickyEl.offset().top;

        $window.scroll(function () {
            $stickyEl.toggleClass("sticky", $window.scrollTop() > elTop);
        });
    }

    // Functions
    var ShowPopup = function (msg) {
        $("#popupmsgInner").html(msg);
        $("#PopupMsg").lightbox_me({
            centered: true
        });
    };
    // Go to a particular form
    var GoToForm = function (gotoformlinkid) {
        var gotoformlink = $("#" + gotoformlinkid);
        var gotoformid = gotoformlinkid.replace("link", "");

        // scroll to div
        $("html, body").animate({
            scrollTop: $("#" + gotoformid).offset().top
        }, 500);
    };

    // Form link clicked
    $(document).on("click", ".linkoptionsubheader", function () {
        GoToForm($(this).attr("id"));
    });

    // Accepting the application
    $(document).on("click", "#acceptappbtn", function () {
        $("#acceptappload").addClass("loading");
        $.ajax({
            type: "POST",
            url: "/Screening/AcceptPassport",
            data: { formno: $("#screeningformno").val() },
            dataType: "json",
            error: function (result) {
                ShowPopup("Unable to access server. Please ensure you are still connected.");
                $("#acceptappload").removeClass("loading"); // remove loading gif
            },
            success: function (result) {
                $("#acceptappload").removeClass("loading"); // remove loading gif
                $("#cancelrejectbtn").click();
                window.location.href = "/ApplicationScreening?msg=1";
            }
        });
    });

    // Attempting to reject an application
    $(document).on("click", "#askreject", function () {
        $("#RejectMsg").lightbox_me({
            centered: true,
            onLoad: function() { 
                $(this).find("textarea").focus();
                $(".rejectreasons").select2({
                    placeholder: "Select an option",
                    maximumSelectionSize: 10,
                    width: "100%"
                });
            }
        });
    });

    // Rejecting the application
    $(document).on("click", "#rejectappbtn", function () {
        if ($("#rejectreasondropdown").val() == 0)
        {
            $("#screeningrejecterr").text("Please select a reason from the dropdown menu");
            return;
        }

        if ($("#rejectreason").val() == "")
        {
            $("#screeningrejecterr").text("Please give details of your rejection reason");
            return;
        }

        $.ajax({
            type: "POST",
            url: "/Screening/RejectPassport",
            data: { formno: $("#screeningformno").val(), reasonid: $("#rejectreasondropdown").val(), reason: $("#rejectreason").val() },
            dataType: "json",
            error: function (result) {
                ShowPopup("Unable to access server. Please ensure you are still connected.");
                $("#acceptappload").removeClass("loading"); // remove loading gif
            },
            success: function (result) {
                $("#acceptappload").removeClass("loading"); // remove loading gif
                $("#cancelrejectbtn").click();
                window.location.href = "/ApplicationScreening?msg=2";

            }
        });
    });
    
    // Search for enrollment
    $(document).on("click", "#SearchScreeningbtn", function () {
        var currform = $("#searchdiv").find("form");
        if (currform.valid()) {
            $("#screeningsearchload").addClass("loading"); // add loading gif
            $("#searchmsg").fadeOut(function () {
                $.ajax({
                    type: "POST",
                    url: "/Screening/SearchForApplication",
                    data: currform.serialize(),
                    dataType: "json",
                    error: function (result) {
                        ShowPopup("Unable to access server. Please ensure you are still connected.");
                        $("#screeningsearchload").removeClass("loading"); // remove loading gif
                    },
                    success: function (result) {
                        $("#screeningsearchload").removeClass("loading"); // remove loading gif

                        if (result.status == "1") {
                            $("#searchResultdiv").html(result.screening).fadeIn();
                            $("#searchResultoptionsdiv").fadeIn();
                        }
                        else {
                            $("#searchmsg").fadeIn();
                        }
                    }
                });
            });
        }
    });
});